(** Syntactic representation of transition functions *)

(** This modules aims at providing a DSL for representing transition
    functions of Petri Nets. However, the module stands itself mostly
    independent of Petri Nets. We represent here expressions with an input as
    a type [('x, 'a) t] where ['x] is the input type, to be thought of the
    context, and ['a] the result type. An expression of type [('x, 'a) t]
    represents a partial function ['x -> 'a option].

    The operations that our DSL allows are:

    - Record building / projection
    - Sum types construction / destruction
    - Builtin operations

    Our type of expression also supports {e shadow variables} (simply called
    variables), whose types are not tracked in the context. These can be used
    to circumvent the typing around the context when it is too tedious to do
    so in OCaml. We use the module {!Place} for variables, to avoid
    unnecessary abstraction. *)

(** Before we define the type for DSLs, we defines types that will reify
    constructors of sum types and fields of records. *)

(** Representation of a record field. *)
module Field : sig
  (** A field is given by its projection function plus a name. *)
  type ('record, 'field) t = {name: string; p: 'record -> 'field}
end

(** Representation of a constructor of a sum type *)
module Ctor : sig
  (** A constructor is identified by its name and injection function. *)
  type ('a, 'b) t = {name: string; i: 'a -> 'b}

  (** This type represents the tags that are used by OCaml internally. *)
  type ocaml_tag = Unboxed of int | Boxed of int
end

(** Representation of built-in operations *)
module Builtin : sig
  (** A builtin. Contains a name (for the SMT export to be), a friendly
      description, and a function representing the semantics of the builtin *)
  type ('a, 'b) t = {name: string; friendly: string; semantics: 'a -> 'b}

  val make : ?friendly:string -> name:string -> ('a -> 'b) -> ('a, 'b) t
  (** Create a builtin from its semantics (the function) and a name. The name
      can be used for backends to SMT or other things. *)
end

(** {1 AST of expressions} *)

(** Our type of expressions. ['a] is the type of the input and ['b] the type
    of the result. The semantics of an expression is a function from ['a] to
    ['b option] written [⟦e⟧]. This sum type is private, meaning that this
    definition can only be used to pattern match expressions, not to
    consturct them. See below for smart constructors that can be used to
    construct expressions. *)

type ('a, 'b) t = private
  | Input : ('a, 'a) t  (** Returns its input: [⟦e⟧(x) = Some x] *)
  | Unit : ('x, unit) t  (** Returns the unit const: [⟦Unit⟧(x) = Some ()] *)
  | Int : int -> ('x, int) t
      (** Returns an integer: [⟦Int k⟧(x) = Some k] *)
  | Builtin : ('a, 'b) Builtin.t * ('x, 'a) t -> ('x, 'b) t
      (** Call a builtin function on the given argument:
          [⟦b(e)⟧(x) =
            Option.map b.semantics (⟦e⟧x)] *)
  | Construct : ('a, 'b) Ctor.t * Ctor.ocaml_tag * ('x, 'a) t -> ('x, 'b) t
      (** Use a constructor for a sumtype.
          [⟦construct(c, _, e)⟧(x) = Option.map c.i (⟦e⟧x)]. The argument
          [ocaml_tag] represents the OCaml internal tag for the constructor
          and it is automatically by the smart constructor {!construct}. *)
  | Destruct : ('a, 'b) Ctor.t * Ctor.ocaml_tag * ('x, 'b) t -> ('x, 'a) t
      (** Destruct a sumtype constructor.
          [⟦destruct (_, tag, e)⟧(x) =
       Option.bind (unsafe_destruct tag) (⟦e⟧x)].
          Note that this operation is the only partial operation in the
          language. The operation [unsafe_destruct] uses the internal
          representation of sum types of OCaml and the internal tag. *)
  | Project : ('a, 'b) Field.t * int * ('x, 'a) t -> ('x, 'b) t
      (** Project a field. [⟦Project (f, _, e)⟧(x) = Option.map f.p (⟦e⟧x)].
          The second argument is the field number (internal to OCaml,
          computed by {!project}. *)
  | Tuple : ('ctx, 'record) tuple -> ('ctx, 'record) t
      (** Construct a tuple (see below for the definition of tuples *)
  | Guard : ('x, _) t * ('x, 'a) t -> ('x, 'a) t
      (** [Guard (t, u)] evaluates [t] first, and if it succeeds, discards
          the result, and evaluates [u] and returns its result. *)
  | Branches : ('a, 'b) t list -> ('a, 'b) t
      (** The evaluation of [Branches es] evaluates the expression in [es] in
          order, until it finds one that succeeds and it returns its result. *)
  | Var : 'a Place.t -> ('x, 'a) t
      (** Shadow variables. This can be used to have free variables that are
          not kept tracked of in ['x], in case it is too much for OCaml's
          typing. See the {!Var} module. *)
  | Foreign : 'a -> ('x, 'a) t

(** Abstract description of a record type, as a non-empty list of fields.
    ['ctx] represents the context, ['record] the record type and ['builder]
    the type of the arguments to build the record: it is a product type. *)
and ('ctx, 'record, 'builder) record =
  | F : ('a, 'b) Field.t -> ('x, 'a, ('x, 'b) t) record
      (** A single field. *)
  | CC :
      ('ctx, 'record, 'builder) record * ('ctx, 'record, 'builder2) record
      -> ('ctx, 'record, 'builder * 'builder2) record
      (** Concatenation of records *)

(** A tuple is a description of a record plus the value of the fields. *)
and ('ctx, 'record) tuple =
  | T : ('ctx, 'record, 'builder) record * 'builder -> ('ctx, 'record) tuple

(** {1 Expression constructions} *)

val foreign : 'a -> ('x, 'a) t

val get_foreign : ('x, 'a) t -> (('x, unit) t * 'a) list

val input : ('a, 'a) t
(** The expression that returns its input. *)

val fail : ('x, 'a) t
(** [fail] is the expression that never returns. *)

val compose : ('a, 'b) t -> ('b, 'c) t -> ('a, 'c) t
(** Compose two expressions *)

val int : int -> ('x, int) t
(** [int k] is an expression always returning [k] *)

val unit : ('x, unit) t
(** [unit] is an expression always returning [()]*)

val guard : ('x, _) t -> ('x, 'a) t -> ('x, 'a) t
(** [guard g e] on an input [i] checks whether [g i] returns (), and then
    returns [e
                                  i]. If [g i] fails, then
    [guard g e] fails on [i] *)

val branches : ('a, 'b) t list -> ('a, 'b) t
(** Tries a series of expressions in sequence, returning the first that
    suceeds *)

val builtin : ('a, 'b) Builtin.t -> ('x, 'a) t -> ('x, 'b) t
(** Apply a builtin to an argument. *)

(** {2 Product type} *)

val pair : ('x, 'a) t -> ('x, 'b) t -> ('x, 'a * 'b) t
(** Pair two expressions together *)

val fst : ('x, 'a * 'b) t -> ('x, 'a) t
(** Extract first component of a product expression *)

val snd : ('x, 'a * 'b) t -> ('x, 'b) t
(** Extract second component of a product expression *)

(** {3 Lifting} *)

val lift_left : ('a, 'b) t -> ('a * 'c, 'b * 'c) t

val lift_right : ('a, 'b) t -> ('c * 'a, 'c * 'b) t

(** {2 Arbitrary record types} *)

val project : ('record, 'field) Field.t -> ('x, 'record) t -> ('x, 'field) t

(** [project field e] evaluates [e] and if it succeeds returns its field
    [field] *)

(** To build records, the entire records must be described in order by the
    following type. While the type is a complicated GADT, its usage is very
    simple. For instance, the record instance for the pair type is:

    {[
      let pair : ('ctx, 'a * 'b, 'a Expression.t * 'b Expression.t) record =
        CC (F field_fst, F field_snd)
    ]}

    One caveat: fields must be listed in the order they are declared in the
    type. *)

val record : ('x, 'record, 'param) record -> 'param -> ('x, 'record) t
(** Build a record out of a description. For instance
    [record
                                  pair (e1, e2)] *)

(** {2 Booleans} *)

val bool : bool -> ('x, bool) t
(** [bool b] is an expression always returning [b] *)

val _true : ('x, bool) t
(** Expression evaluating to true *)

val untrue : ('x, bool) t -> ('x, unit) t
(** [untrue e] is an expression that succeeds only if [e] succeeds and
    evaluates to true *)

val _false : ('x, bool) t
(** Expression evaluating to false *)

val unfalse : ('x, bool) t -> ('x, unit) t
(** [unfalse e] is an expression that succeeds only if [e] succeeds and
    evaluates to false *)

val not : ('x, bool) t -> ('x, bool) t
(** Boolean negation at the level of expressions *)

(** {3 Conditionals and guarding} *)

val _if : _then:('x, 'a) t -> ?_else:('x, 'a) t -> ('x, bool) t -> ('x, 'a) t
(** [_if cond ~_then: e
                                  ~_else: e']
    evalates [cond] and if it succeeds, evaluates [e] or [e'] depending on
    the result of [cond]. The parameter [e'] defaults to {!fail} *)

val _when : ('x, bool) t -> ('x, 'a) t -> ('x, 'a) t
(** [_when cond
                                  t] is [_if cond ~_then: t] *)

val _unless : ('x, bool) t -> ('x, 'a) t -> ('x, 'a) t
(** [_unless
                                  cond t] is
    [_if cond ~_then: fail
                                  ~_else: t] *)

val ( == ) : ('x, 'a) t -> ('x, 'a) t -> ('x, bool) t

(** {2 Sum types} *)

val construct : ('a, 'b) Ctor.t -> ('x, 'a) t -> ('x, 'b) t
(** [construct ctor e] constructs the value ctor(e) *)

val construct0 : (unit, 'a) Ctor.t -> ('x, 'a) t

val construct2 :
  ('a * 'b, 'c) Ctor.t -> ('x, 'a) t -> ('x, 'b) t -> ('x, 'c) t

val destruct : ('a, 'b) Ctor.t -> ('x, 'b) t -> ('x, 'a) t
(** [destruct ctor e] evaluates [e] and if it succeeds, matches its result
    against [ctor]. If [e] is indeed of the shape [ctor(e')], then returns
    [e'], otherwise fails *)

val destruct0 : (unit, 'b) Ctor.t -> ('x, 'b) t -> ('x, unit) t

val destruct2 : ('a * 'b, 'c) Ctor.t -> ('x, 'c) t -> ('x, 'a) t * ('x, 'b) t

(** {2 Option} *)

val some : ('x, 'a) t -> ('x, 'a option) t

val unsome : ('x, 'a option) t -> ('x, 'a) t

val none : unit -> ('x, 'a option) t

val unnone : ('x, 'a option) t -> ('x, unit) t

(** {2 Lists} *)

val nil : ('a, 'b list) t

val unnil : ('a, 'b list) t -> ('a, unit) t

val cons : ('a, 'b) t -> ('a, 'b list) t -> ('a, 'b list) t

val uncons : ('a, 'b list) t -> ('a, 'b) t * ('a, 'b list) t

val hd : ('x, 'a list) t -> ('x, 'a) t

val tl : ('x, 'a list) t -> ('x, 'a list) t

(** {1 Variables} *)

module Var : sig
  val inject : 'a Place.t -> ('x, 'a) t
  (** Turn a shadow variable into an expression *)

  val hide : 'a Place.t -> ('x, 'b) t -> ('x * 'a, 'b) t
  (** Hide the variable, by making it part of the context. *)

  val hide_cond : 'x Cond.t -> (unit, 'a) t -> ('x, 'a) t
  (** Hide a set of variable, described a condition. *)

  val freevars : ('x, 'a) t -> Cond._t option
  (** Returns the set of free variables of an expression. We use
      [Cond._t option] instead of [Cond._t] as the list may be empty *)

  val mem : 'a Place.t -> ('x, 'b) t -> bool
  (** [mem loc e] returns [true] if variable [loc] occurs in [e] *)
end

(** {1 Expressions as functions} *)
type ('a, 'b) func = ('a, 'a) t -> ('a, 'b) t

val apply : ('a, 'b) func -> ('x, 'a) t -> ('x, 'b) t

val post_apply : ('a, 'b) func -> ('b, 'c) t -> ('a, 'c) t

val unfunc : ('a, 'b) func -> ('a, 'b) t

val functionalise : ('a, 'b) t -> ('x, 'a) t -> ('x, 'b) t
(** [functionalise e] turns an expression [e] into a functional expression
    mapping any expression of type α in a given context x into an expression
    of type β in the same context *)

(** {1 Expression manipulation} *)
val eval : ('a, 'b) t -> 'a -> 'b option
(** The semantics of an expression is a partial function. *)

val to_string : ('a, 'b) t -> string
(** Produce a nice description of an expression as some kind of source code *)
