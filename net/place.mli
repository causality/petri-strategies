(** Locations of a Net *)

(** Type of locations storing values of type α. This is merely a typed ID. *)
type 'a t

val make : ?show:('a -> string * string) -> unit -> 'a t
(** Create a new location. *)

val compare : 'a t -> 'b t -> ('a, 'b) Poly.Cmp.t
(** Compare two locations of potentially different type. *)

val equal : 'a t -> 'b t -> ('a, 'b) Poly.Eq.t option
(** Check whether two locations are equal. *)

type _t = L : 'a t -> _t  (** Abstracted type *)

(** Module of abstracted type *)
module Untyped : sig
  type t = _t

  val compare : t -> t -> int

  val id : t -> int
end

val show : 'a t -> 'a -> string * string
(** [show loc v] shows [v] as according to the show functions in [loc] *)

val id : 'a t -> int
(** [id t] returns the identifier of [t] *)
