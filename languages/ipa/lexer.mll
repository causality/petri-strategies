{
  open Parser
  exception Lexer_error of Common.Loc.t
}
  
let digit = ['0' - '9']
let integer_constant = digit+
let nondigit = ['_' 'a'-'z' 'A'-'Z' '\'']

let identifier =
  nondigit (nondigit | digit)*
                         
                         (*management of chars/strings*)
let quote = '"'
let double_quote = '"'
let single_quote = '\''
let backslash = '\\'
let newline = "\n" | "\r" | "\r\n"
                         
let simple_escape_sequence =
  (backslash single_quote) | (backslash double_quote)
let escape_sequence =
  simple_escape_sequence
         
let s_char = [^ '"' '\\' '\n'] | escape_sequence
let quote = '"'
let string_literal =
  (quote s_char* quote)

rule token = parse
|  newline {
  Common.Loc.next_line lexbuf;
  token lexbuf
}
| [' ' '\t' ] {token lexbuf}
| integer_constant {
      TK_INT (int_of_string (Lexing.lexeme lexbuf), Common.Loc.of_lexbuf lexbuf)
    }
| "true"              { TK_BOOL (true, Common.Loc.of_lexbuf lexbuf) }
| "false"              { TK_BOOL (false, Common.Loc.of_lexbuf lexbuf) }
| "()"             { TK_UNIT ((), Common.Loc.of_lexbuf lexbuf) }
| '('              { TK_LPAR ((), Common.Loc.of_lexbuf lexbuf) }
| ')'              { TK_RPAR ((), Common.Loc.of_lexbuf lexbuf) }
| ';'              { TK_SEMICOLON ((), Common.Loc.of_lexbuf lexbuf) }
| '+'              { TK_PLUS ((), Common.Loc.of_lexbuf lexbuf) }
| '-'              { TK_MINUS ((), Common.Loc.of_lexbuf lexbuf) }
| '*'              { TK_MULT ((), Common.Loc.of_lexbuf lexbuf) }
| '/'              { TK_DIV ((), Common.Loc.of_lexbuf lexbuf) }
| "fun"             { TK_FUN ((), Common.Loc.of_lexbuf lexbuf) }
| "newref"             { TK_NEWREF ((), Common.Loc.of_lexbuf lexbuf) }
| "newsem"             { TK_NEWSEM ((), Common.Loc.of_lexbuf lexbuf) }
| "grab"             { TK_GRAB ((), Common.Loc.of_lexbuf lexbuf) }
| "release"             { TK_RELEASE ((), Common.Loc.of_lexbuf lexbuf) }
| "succ"             { TK_SUCC ((), Common.Loc.of_lexbuf lexbuf) }
| "pred"             { TK_PRED ((), Common.Loc.of_lexbuf lexbuf) }
| ":="             { TK_SET ((), Common.Loc.of_lexbuf lexbuf) }
| "!"             { TK_DEREF ((), Common.Loc.of_lexbuf lexbuf) }
| "if"             { TK_IF ((), Common.Loc.of_lexbuf lexbuf) }
| "iszero"             { TK_ISZERO ((), Common.Loc.of_lexbuf lexbuf) }
| "then"             { TK_THEN ((), Common.Loc.of_lexbuf lexbuf) }
| "else"             { TK_ELSE ((), Common.Loc.of_lexbuf lexbuf) }
| "->"             { TK_ARROW ((), Common.Loc.of_lexbuf lexbuf) }
| "in"             { TK_IN ((), Common.Loc.of_lexbuf lexbuf) }
| "let"             { TK_LET ((), Common.Loc.of_lexbuf lexbuf) }
| "="             { TK_EQUAL ((), Common.Loc.of_lexbuf lexbuf) }
| "Y"             { TK_Y ((), Common.Loc.of_lexbuf lexbuf) }
| "|"             { TK_PAR ((), Common.Loc.of_lexbuf lexbuf) }
| identifier { TK_ID (Lexing.lexeme lexbuf, Common.Loc.of_lexbuf lexbuf) }
| eof              { EOF }
| _ {
  raise (Lexer_error (Common.Loc.of_lexbuf lexbuf))
}

