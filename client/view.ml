open Js_of_ocaml
open Net

module Make (Lang : Common.Language) = struct
  module TNet = TNet.Make (Lang.Sig)
  module Run = Run.Make (Lang.Sig)
  module Game = Game.Make (Lang.Sig)
  module D = Net.Datatyp
  module A = Lang.Sig.Address
  module M = Lang.Sig.Move
  open TNet

  type selection =
    | NoSelection : selection
    | Token : Run.Availability.t -> selection
    | Justifier :
        { parent_addr: 'b Lang.Sig.Address.t
        ; addr: 'a Lang.Sig.Address.t
        ; tr: Transition._t
        ; f: 'a -> Run.Event._t
        ; justifiers: 'b list }
        -> selection
    | Data : 'b D.t * ('b -> t) -> selection

  and t =
    { net: TNet.t
    ; selection: selection
    ; interface: Lang.Interface.t
    ; game: Game.t
    ; marking: Run.Marking.t
    ; run: Run.t
    ; mode: bool }

  let tokens_at view address = Run.tokens_at view.run address

  let moves_at view (A.A address) =
    List.map (fun v -> M.M (address, v)) (tokens_at view address)

  let justifying_moves view (Transition.T t) =
    match TNet.Transition.addr t with
    | Some addr -> (
        let loc = Game.find_addr addr view.game in
        match loc.parent with
        | None -> None
        | Some p -> Some (moves_at view p.addr) )
    | _ -> None

  let reverse_mode view = {view with mode= not view.mode}

  (** 1. When a transition is ready to be fired (we have everything.) *)
  let fire_transition itr view =
    let marking =
      if view.mode then
        Run.(Marking.(view.marking --- Event.pre itr +++ Event.post itr))
      else Run.(Marking.(view.marking --- Event.post itr +++ Event.pre itr))
    in
    let run = (if view.mode then Run.add else Run.remove) view.run itr in
    Printf.printf "fire_transition: We are called !\n" ;
    {view with marking; selection= NoSelection; run}

  (** 2. When the justifier is picked, we only need the data part *)
  let generate : type a. t -> a D.t -> (a -> t) -> t =
   fun view d k ->
    match d with D.Unit -> k () | _ -> {view with selection= Data (d, k)}

  let select_justifier view addr f justifier =
    Lang.generate_token view.game
      {Common.gen= (fun x -> generate view x)}
      addr (Some justifier)
      (fun token -> fire_transition (f token) view)

  let addr_neg : type a b. (a, b) Transition.t -> a A.t =
   fun tr -> match tr.cond with Negative x -> x.addr | _ -> assert false

  let fire_atransition view (Run.ATr.A atr) =
    match atr with
    | Run.ATr.Ready itr -> fire_transition (Run.Event.E itr) view
    | PendingInput (f, tr) -> (
        let addr = addr_neg tr in
        let f x = Run.Event.E (f x) in
        match Game.parent view.game (A.A addr) with
        | None ->
            Lang.generate_token view.game
              {Common.gen= (fun x -> generate view x)}
              addr None
              (fun token -> fire_transition (f token) view)
        | Some (A.A parent_addr) -> (
            let justifiers = tokens_at view parent_addr in
            let justifiers =
              let is_unanswered token =
                not
                  (List.exists
                     (fun child_candidate ->
                       Lang.Sig.Move.sdep
                         (M.M (parent_addr, token))
                         (M.M (addr, child_candidate)) )
                     (tokens_at view addr) )
              in
              if Game.is_linear view.game (A.A addr) then
                List.filter is_unanswered justifiers
              else justifiers
            in
            match justifiers with
            | [] -> view
            | [justifier] ->
                select_justifier view addr f (M.M (parent_addr, justifier))
            | justifiers ->
                { view with
                  selection=
                    Justifier
                      {parent_addr; addr; tr= Transition.T tr; f; justifiers}
                } ) )

  let is_linear_already_fired view (Transition.T t) =
    match t.Transition.cond with
    | Negative {addr; _} ->
        (* If the address is Linear, we need to see if every justifying
           tokens of addr has been "answered" on this address. *)
        if Game.is_linear view.game (A.A addr) then
          match justifying_moves view (Transition.T t) with
          | None -> tokens_at view addr <> []
          | Some tokens ->
              List.length tokens = List.length (tokens_at view addr)
        else false
    | _ -> false

  let no_justifier view t = justifying_moves view t = Some []

  let available_transitions view =
    match view.mode with
    | true ->
        (* forward mode *)
        let is_fireable a =
          let t = Run.Availability.transition a in
          (not (is_linear_already_fired view t)) && not (no_justifier view t)
        in
        if view.selection <> NoSelection then []
        else
          List.filter is_fireable
          @@ Run.Availability.available view.net view.marking
    | false ->
        (* reverse mode *)
        if view.selection <> NoSelection then [] else []

  let select_transition view possibilities =
    match Run.Availability.project possibilities with
    | Some atr -> fire_atransition view atr
    | None -> {view with selection= Token possibilities}

  let interpret code =
    let term, interface = Lang.Term.parse code in
    let net = Lang.Term.interpret interface term in
    let game = Lang.Interface.game_of interface in
    { net
    ; marking= Run.Marking.empty
    ; selection= NoSelection
    ; run= Run.empty
    ; game
    ; interface
    ; mode= true }

  let select_token location token view =
    match view.selection with
    | Token possibilities -> (
        let possibilities =
          Run.Availability.select possibilities location token
        in
        match Run.Availability.project possibilities with
        | Some atr -> fire_atransition view atr
        | None -> view )
    | _ -> view

  let draw_code upd code view =
    let ( let* ) = Option.bind in
    let enabled = available_transitions view in
    let map_loc (Run.Marking.Tokil.T (l, token)) =
      let* loc = (* Lang.loc_of_location label *) None in
      Some
        ( loc
        , Color.
            [ Class "has_token"
            ; Subscript
                [H.txt @@ Printf.sprintf "%s" (fst @@ Place.show l token)] ]
        )
    in
    let map_tr availability =
      let (Transition.T transition) =
        Run.Availability.transition availability
      in
      let* descr = Lang.descr_of_transition transition.label in
      let* loc = Lang.loc_of_transition transition.label in
      Some
        ( loc
        , Color.
            [ Class "code_active"
            ; Click
                (descr, fun () -> upd (select_transition view availability))
            ] )
    in
    Color.color
      ~locs:
        ( List.filter_map map_loc view.marking
        @ List.filter_map map_tr enabled )
      code

  let draw_net upd view k =
    let dot = Run.Marking.to_dot view.net view.marking in
    let enabled = available_transitions view in
    let filter elem value =
      match value with
      | `Transition (TNet.Transition.T t) ->
          ( try
              let possibility =
                List.find
                  (fun a ->
                    let (Transition.T t') = Run.Availability.transition a in
                    t.TNet.Transition.id = t'.id )
                  enabled
              in
              H.Dot.onclick
                (fun _ -> upd (select_transition view possibility))
                elem t ;
              H.add_class elem "active"
            with _ -> () ) ;
          H.Dot.title Transition.fun_to_string elem (TNet.Transition.T t)
      | `Place (Place.L loc) ->
          let open H in
          let present =
            match view.selection with
            | Token poss ->
                let l = Run.Availability.project_at_loc poss loc in
                if List.length l = 1 then [] else l
            | _ -> []
          in
          let make_child v =
            let title, subtitle = Place.show loc v in
            let x =
              H.Node.make Dom_svg.createTspan
                (if List.mem v present then [("class", "select")] else [])
                [ Node.pcdata title
                ; ( H.Node.make Dom_svg.createTitle [] [Node.pcdata subtitle]
                    :> Dom.node Js.t ) ]
            in
            if List.mem v present then
              x##.onclick :=
                Dom.handler (fun _ ->
                    upd (select_token loc v view) ;
                    Js._false ) ;
            [(x :> Dom.node Js.t); Node.pcdata " "]
          in
          let display =
            List.concat_map make_child (Run.Marking.at view.marking loc)
          in
          H.Dot.title (Printf.sprintf "Place id: %d") elem (Place.id loc) ;
          Dot.update_text
            (elem :> Dom.element Js.t)
            (fun text ->
              List.iter (fun c -> ignore (text##appendChild c)) display )
    in
    H.Dot.(draw ~filter dot k)

  let tooltip upd view =
    let draw : type a. a D.t -> (a -> t) -> _ =
     fun datatyp k ->
      let form : a Formu.form =
        match datatyp with
        | D.Nat ->
            Formu.(
              modify ~pre:[H.em [H.txt "Data of the token (integer): "]] int
              |>> fun n -> n)
        | D.Bool ->
            Formu.(modify ~pre:[H.txt "Data of the token (boolean): "] bool)
        | _ -> assert false
      in
      Some
        ( ""
        , Formu.run "Throw token"
            (Formu.modify ~pre:H.[h2 [txt "Input token data"]] form)
            (fun x -> upd (k x)) )
    in
    match view.selection with
    | Data (datatyp, k) -> draw datatyp k
    | _ -> None

  let draw_run upd view k =
    let is_move_selected : type a. a A.t -> a -> (unit -> unit) option =
     fun addr value ->
      match view.selection with
      | Justifier {parent_addr; addr= addr'; f; justifiers; _} -> (
        match A.equal parent_addr addr with
        | Some E ->
            if List.mem value (justifiers : a list) then
              Some
                (fun () ->
                  upd @@ select_justifier view addr' f (M.M (addr, value)) )
            else None
        | None -> None )
      | _ -> None
    in
    let maxvis = Run.visible_both_maximal view.run in
    let filter elem i =
      match view.mode with
      | true -> (
          (* forward mode *)
          let (Run.Event.E etrans) = Run.at view.run i in
          let (M.M (addr, tok)) =
            match Run.Event.move etrans with
            | None -> failwith "Impossible"
            | Some m -> m
          in
          H.Dot.title
            (fun () ->
              let a, b = Lang.Sig.Address.show_msg addr tok in
              Printf.sprintf "Address: %s\nToken: %s - %s"
                (Lang.Sig.Address.show (A.A addr))
                a b )
            elem () ;
          match is_move_selected addr tok with
          | None -> ()
          | Some f -> H.Dot.onclick f elem () ; H.add_class elem "active" )
      | false ->
          let (Run.Event.E etrans) = Run.at view.run i in
          let (M.M (addr, tok)) =
            match Run.Event.move etrans with
            | None -> failwith "Impossible"
            | Some m -> m
          in
          H.Dot.title
            (fun () ->
              Printf.sprintf "Address: %s\nToken: %s"
                (Lang.Sig.Address.show (A.A addr))
                (fst @@ Lang.Sig.Address.show_msg addr tok) )
            elem () ;
          if maxvis.(i) then (
            H.Dot.onclick
              (fun _ -> upd (fire_transition (Run.Event.E etrans) view))
              elem () ;
            H.add_class elem "active" )
    in
    H.Dot.draw ~filter (Run.to_dot ~game:view.game view.run) k
end
