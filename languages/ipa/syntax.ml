open Common
module E = Net.Expression

module Typ = struct
  type base = Int | Bool | Unit | Ref | Sem

  type t = Base of base | Arrow of t * t

  let show_base = function
    | Int -> "int"
    | Bool -> "bool"
    | Unit -> "unit"
    | Ref -> "ref"
    | Sem -> "sem"

  let subscriptize s =
    let l = ref [] in
    String.iter
      (fun c ->
        match c with
        | '0' -> l := "₀" :: !l
        | '1' -> l := "₁" :: !l
        | '2' -> l := "₂" :: !l
        | '3' -> l := "₃" :: !l
        | '4' -> l := "₄" :: !l
        | '5' -> l := "₅" :: !l
        | '6' -> l := "₆" :: !l
        | '7' -> l := "₇" :: !l
        | '8' -> l := "₈" :: !l
        | '9' -> l := "₉" :: !l
        | _ -> () )
      s ;
    List.fold_left (fun s1 s2 -> s2 ^ s1) "" !l

  let show_base_ann b i =
    match b with
    | Int -> "ℕ" ^ subscriptize (string_of_int i)
    | Bool -> "𝔹" ^ subscriptize (string_of_int i)
    | Unit -> "𝕌" ^ subscriptize (string_of_int i)
    | Ref -> "ref" ^ subscriptize (string_of_int i)
    | Sem -> "sem" ^ subscriptize (string_of_int i)

  let show =
    let rec aux paren = function
      | Base b -> show_base b
      | Arrow (t, u) when paren ->
          "(" ^ aux true t ^ " → " ^ aux false u ^ ")"
      | Arrow (t, u) -> aux true t ^ " → " ^ aux false u
    in
    aux false

  type annoty = BaseA of base * int | ArrowA of annoty * annoty

  let show_ann =
    let rec aux paren = function
      | BaseA (b, i) -> show_base_ann b i
      | ArrowA (t, u) when paren ->
          "(" ^ aux true t ^ " → " ^ aux false u ^ ")"
      | ArrowA (t, u) -> aux true t ^ " → " ^ aux false u
    in
    aux false

  let show_ctx_ann ctx =
    match ctx with
    | [] -> ""
    | (s, ty) :: q ->
        List.fold_left
          (fun s (s', ty) -> s ^ ", " ^ s' ^ ": " ^ show_ann ty)
          (s ^ ": " ^ show_ann ty)
          q

  let rec annotate_typ n = function
    | Base b -> (BaseA (b, n), n + 1)
    | Arrow (sigma, tau) ->
        let sigma', n = annotate_typ n sigma in
        let tau', n = annotate_typ n tau in
        (ArrowA (sigma', tau'), n)
end

module Term = struct
  type operator =
    | Add
    | Sub
    | Mult
    | Div
    | And
    | Succ
    | Pred
    | Iszero
    | Or
    | Not
    | Equal
    | Deref
    | Assign
    | Grab
    | Release
    | Par

  type const = Skip | Bool of bool | Int of int [@@deriving yojson]

  let cskip = E.Ctor.{name= "Skip"; i= (fun () -> Skip)}

  let cbool = E.Ctor.{name= "Bool"; i= (fun b -> Bool b)}

  let cint = E.Ctor.{name= "Int"; i= (fun i -> Int i)}

  type 'data constructor =
    | Const of const
    | Var of string
    | Lambda of string * Typ.t option * 'data t
    | App of 'data t * 'data t
    | If of (Loc.t * Loc.t * Loc.t) * 'data t * 'data t * 'data t
    | Y
    | Operator of (operator * Loc.t) * 'data t list
    | Let of Loc.t * (string * Loc.t) * Loc.t * 'data t * 'data t
    | Newref of (string * Loc.t) * 'data t
    | Newsem of (string * Loc.t) * 'data t

  and 'data t = {loc: Loc.t; constructor: 'data constructor; data: 'data}

  let rec map_constructor f = function
    | Const c -> Const c
    | Var s -> Var s
    | Y -> Y
    | Lambda (a, b, term) -> Lambda (a, b, map f term)
    | App (t1, t2) -> App (map f t1, map f t2)
    | If (ls, a, b, c) -> If (ls, map f a, map f b, map f c)
    | Operator (op, l) -> Operator (op, List.map (map f) l)
    | Let (l, s, l', d1, d2) -> Let (l, s, l', map f d1, map f d2)
    | Newref (s, d) -> Newref (s, map f d)
    | Newsem (s, d) -> Newsem (s, map f d)

  and map f x =
    {x with data= f x.data; constructor= map_constructor f x.constructor}

  module S = Set.Make (String)

  let rec freevars_set term =
    match term.constructor with
    | Const _ -> S.empty
    | Var v -> S.singleton v
    | Y -> S.empty
    | Lambda (a, _, term) -> S.remove a @@ freevars_set term
    | App (t1, t2) -> S.union (freevars_set t1) (freevars_set t2)
    | If (_, a, b, c) ->
        S.union (freevars_set a) (S.union (freevars_set b) (freevars_set c))
    | Operator (_, l) ->
        List.fold_left S.union S.empty @@ List.map freevars_set l
    | Let (_, x, _, body, rest) ->
        S.union (freevars_set body) (S.remove (fst x) (freevars_set rest))
    | Newref (s, body) | Newsem (s, body) ->
        S.remove (fst s) (freevars_set body)

  let common t1 t2 =
    S.elements @@ S.inter (freevars_set t1) (freevars_set t2)

  let freevars term = S.elements @@ freevars_set term
end

module Barendregt = struct
  open Term

  let rec substitute term var subst =
    match term with
    | Const c -> Const c
    | Var s when s = var -> subst
    | Var s -> Var s
    | Y -> Y
    | Lambda (a, b, term) when a = var -> Lambda (a, b, term)
    | Lambda (a, b, term) -> Lambda (a, b, substitute_t term var subst)
    | App (t1, t2) ->
        App (substitute_t t1 var subst, substitute_t t2 var subst)
    | If (loc, a, b, c) ->
        If
          ( loc
          , substitute_t a var subst
          , substitute_t b var subst
          , substitute_t c var subst )
    | Operator (op, l) ->
        Operator (op, List.map (fun t -> substitute_t t var subst) l)
    | Let (loc, s, loc', d1, d2) when fst s = var ->
        Let (loc, s, loc', d1, d2)
    | Let (loc, s, loc', d1, d2) ->
        Let
          (loc, s, loc', substitute_t d1 var subst, substitute_t d2 var subst)
    | Newref (s, d) when fst s = var -> Newref (s, d)
    | Newref (s, d) -> Newref (s, substitute_t d var subst)
    | Newsem (s, d) when fst s = var -> Newsem (s, d)
    | Newsem (s, d) -> Newsem (s, substitute_t d var subst)

  and substitute_t triple var subst =
    { loc= triple.loc
    ; constructor= substitute triple.constructor var subst
    ; data= triple.data }

  let barendregt triple =
    let fresh = ref 0 in
    let rec aux = function
      | Const c -> Const c
      | Var s -> Var s
      | Y -> Y
      | Lambda (a, b, term) ->
          let fr = Printf.sprintf "var%i" !fresh in
          incr fresh ;
          Lambda (fr, b, aux_t (substitute_t term a (Var fr)))
      | App (t1, t2) -> App (aux_t t1, aux_t t2)
      | If (loc, a, b, c) -> If (loc, aux_t a, aux_t b, aux_t c)
      | Operator (op, l) -> Operator (op, List.map aux_t l)
      | Let (loc, s, loc', d1, d2) ->
          let fr = Printf.sprintf "var%i" !fresh in
          incr fresh ;
          Let
            ( loc
            , (fr, snd s)
            , loc'
            , aux_t (substitute_t d1 (fst s) (Var fr))
            , aux_t (substitute_t d2 (fst s) (Var fr)) )
      | Newref (s, d) ->
          let fr = Printf.sprintf "var%i" !fresh in
          incr fresh ;
          Newref ((fr, snd s), aux_t (substitute_t d (fst s) (Var fr)))
      | Newsem (s, d) ->
          let fr = Printf.sprintf "var%i" !fresh in
          incr fresh ;
          Newsem ((fr, snd s), aux_t (substitute_t d (fst s) (Var fr)))
    and aux_t triple =
      { loc= triple.loc
      ; constructor= aux triple.constructor
      ; data= triple.data }
    in
    aux_t triple
end
