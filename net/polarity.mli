(** Polarity -- Either negative or positive *)

type t = Positive | Negative

val is_negative : t -> bool

val is_positive : t -> bool

val dual : t -> t

val mult : t -> t -> t

val ( * ) : t -> t -> t
