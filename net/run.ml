let ( let* ) = Option.bind

module Make (S : Sig.S) = struct
  module TNet = TNet.Make (S)
  module Game = Game.Make (S)
  open TNet

  module Marking = struct
    module Tokil = struct
      type t = T : 'a Place.t * 'a -> t

      let equal (T (loc, v)) (T (loc', v')) =
        match Place.equal loc loc' with Some E -> v = v' | _ -> false

      let to_string (T (loc, v)) =
        Printf.sprintf "%s@%d" (fst (Place.show loc v)) (Place.id loc)
    end

    type t = Tokil.t list

    let empty = []

    let ( +++ ) = ( @ )

    let belongs tokil marking = List.exists (Tokil.equal tokil) marking

    let ( --- ) l1 l2 = List.filter (fun tokil -> not @@ belongs tokil l2) l1

    let at : type a. t -> a Place.t -> a list =
     fun marking loc ->
      List.filter_map
        (fun (Tokil.T (loc', token)) ->
          match Place.equal loc loc' with
          | Some E -> Some (token : a)
          | _ -> None )
        marking

    let to_dot net marking =
      let name (Place.L l) =
        let name = "" (*S.LocLabel.show l.Place.label*) in
        match at marking l with
        | [] -> name
        | tokens ->
            (if name = "" then "" else name ^ ": ")
            ^ String.concat " "
                (List.map
                   (fun t -> Printf.sprintf "%s " (fst @@ Place.show l t))
                   tokens )
      in
      TNet.to_dot ~name net

    let rec zip : type a. a Cond.t -> a -> t =
     fun pre b ->
      match (pre, b) with
      | Cond.L loc, x -> [Tokil.T (loc, x)]
      | Cond.CC (a, b), (x, y) -> zip a x @ zip b y

    let prod l1 l2 =
      List.concat_map (fun x -> List.map (fun y -> (x, y)) l2) l1

    let rec matches : type a. a Cond.t -> t -> a list =
     fun pre marking ->
      match pre with
      | L loc -> at marking loc
      | CC (x, y) -> prod (matches x marking) (matches y marking)

    let to_string l =
      Printf.sprintf "{%s}" (String.concat ", " (List.map Tokil.to_string l))
  end

  module Event = struct
    type ('a, 'b) t =
      {transition: ('a, 'b) Transition.t; input: 'a; output: 'b}

    type _t = E : ('a, 'b) t -> _t  (** Existential quantification *)

    let to_string (E {transition; input; output}) =
      match transition.cond with
      | Negative {post; addr} ->
          Printf.sprintf "%s⁻[%s → %s]"
            S.Address.(show (A addr))
            (fst @@ S.Address.show_msg addr input)
            (Cond.show_in post output)
      | Neutral {pre; post} ->
          Printf.sprintf "*[%s → %s]" (Cond.show_in pre input)
            (Cond.show_in post output)
      | Positive {pre; addr} ->
          Printf.sprintf "%s⁺[%s → %s]"
            S.Address.(show (A addr))
            (Cond.show_in pre input)
            (fst @@ S.Address.show_msg addr output)

    let polarity ev = Transition.(polarity (T ev.transition))

    let pre (E {transition; input; _}) =
      match transition.Transition.cond with
      | Negative _ -> []
      | Positive {pre; _} -> Marking.zip pre input
      | Neutral {pre; _} -> Marking.zip pre input

    let post (E {transition; output; _}) =
      match transition.Transition.cond with
      | Positive _ -> []
      | Negative {post; _} -> Marking.zip post output
      | Neutral {post; _} -> Marking.zip post output

    let marking_after ev marking = Marking.(marking --- pre ev +++ post ev)

    let apply_list events marking =
      List.fold_left (fun m e -> marking_after e m) marking events

    let move : type a b. (a, b) t -> S.Move.t option =
     fun ev ->
      match (ev.transition.cond, ev.input, ev.output) with
      | Negative {addr; _}, tok, _ -> Some (S.Move.M (addr, tok))
      | Positive {addr; _}, _, tok -> Some (S.Move.M (addr, tok))
      | _ -> None

    let move_ (E x) = move x

    let eat ev = Marking.(pre ev --- post ev)

    let prod ev = Marking.(post ev --- pre ev)

    let ddep it1 it2 =
      not
        ( List.filter
            (fun x -> List.exists (Marking.Tokil.equal x) (prod it1))
            (eat it2)
        = [] )

    let sdep (E it1) (E it2) =
      match (move it1, move it2) with
      | Some m1, Some m2 -> S.Move.sdep m1 m2
      | _ -> false

    let make transition input =
      let* output = Transition.run transition (Obj.magic input) in
      Some {transition; input; output}

    let make_ transition input =
      let* x = make transition input in
      Some (E x)

    let conflict e e' =
      let e1 = eat e and e2 = eat e' in
      List.exists (fun i -> List.mem i e1) e2

    let split events =
      let array = Array.of_list events in
      let module S = Set.Make (Int) in
      let find2 p s s' =
        let r = ref None in
        ignore
        @@ S.exists
             (fun i ->
               S.exists
                 (fun j ->
                   if p i j then (
                     r := Some (i, j) ;
                     true )
                   else false )
                 s' )
             s ;
        !r
      in
      let rec aux events =
        match
          find2
            (fun i j -> i <> j && conflict array.(i) array.(j))
            events events
        with
        | None -> [List.map (Array.get array) @@ S.elements events]
        | Some (i, j) -> aux (S.remove i events) @ aux (S.remove j events)
      in
      aux (S.of_list (List.init (Array.length array) (fun i -> i)))
  end

  module ATr = struct
    type ('a, 'b) t =
      | Ready : ('a, 'b) Event.t -> ('a, 'b) t
      | PendingInput :
          ('a -> ('a, 'b) Event.t) * ('a, 'b) TNet.Transition.t
          -> ('a, 'b) t
          (** An available transition. Either a positive or neutral
              transition which has been precomputed (and is guaranteed not to
              fail) or a negative transition waiting for an input token. *)

    type _t = A : ('a, 'b) t -> _t

    let transition : type a b. (a, b) t -> (a, b) Transition.t = function
      | Ready {transition; _} -> transition
      | PendingInput (_, t) -> t

    let zip l l' = List.map2 (fun a b -> (a, b)) l l'

    let ( let* ) = Option.bind

    let make transition pre =
      match Transition.run transition pre with
      | Some output -> Some (Ready {transition; input= pre; output})
      | _ -> None

    let make_negative transition =
      let make tok =
        match Event.make transition tok with
        | None ->
            failwith
            @@ Printf.sprintf "Token rejected by negative transition!"
        | Some ev -> ev
      in
      PendingInput (make, transition)
  end

  module Availability = struct
    type t = A : ('a, 'b) ATr.t list -> t
    (* Invariant: All the ATr have the same underlying transition *)

    let project (A ts) = match ts with [x] -> Some (ATr.A x) | _ -> None

    let available net marking =
      let matches (TNet.Transition.T t) =
        match t.cond with
        | Negative _ -> Some (A [ATr.make_negative t])
        | Neutral {pre; _} ->
            let precondition = Marking.matches pre marking in
            let l = List.filter_map (ATr.make t) precondition in
            if l = [] then None else Some (A l)
        | Positive {pre; _} ->
            let precondition = Marking.matches pre marking in
            let l = List.filter_map (ATr.make t) precondition in
            if l = [] then None else Some (A l)
      in
      List.filter_map matches net.transitions

    let transition (A ts) = Transition.T (ATr.transition (List.hd ts))

    let project_single : type a b c. c Place.t -> (a, b) ATr.t -> c option =
     fun loc t ->
      match t with
      | ATr.Ready
          { input
          ; transition= {cond= Neutral {pre; _} | Positive {pre; _}; _}
          ; _ } ->
          Cond.lookup pre input loc
      | _ -> None

    let filter loc value atr =
      match project_single loc atr with Some v -> v = value | None -> false

    let project_at_loc (A ts) loc = List.filter_map (project_single loc) ts

    let select (A ts) loc token = A (List.filter (filter loc token) ts)
  end

  type t =
    { run: Event._t list
    ; adj_static: bool array array
    ; adj_dynamic: bool array array
    ; adj_dynamic_trans: bool array array
    ; adj_total: bool array array
    ; len_static: int array array
    ; len_total: int array array }

  let static_adj r =
    let n = List.length r in
    let m = Array.make_matrix n n false in
    for i = 0 to n - 1 do
      for j = 0 to n - 1 do
        m.(i).(j) <- Event.sdep (List.nth r i) (List.nth r j)
      done
    done ;
    m

  let dynamic_adj r =
    let n = List.length r in
    let m = Array.make_matrix n n false in
    for i = 0 to n - 1 do
      for j = 0 to n - 1 do
        m.(i).(j) <- Event.ddep (List.nth r i) (List.nth r j)
      done
    done ;
    m

  let transitive_closure adj r =
    let n = Array.length adj in
    for k = 0 to n - 1 do
      for i = 0 to n - 1 do
        for j = 0 to n - 1 do
          match Event.move (List.nth r k) with
          | None ->
              adj.(i).(j) <- max adj.(i).(j) (min adj.(i).(k) adj.(k).(j))
          | _ -> ()
        done
      done
    done ;
    adj

  let caus_length adj =
    let n = Array.length adj in
    let adj =
      Array.map (Array.map (function false -> 0 | true -> 1)) adj
    in
    for k = 0 to n - 1 do
      for i = 0 to n - 1 do
        for j = 0 to n - 1 do
          if adj.(i).(k) = 0 || adj.(k).(j) = 0 then ()
          else adj.(i).(j) <- max adj.(i).(j) (adj.(i).(k) + adj.(k).(j))
        done
      done
    done ;
    adj

  let visibles run =
    let n = List.length run in
    let a = Array.make n false in
    List.iteri
      (fun i (Event.E tr) ->
        match Event.move tr with Some _ -> a.(i) <- true | None -> () )
      run ;
    a

  let show_addr (tr : (_, _) Event.t) =
    match Event.move tr with
    | Some (M (addr, _)) -> S.Address.show (A addr)
    | None -> ""

  let show_token tr =
    match Event.move tr with
    | Some (M (addr, x)) -> fst @@ S.Address.show_msg addr x
    | None -> "*"

  let empty =
    { run= []
    ; adj_static= [||]
    ; adj_dynamic= [||]
    ; adj_dynamic_trans= [||]
    ; adj_total= [||]
    ; len_static= [||]
    ; len_total= [||] }

  (** adj_update: prend un run, une matrice d'adjacence, une etransition et
      une relation de dépendance, renvoie une matrice d'adjacence updatée *)

  let adj_update r (adj : bool array array) t dep =
    let n = Array.length adj in
    let adj_upd = Array.make_matrix (n + 1) (n + 1) false in
    Array.iteri
      (fun i -> Array.iteri (fun j x -> adj_upd.(i + 1).(j + 1) <- x))
      adj ;
    Array.iteri (fun i _ -> adj_upd.(i + 1).(0) <- dep (List.nth r i) t) adj ;
    adj_upd

  (** adj_trans_update: prend une matrice d'adjacence transitive de taille n,
      une matrice d'adjacence de taille n+1, et renvoie une matrice
      d'adjacence transitive de taille n+1*)

  let adj_trans_update (adj_trans : bool array array) (adj : bool array array)
      =
    let n = Array.length adj_trans in
    let adj' = Array.make_matrix (n + 1) (n + 1) false in
    Array.iteri
      (fun i -> Array.iteri (fun j x -> adj'.(i + 1).(j + 1) <- x))
      adj_trans ;
    Array.iteri
      (fun i ->
        Array.iteri (fun j x ->
            adj'.(i + 1).(0) <-
              max adj'.(i + 1).(0) (min (x || i = j) adj.(j + 1).(0)) ) )
      adj_trans ;
    adj'

  (** len_update: prend une matrice de la taille des plus longs chemins de
      taille n, une matrice d'ajacence de taille n+1, et update la taille des
      plus longs chemins *)
  let len_update len adj =
    let n = Array.length len in
    let len' = Array.make_matrix (n + 1) (n + 1) 0 in
    Array.iteri
      (fun i -> Array.iteri (fun j x -> len'.(i + 1).(j + 1) <- x))
      len ;
    Array.iteri
      (fun i ->
        Array.iteri (fun j x ->
            if adj.(j + 1).(0) && (i = j || x >= 1) then
              let y = len'.(i + 1).(0) in
              if x + 1 > y then len'.(i + 1).(0) <- x + 1 else ()
            else () ) )
      len ;
    len'

  let adj_union = Array.map2 (Array.map2 (fun b b' -> b || b'))

  let d_adj_hidden vis adj =
    Array.mapi (fun i -> Array.mapi (fun j b -> b && vis.(i) && vis.(j))) adj

  let remove_adj adj i def =
    let n = Array.length adj in
    let adj_upd = Array.make_matrix (n - 1) (n - 1) def in
    Array.iteri
      (fun j a ->
        Array.iteri
          (fun k _ ->
            match (j < i, k < i) with
            | true, true -> adj_upd.(j).(k) <- adj.(j).(k)
            | true, false -> adj_upd.(j).(k) <- adj.(j).(k + 1)
            | false, true -> adj_upd.(j).(k) <- adj.(j + 1).(k)
            | false, false -> adj_upd.(j).(k) <- adj.(j + 1).(k + 1) )
          a )
      adj_upd ;
    adj_upd

  let maximals adj =
    let n = Array.length adj in
    let max = Array.init n (fun i -> (i, true)) in
    Array.iteri
      (fun i v ->
        Array.iteri (fun _ b -> if b then max.(i) <- (i, false) else ()) v )
      adj ;
    Array.fold_left (fun l (i, b) -> if b then i :: l else l) [] max

  let index_of a l =
    let rec aux a l i =
      match l with
      | [] -> None
      | t :: _ when a = t -> Some i
      | _ :: q -> aux a q (i + 1)
    in
    aux a l 0

  let rec remove_index i l =
    match (i, l) with
    | _, [] -> []
    | 0, _ :: q -> q
    | _, t :: q -> t :: remove_index (i - 1) q

  (** update des infos de causalité avec un coup en plus ou moins *)
  let update (mode : bool) (caus : t) (t : Event._t) =
    match mode with
    | true ->
        (* forward mode *)
        let run = t :: caus.run in
        let vis = visibles run in
        let adj_static = adj_update caus.run caus.adj_static t Event.sdep in
        let adj_dynamic =
          adj_update caus.run caus.adj_dynamic t Event.ddep
        in
        let adj_dynamic_trans =
          adj_trans_update caus.adj_dynamic_trans adj_dynamic
        in
        let d_adj_hid = d_adj_hidden vis adj_dynamic_trans in
        let adj_total = adj_union adj_static d_adj_hid in
        let len_static = len_update caus.len_static adj_static in
        let len_total = len_update caus.len_total adj_total in
        { run
        ; adj_static
        ; adj_dynamic
        ; adj_dynamic_trans
        ; adj_total
        ; len_static
        ; len_total }
    | false ->
        (* reverse mode *)
        let i =
          match index_of t caus.run with
          | None -> failwith "No such move to remove"
          | Some i -> i
        in
        let run = remove_index i caus.run in
        let vis = visibles run in
        let adj_static = remove_adj caus.adj_static i false in
        let adj_dynamic = remove_adj caus.adj_dynamic i false in
        let adj_dynamic_trans = remove_adj caus.adj_dynamic_trans i false in
        let d_adj_hid = d_adj_hidden vis adj_dynamic_trans in
        let adj_total = adj_union adj_static d_adj_hid in
        let len_static = remove_adj caus.len_static i 0 in
        let len_total = remove_adj caus.len_total i 0 in
        { run
        ; adj_static
        ; adj_dynamic
        ; adj_dynamic_trans
        ; adj_total
        ; len_static
        ; len_total }

  let add = update true

  let remove = update false

  let subscriptize s =
    let l = ref ([] : string list) in
    String.iter
      (fun c ->
        match c with
        | '0' -> l := "₀" :: !l
        | '1' -> l := "₁" :: !l
        | '2' -> l := "₂" :: !l
        | '3' -> l := "₃" :: !l
        | '4' -> l := "₄" :: !l
        | '5' -> l := "₅" :: !l
        | '6' -> l := "₆" :: !l
        | '7' -> l := "₇" :: !l
        | '8' -> l := "₈" :: !l
        | '9' -> l := "₉" :: !l
        | _ -> () )
      s ;
    List.fold_left (fun s1 s2 -> s2 ^ s1) "" !l

  let show_token_with_game g e =
    match Event.move e with
    | None -> "*"
    | Some (S.Move.M (addr, v)) ->
        let n = (Game.find_addr (S.Address.A addr) g).id in
        fst (S.Address.show_msg addr v) ^ subscriptize (string_of_int n)

  let to_dot ?game causality =
    let open Dot in
    let run = causality.run in
    let show_token x =
      match game with
      | None -> show_token x
      | Some g -> show_token_with_game g x
    in
    let vis = visibles run in
    let s_len = causality.len_static in
    let d_adj_trans = causality.adj_dynamic_trans in
    let d_adj_hid = d_adj_hidden vis d_adj_trans in
    let t_len = causality.len_total in
    let node_of_move i (Event.E m) =
      if vis.(i) then
        let lbl = Printf.sprintf "%s" (show_token m) (* show_move_short *) in
        let css_class =
          match m.transition.cond with
          | Negative _ -> "negative"
          | Positive _ -> "positive"
          | Neutral _ -> "neutral"
        in
        [ Node.make ~label:i
            ~attrs:Attribute.[class_ css_class; shape "rectangle"; label lbl]
        ]
      else []
    in
    let edge_of_moves i j =
      if d_adj_hid.(i).(j) && t_len.(i).(j) = 1 then
        [Edge.make ~src:i ~tgt:j ~attrs:[]]
      else if s_len.(i).(j) = 1 then
        [Edge.make ~src:i ~tgt:j ~attrs:[Attribute.style "dotted"]]
      else []
    in
    let edg =
      let a = Array.of_list run in
      let l = ref [] in
      for i = 0 to Array.length a - 1 do
        for j = 0 to Array.length a - 1 do
          l := edge_of_moves i j @ !l
        done
      done ;
      !l
    in
    {edges= edg; nodes= List.concat (List.mapi node_of_move run)}

  let find_atrans {run; _} transition output =
    let (Event.E ev) =
      List.find
        (fun (Event.E ev) ->
          ev.Event.transition.id = transition.Transition.id
          && ev.output = Obj.magic output )
        run
    in
    ATr.A (Ready ev)

  let tokens_at : type a. t -> a S.Address.t -> a list =
   fun {run; _} address ->
    List.filter_map
      (fun (Event.E ev) ->
        match Event.move ev with
        | Some (M (addr, token)) -> (
          match S.Address.equal addr address with
          | Some E -> Some (token : a)
          | _ -> None )
        | _ -> None )
      run

  let visible_maximal run =
    let filter i =
      let (E ev) = List.nth run.run i in
      if Transition.is_neutral (Transition.T ev.Event.transition) then None
      else Some (Event.E ev)
    in
    List.filter_map filter (maximals run.adj_dynamic)

  let at {run; _} i = List.nth run i

  let visible_both_maximal run =
    let vis = visibles run.run in
    let smax = maximals run.adj_static in
    let dmax = maximals run.adj_dynamic in
    let n = Array.length vis in
    let maxvis = Array.make n false in
    let smax_a = Array.make n false in
    let dmax_a = Array.make n false in
    List.iter (fun i -> smax_a.(i) <- true) smax ;
    List.iter (fun i -> dmax_a.(i) <- true) dmax ;
    List.iter
      (fun i -> maxvis.(i) <- vis.(i) && smax_a.(i) && dmax_a.(i))
      dmax ;
    maxvis

  let events {run; _} = List.rev run

  let moves run =
    List.filter_map (fun (Event.E e) -> Event.move e) (events run)

  let inputs run =
    List.filter
      (fun (S.Move.M (addr, _)) ->
        S.Address.polarity (A addr) = Polarity.Negative )
      (moves run)

  let outputs run =
    List.filter
      (fun (S.Move.M (addr, _)) ->
        S.Address.polarity (A addr) = Polarity.Positive )
      (moves run)

  module Machine = struct
    type opponent = {o: 'a. S.Move.t option -> 'a S.Address.t -> 'a list}

    type state =
      { opponent: opponent
      ; game: Game.t
      ; net: TNet.t
      ; run: t
      ; marking: Marking.t }

    let run {run; _} = run

    let marking {marking; _} = marking

    let outputs state = outputs (run state)

    let inputs state = inputs (run state)

    let get_pactions state =
      let open Transition in
      let on_transition (T t) =
        match t.cond with
        | Negative _ -> []
        | Neutral {pre; _} | Positive {pre; _} ->
            let inputs = Marking.matches pre state.marking in
            List.filter_map (Event.make_ t) inputs
      in
      List.concat_map on_transition state.net.transitions

    let spawn state justifier (S.Address.A addr) =
      let spawn_move (M (addr, token) : S.Move.t) =
        List.find_map
          (fun (Transition.T t) ->
            match t.cond with
            | Positive _ | Neutral _ -> None
            | Negative x -> (
              match S.Address.equal addr x.addr with
              | None -> None
              | Some E -> (
                match Expression.eval t.func token with
                | None -> None
                | Some output ->
                    Some Event.(E {transition= t; input= token; output}) ) )
            )
          state.net.transitions
      in
      let tokens = state.opponent.o justifier addr in
      List.filter_map (fun tok -> spawn_move (M (addr, tok))) tokens

    let apply state e =
      { state with
        run= add state.run e
      ; marking= Event.marking_after e state.marking }

    let apply_list = List.fold_left apply

    let init game net opponent =
      let init = {game; net; opponent; marking= Marking.empty; run= empty} in
      let events = List.concat_map (spawn init None) (Game.roots game) in
      apply_list init events

    let step state =
      let events = get_pactions state in
      let finish events =
        let apply (run, marking) e =
          (add run e, Event.marking_after e marking)
        in
        let fold (run, marking) event =
          let run, marking = apply (run, marking) event in
          match Event.move_ event with
          | None -> (run, marking)
          | Some (M (addr, _) as justifier) ->
              let events =
                List.concat_map
                  (spawn state (Some justifier))
                  (Game.children state.game (A addr))
              in
              List.fold_left apply (run, marking) events
        in
        let run, marking =
          List.fold_left fold (state.run, state.marking) events
        in
        {state with run; marking}
      in
      if events = [] then None
      else Some (List.map finish (Event.split events))

    let final_states start =
      let rec aux finished = function
        | [] -> finished
        | t :: q -> (
          match step t with
          | None -> aux (t :: finished) q
          | Some rest -> aux finished (q @ rest) )
      in
      aux [] [start]

    let run game net (S.Move.M (root, value)) =
      let state =
        let o : type a. _ -> a S.Address.t -> a list =
         fun _ addr ->
          match S.Address.equal addr root with
          | Some E -> [(value : a)]
          | _ -> []
        in
        init game net {o}
      in
      List.map run (final_states state)
  end
end
