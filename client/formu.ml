open Js_of_ocaml
open H

type 'a form = {get: unit -> 'a; html: Html_types.div_content H.elt list}

let ( |>> ) form f = {form with get= (fun () -> f (form.get ()))}

let const x = {get= (fun _ -> x); html= []}

let input descr f =
  let i = H.input () in
  { html= [i]
  ; get=
      (fun () ->
        let s = Js.to_string @@ (H.Cast.of_input i)##.value in
        try f s
        with e ->
          failwith
            (Printf.sprintf "Invalid input `%s` (expected %s): %s" s descr
               (Printexc.to_string e) ) ) }

let int = input "integer" int_of_string

let bool = input "boolean (true or false)" bool_of_string

let ( *** ) f1 f2 =
  {html= f1.html @ f2.html; get= (fun () -> (f1.get (), f2.get ()))}

let modify ?(pre = []) ?(post = []) form =
  match form.html with
  | [] -> form
  | _ -> {form with html= pre @ form.html @ post}

let choice list =
  let waiter_list = ref [] in
  let generate i (value, text) =
    let id = "_" ^ string_of_int i in
    let input =
      H.input ~a:[a_input_type `Radio; a_id id; a_name "stack"] ()
    in
    waiter_list :=
      (fun () ->
        if Js.to_bool (H.Cast.of_input input)##.checked then Some value
        else None )
      :: !waiter_list ;
    [input; label ~a:[a_label_for id] [txt text]]
  in
  let html = List.concat @@ List.mapi generate list in
  let get () = Option.get @@ List.find_map (fun f -> f ()) !waiter_list in
  {html; get}

let run button form k =
  if form.html = [] then (
    k (form.get ()) ;
    [] )
  else
    let err = span ~a:[a_class ["errform"]] [] in
    let go _ =
      match form.get () with
      | x -> k x ; true
      | exception e ->
          H.Node.set_content err
            [H.txt ("Invalid input: " ^ Printexc.to_string e)] ;
          true
    in
    form.html
    @ [H.br (); a ~a:[a_onclick go; a_class ["button"]] [txt button]; err]
