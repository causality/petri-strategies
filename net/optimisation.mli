(** Petri Net optimisation *)
module Make (S : Sig.S) : sig
  module TNet := TNet.Make(S)

  val optim :
       merge_label:(S.TransLabel.t -> S.TransLabel.t -> S.TransLabel.t)
    -> skippable:(Place._t -> bool)
    -> TNet.t
    -> TNet.t
  (** Optimise a net. The [merge_label] parameter is used when squashing
      transitions together, to compute the resulting label. The [skippable]
      parameter is used to indicate which locations are skippable, i.e.
      removed by optimisations. This allows to leave some locations there if
      they are important to relate to the source code. *)
end
