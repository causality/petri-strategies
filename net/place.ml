type 'a loc_name = ..

type 'a t =
  { name: 'a loc_name
  ; show: 'a -> string * string
  ; cmp: 'b. 'b loc_name -> ('a, 'b) Poly.Cmp.t }

let id {name; _} = Obj.(tag (repr name))

let make (type a) ?(show = fun _ -> ("", "")) () : a t =
  let module X = struct
    type 'a loc_name += L : a loc_name
  end in
  let cmp : type b. b loc_name -> (a, b) Poly.Cmp.t = function
    | X.L -> Poly.Cmp.Eq
    | n ->
        if compare (Obj.tag (Obj.repr X.L)) (Obj.tag (Obj.repr n)) < 0 then
          Less
        else Great
  in
  {name= X.L; show; cmp}

let compare {cmp; _} {name; _} = cmp name

let equal x y = Poly.Cmp.to_eq (compare x y)

type _t = L : 'a t -> _t

module Untyped = struct
  type t = _t

  let compare (L x) (L y) = Poly.Cmp.to_int (compare x y)

  let id (L x) = id x
end

let show {show; _} = show
