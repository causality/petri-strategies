module TNet = Net.TNet.Make (Sig)
module Game = Net.Game.Make (Sig)
module IMap = Map.Make (Int)
module Cond = Net.Cond
open TNet
open Syntax
open Sig
open Transition.Fancy
module A = Sig.Address
module D = Net.Datatyp
module Tr = Transition
open Address.AddrSig
module Place = Net.Place

let ( --> ) = A.( --> )

(** {1 Types to Games} *)
module GameOf = struct
  let rec typ ty =
    let open Game in
    let ret a a' b c =
      [A.(A (Addr (a, Question b)), A (Addr (a', Answer c)))]
    in
    let of_base = function
      | Syntax.Typ.Bool -> ret A.[] A.[] D.Unit D.Bool
      | Int -> ret A.[] A.[] D.Unit D.Nat
      | Unit -> ret A.[] A.[] D.Unit D.Unit
      | Ref ->
          ret A.[L Read] A.[L Read] D.Unit D.Nat
          @ ret A.[L Write] A.[L Write] D.Nat D.Unit
      | Sem ->
          ret A.[L Grab] A.[L Grab] D.Unit D.Unit
          @ ret A.[L Release] A.[L Release] D.Unit D.Unit
    in
    match ty with
    | Typ.BaseA (b, id) ->
        let create (q, a) =
          let rec question =
            {id; addr= q; mode= Linear; children= [answer]; parent= None}
          and answer =
            {id; addr= a; mode= Linear; children= []; parent= Some question}
          in
          question
        in
        List.map create (of_base b)
    | ArrowA (a, b) -> (
        let ga = typ a in
        let gb = typ b in
        let ga' =
          bang
            (map_addresses
               (fun (A a) -> A (A.construct A.[R Argument] a))
               ga )
        in
        let gb' =
          map_addresses (fun (A a) -> A (A.construct A.[L Result] a)) gb
        in
        match gb' with
        | [loc] ->
            loc.children <- ga' @ loc.children ;
            [loc]
        | _ -> failwith "Forming an arrow with a product on the rhs" )

  let context l =
    List.fold_left
      (fun g (s, t) ->
        let g' = typ t in
        Game.(
          g
          ||| map_addresses (fun (A a) -> A (A.construct A.[L (Var s)] a)) g')
        )
      Game.empty l
end

(** {1 Interpretation of interfaces} *)
module Interface = struct
  type t = {context: (string * Typ.annoty) list; typ: Typ.annoty}

  let show {context; typ} =
    Typ.show_ctx_ann context ^ "⊦ " ^ Typ.show_ann typ

  let game_of {context= c; typ= t} =
    let open GameOf in
    let g = context c in
    let g' = typ t in
    let g =
      Game.(
        bang (map_addresses (fun (A a) -> A (A.construct A.[R Context] a)) g)
        ||| map_addresses (fun (A a) -> A (A.construct A.[L Value] a)) g')
    in
    g
end

(** {1 Some useful aliases over primitives of TNet *} *)
let flow ?show ?loc ~f ~source =
  TNet.flow ?show ~llabel:loc ~tlabel:(None, None) ~f ~source

let flow' ~source = TNet.flow' ~llabel:None ~tlabel:(None, None) ~source

let merge_opt x y = match (x, y) with Some x, _ -> Some x | _, y -> y

let merge_label (x, y) (x', y') = (merge_opt x x', merge_opt y y')

let new_loc ?show _loc = Place.make ?show ()

let show_exp show (x, e) =
  let a, b = show x in
  (a, b ^ "[" ^ Exponential.show e ^ "]")

let show_q _ = ("¿", "")

let show_dt d x = (D.show_value d x, "")

let question = A.(Addr ([], Question D.Unit))

let question' d = A.(Addr ([], Question d))

let answer d = A.(Addr ([], Answer d))

let ( >>> ) sigma tau =
  compose ~merge_label
    ~left:{ma= (fun x -> A.destruct A.[L Value] x)}
    ~right:{ma= (fun x -> A.destruct A.[L LContext] x)}
    sigma tau

let rewrite rules net =
  TNet.Rewrite.rewrite
    {rw= (fun a -> TNet.Rewrite.maybe @@ A.apply rules a)}
    net

(** {1 Combinators} *)
let tensor sigma tau =
  let open TNet.Rewrite in
  let rw : type a. linear -> a Address.t -> a result option =
   fun l a ->
    match a with
    | Addr (L LContext :: is, k) -> return (Addr (L LContext :: L l :: is, k))
    | Addr (L Value :: is, k) -> return (Addr (L Value :: L l :: is, k))
    | Addr (R Context :: is, k) -> return (Addr (R Context :: L l :: is, k))
    | _ -> None
  in
  let sigma = rewrite {rw= (fun a -> rw Left a)} sigma
  and tau = rewrite {rw= (fun a -> rw Right a)} tau in
  TNet.union sigma tau

(** {2 Basic primitives} *)

(** {3 Constants} *)
type 'a _const = C : ('a, 'b) E.t * 'b D.t -> 'a _const

let const loc c =
  let (C (exp, d)) =
    match c with
    | Term.Skip -> C (E.unit, Unit)
    | Term.Bool b -> C (E.bool b, Bool)
    | Int k -> C (E.int k, Nat)
  in
  flow ~loc ~show:(show_dt d)
    ~source:A.([L Value] @:: question)
    ~target:A.([L Value] @:: answer d)
    ~f:(fun _ -> exp)
    ()

let compose_bang sigma tau =
  let right : type a. a A.t -> a A.t option = function
    | A.(Addr (R Context :: is, k)) ->
        Some A.(Addr (R PromotedValue :: is, k))
    | _ -> None
  in
  let left : type a. a A.t -> a A.t option = function
    | A.(Addr (R PromotedValue :: is, k)) ->
        Some A.(Addr (R PromotedValue :: is, k))
    | _ -> None
  in
  TNet.compose ~merge_label ~left:{ma= left} ~right:{ma= right} sigma tau

let app func tau =
  let left : type a. a A.t -> a A.t option = function
    | A.(Addr (L Value :: R Argument :: is, k)) ->
        Some A.(Addr (R PromotedValue :: is, k))
    | _ -> None
  in
  let right : type a. a A.t -> a A.t option = function
    | A.(Addr (R PromotedValue :: is, k)) ->
        Some A.(Addr (R PromotedValue :: is, k))
    | _ -> None
  in
  let lrw : type a. linear -> a A.t -> a TNet.Rewrite.result option =
   fun l -> function
    | A.(Addr (R Context :: is, k)) ->
        TNet.Rewrite.return @@ A.(Addr (R Context :: L l :: is, k))
    | _ -> None
  in
  rewrite [[Value; Result] --> [Value]]
  @@ TNet.compose ~merge_label ~left:{ma= left} ~right:{ma= right}
       (TNet.Rewrite.rewrite {rw= (fun x -> lrw Left x)} func)
       (TNet.Rewrite.rewrite {rw= (fun x -> lrw Right x)} tau)

let ( @@@ ) = TNet.union

let seq _x_loc _y_loc _ _semi_loc (D.D d') (D.D d) =
  flow' ~show:show_q
    ~source:A.([L Value] @:: question)
    ~target:A.([L LContext; L Left] @:: question)
    ()
  @@@ flow ~show:show_q
        ~source:A.([L LContext; L Left] @:: answer d')
        ~target:A.([L LContext; L Right] @:: question)
        ~f:(fun _ -> E.unit)
        ()
  @@@ flow' ~show:(show_dt d)
        ~source:A.([L LContext; L Right] @:: answer d)
        ~target:A.([L Value] @:: answer d)
        ()

let netlet (D.D d) (D.D d') =
  let storebody = new_loc ~show:(show_dt d) None in
  let evalarg = new_loc ~show:(show_exp show_q) None in
  let evalcont = new_loc ~show:show_q None in
  let contrepvalue = new_loc ~show:(show_exp (show_dt d)) None in
  let inside =
    { locations= [L evalcont; L evalarg; L storebody; L contrepvalue]
    ; transitions=
        [ Negative.(
            make ~label:(None, None)
              ~addr:A.([L LContext; L Right] @:: answer d)
              ~f:(fun x -> evalcont @:= E.unit ||| storebody @:= x))
        ; Tr.positive' ~loc:evalcont ~label:(None, None)
            A.([L LContext; L Left; L Result] @:: question)
        ; Tr.negative'
            A.([L LContext; L Left; R Argument] @:: question)
            ~loc:evalarg ~label:(None, None)
        ; Neutral.(
            make ~label:(None, None)
              ~f:
                (let* value = L storebody and* e = L evalarg in
                 storebody @:= value
                 ||| contrepvalue @:= E.pair value (E.snd e) ))
        ; Tr.positive' ~loc:contrepvalue
            A.([L LContext; L Left; R Argument] @:: answer d)
            ~label:(None, None) ] }
  in
  flow' ~show:show_q
    ~source:A.([L Value] @:: question)
    ~target:A.([L LContext; L Right] @:: question)
    ()
  @@@ inside
  @@@ flow' ~show:(show_dt d')
        ~source:A.([L LContext; L Left; L Result] @:: answer d')
        ~target:A.([L Value] @:: answer d')
        ()

(** {3 Lambda Abstraction} *)
let lambda var net =
  let open A in
  let rw : type a. a Address.t -> a TNet.Rewrite.result option = function
    | Addr (R Context :: L (Var v) :: is, k) when var = v ->
        TNet.Rewrite.return (Addr (L Value :: R Argument :: is, k))
    | Addr (L Value :: is, k) ->
        TNet.Rewrite.return (Addr (L Value :: L Result :: is, k))
    | _ -> None
  in
  Rewrite.(rewrite {rw} net)

(** {3 Interpretation of variables (dereliction)} *)
let var _loc var typ =
  let fw addr =
    let left = A.([R Context; L (Var var)] @:: addr) in
    let right = A.([L Value] @:: addr) in
    let leftToRight x = E.(guard (Exponential.undiamond (snd x)) (fst x)) in
    let rightToLeft x = E.pair x (Exponential.diamond ()) in
    TNet.Fw
      {left; right; leftToRight; rightToLeft; tlbl= (None, None); llbl= None}
  in
  TNet.forwarder {TNet.fw} (GameOf.typ typ)

(** {2 State related operations (references, semaphores)} *)
type 'state operator =
  | O :
      { question: 'a D.t
      ; answer: 'b D.t
      ; injections: linear list
      ; handle_req: ('a * 'state, 'b) E.func
      ; handle_store: ('a * 'state, 'state) E.func }
      -> 'state operator

type 'state operator' =
  | O' :
      { question: 'a Address.t
      ; answer: 'b Address.t
      ; handle_req: ('a * 'state, 'b) E.func
      ; handle_store: ('a * 'state, 'state) E.func
      ; req: ('a * Exponential.t) Place.t
      ; reply: ('b * Exponential.t) Place.t }
      -> 'state operator'

let atomic (type a) (D.D d) show (init : (_, a) E.t)
    (operators : a operator list) _name =
  let call = new_loc ~show:show_q None in
  let store : (a * Exponential.t) Place.t =
    new_loc ~show:(show_exp (fun x -> (show x, ""))) None
  in
  let operators =
    List.map
      (fun (O o) ->
        O'
          { question= A.(A.to_injs o.injections @:: question' o.question)
          ; answer= A.(A.to_injs o.injections @:: answer o.answer)
          ; handle_req= o.handle_req
          ; handle_store= o.handle_store
          ; req= new_loc ~show:(show_exp (show_dt o.question)) None
          ; reply= new_loc ~show:(show_exp (show_dt o.answer)) None } )
      operators
  in
  let make (O' o) =
    [ Negative.(
        make
          ~addr:A.([L LContext; R Argument] @:: o.question)
          ~label:(None, None)
          ~f:(fun x -> o.req @:= x))
    ; Neutral.(
        make ~label:(None, None)
          ~f:
            (let* req = L o.req and* value = L store in
             o.reply
             @:= E.pair
                   (E.apply o.handle_req E.(pair (fst req) (fst value)))
                   (E.snd req)
             ||| store
                 @:= E.pair
                       (E.apply o.handle_store
                          E.(pair (fst req) (fst value)) )
                       (E.snd req) ))
    ; Tr.positive'
        A.([L LContext; R Argument] @:: o.answer)
        ~loc:o.reply ~label:(None, None) ]
  in
  let transitions =
    [ Negative.(
        make
          ~addr:A.([L Value] @:: question)
          ~label:(None, None)
          ~f:(fun x ->
            store @:= E.pair init (Exponential.diamond ()) ||| call @:= x ))
    ; Tr.positive' ~label:(None, None) ~loc:call
        A.([L LContext; L Result] @:: question) ]
    @ List.concat_map make operators
  in
  { locations=
      Place.(
        [L call; L store]
        @ List.concat_map (fun (O' o) -> [L o.reply; L o.req]) operators)
  ; transitions }
  @@@ flow' ~show:(show_dt d)
        ~source:A.([L LContext; L Result] @:: answer d)
        ~target:A.([L Value] @:: answer d)
        ()

let newref d =
  atomic d string_of_int (E.int 0)
    [ O
        { question= D.Nat
        ; answer= D.Unit
        ; injections= [Write]
        ; handle_req= (fun _ -> E.unit)
        ; handle_store= (fun x -> E.fst x) }
    ; O
        { question= D.Unit
        ; answer= D.Nat
        ; injections= [Read]
        ; handle_req= (fun x -> E.snd x)
        ; handle_store= (fun x -> E.snd x) } ]

let newsem d =
  atomic d string_of_bool (E.bool false)
    [ O
        { question= D.Unit
        ; answer= D.Unit
        ; injections= [Grab]
        ; handle_req= (fun _ -> E.unit)
        ; handle_store= (fun x -> E.(_unless (snd x) _true)) }
    ; O
        { question= D.Unit
        ; answer= D.Unit
        ; injections= [Release]
        ; handle_req= (fun _ -> E.unit)
        ; handle_store= (fun x -> E.(_when (snd x) _false)) } ]

(** {2 Fixpoint combinator} *)
type point = D : 'a A.t * ('a * Exponential.t) Place.t -> point

let y typ =
  let open Exponential in
  let g = GameOf.typ typ in
  let locations =
    List.map
      (fun (A.A a) -> D (a, new_loc ~show:(show_exp (A.show_msg a)) None))
      (Game.addresses g)
  in
  let make_positive (address, loc) =
    [ Tr.negative' ~label:(None, None) ~loc
        A.([L Value; R Argument; L Result] @:: address)
    ; Tr.positive ~loc ~label:(None, None)
        ~f:(fun x -> E.(guard (undiamond (unleft (snd x))) (fst x)))
        A.([L Value; L Result] @:: address)
    ; Tr.positive ~loc ~label:(None, None)
        ~f:(fun x ->
          let a, b = unpair (unright (E.snd x)) in
          E.(pair (pair (fst x) a) b) )
        A.([L Value; R Argument; R Argument] @:: address) ]
  in
  let make_negative (address, loc) =
    [ Tr.negative ~loc ~label:(None, None)
        ~f:(fun x -> E.pair x (left (diamond ())))
        A.([L Value; L Result] @:: address)
    ; Tr.negative ~loc ~label:(None, None)
        ~f:(fun x ->
          let v = E.fst (E.fst x)
          and e1 = E.(snd (fst x))
          and e2 = E.(snd x) in
          E.pair v (right (pair e1 e2)) )
        A.([L Value; R Argument; R Argument] @:: address)
    ; Tr.positive' ~label:(None, None) ~loc
        A.([L Value; R Argument; L Result] @:: address) ]
  in
  { locations= List.map (fun (D (_, loc)) -> Place.L loc) locations
  ; transitions=
      List.concat_map
        (fun (D (addr, loc)) ->
          if Address.polarity (A.A addr) = Net.Polarity.Positive then
            make_positive (addr, loc)
          else make_negative (addr, loc) )
        locations }

(** {2 Conditionals} *)
let _if (D.D d) =
  let condret = new_loc ~show:(show_dt D.Bool) None in
  let readyret = new_loc ~show:(show_dt d) None in
  let locations = Place.[L condret; L readyret] in
  let transitions =
    [ Tr.negative' ~label:(None, None) ~loc:condret
        A.([L LContext; L Left] @:: answer D.Bool)
    ; Tr.positive ~loc:condret ~label:(None, None)
        ~f:(fun x -> E._when x E.unit)
        A.([L LContext; L Right; L Left] @:: question)
    ; Tr.positive ~loc:condret ~label:(None, None)
        ~f:(fun x -> E.(_unless x unit))
        A.([L LContext; L Right; L Right] @:: question)
    ; Tr.negative' ~loc:readyret
        A.([L LContext; L Right; L Left] @:: answer d)
        ~label:(None, None)
    ; Tr.negative' ~loc:readyret
        A.([L LContext; L Right; L Right] @:: answer d)
        ~label:(None, None)
    ; Tr.positive' ~label:(None, None) ~loc:readyret
        A.([L Value] @:: answer d) ]
  in
  {transitions; locations}
  @@@ flow' ~show:show_q
        ~source:A.([L Value] @:: question)
        ~target:A.([L LContext; L Left] @:: question)
        ()

(** {2 Assignment} *)
let assign () =
  flow' ~show:show_q
    ~source:A.([L Value] @:: question)
    ~target:A.([L LContext; L Right] @:: question)
    ()
  @@@ flow' ~show:(show_dt D.Nat)
        ~source:A.([L LContext; L Right] @:: answer D.Nat)
        ~target:A.([L LContext; L Left; L Write] @:: question' D.Nat)
        ()
  @@@ flow' ~show:(show_dt D.Unit)
        ~source:A.([L LContext; L Left; L Write] @:: answer D.Unit)
        ~target:A.([L Value] @:: answer D.Unit)
        ()

(** {2 Binary operators} *)
type ('a, 'b, 'c) bin_op =
  {d1: 'a D.t; d2: 'b D.t; result: 'c D.t; f: ('a * 'b, 'c) E.func}

type ('a, 'b) un_op = {d: 'a D.t; result: 'b D.t; f: ('a, 'b) E.func}

type op =
  | Bin : ('a, 'b, 'c) bin_op -> op
  | Un : ('a, 'b) un_op -> op
  | Other : (unit -> TNet.t) -> op

let dt_of_term term =
  match term.Syntax.Term.data with
  | Typ.Base Int -> D.(D Nat)
  | Typ.Base Bool -> D.(D Bool)
  | _ -> D.(D Unit)

let sem_op ts = function
  | Syntax.Term.Add -> Bin {d1= D.Nat; d2= Nat; result= D.Nat; f= O.add}
  | Syntax.Term.Sub -> Bin {d1= D.Nat; d2= Nat; result= D.Nat; f= O.sub}
  | Syntax.Term.Mult -> Bin {d1= D.Nat; d2= Nat; result= D.Nat; f= O.mul}
  | Syntax.Term.Div -> Bin {d1= D.Nat; d2= Nat; result= D.Nat; f= O.div}
  | Syntax.Term.Equal -> Bin {d1= D.Nat; d2= Nat; result= D.Bool; f= O.eq}
  | Syntax.Term.Par ->
      let (D d2) = dt_of_term (List.nth ts 1) in
      Bin {d1= D.Unit; d2; result= d2; f= (fun x -> E.snd x)}
  | Syntax.Term.And ->
      Bin
        { d1= D.Bool
        ; d2= Bool
        ; result= D.Bool
        ; f= (fun x -> E.(_when (fst x) (snd x))) }
  | Syntax.Term.Or ->
      Bin
        { d1= D.Bool
        ; d2= Bool
        ; result= D.Bool
        ; f= (fun x -> E.(_if (fst x) ~_then:(snd x) ~_else:E._false)) }
  | Syntax.Term.Not -> Un {d= D.Bool; result= D.Bool; f= E.not}
  | Syntax.Term.Succ -> Un {d= D.Nat; result= D.Nat; f= O.succ}
  | Syntax.Term.Pred -> Un {d= D.Nat; result= D.Nat; f= O.pred}
  | Syntax.Term.Iszero -> Un {d= D.Nat; result= D.Bool; f= O.iszero}
  | Syntax.Term.Assign -> Other assign
  | Term.Deref ->
      let fw g =
        Fw
          { left= A.([L LContext; L Read] @:: g)
          ; right= A.([L Value] @:: g)
          ; leftToRight= (fun x -> x)
          ; rightToLeft= (fun x -> x)
          ; tlbl= (None, None)
          ; llbl= None }
      in
      Other (fun () -> TNet.forwarder {fw} (GameOf.typ Typ.(BaseA (Int, -1))))
  | Grab ->
      let fw g =
        Fw
          { left= A.([L LContext; L Grab] @:: g)
          ; right= A.([L Value] @:: g)
          ; leftToRight= (fun x -> x)
          ; rightToLeft= (fun x -> x)
          ; tlbl= (None, None)
          ; llbl= None }
      in
      Other
        (fun () -> TNet.forwarder {fw} (GameOf.typ Typ.(BaseA (Unit, -1))))
  | Release ->
      let fw g =
        Fw
          { left= A.([L LContext; L Release] @:: g)
          ; right= A.([L Value] @:: g)
          ; leftToRight= (fun x -> x)
          ; rightToLeft= (fun x -> x)
          ; tlbl= (None, None)
          ; llbl= None }
      in
      Other
        (fun () -> TNet.forwarder {fw} (GameOf.typ Typ.(BaseA (Unit, -1))))

let binary_operator_seq b =
  let askleft = new_loc ~show:show_q None in
  let askright = new_loc ~show:show_q None in
  let collectleft = new_loc ~show:(show_dt b.d1) None in
  let collectright = new_loc ~show:(show_dt b.d2) None in
  let transitions =
    [ Tr.negative' ~loc:askleft A.([L Value] @:: question) ~label:(None, None)
    ; Tr.positive' ~loc:askleft
        A.([L LContext; L Left] @:: question)
        ~label:(None, None)
    ; Negative.(
        make
          ~addr:A.([L LContext; L Left] @:: answer b.d1)
          ~label:(None, None)
          ~f:(fun x -> askright @:= x ||| collectleft @:= x))
    ; Tr.positive ~loc:askright ~label:(None, None)
        ~f:(fun _ -> E.unit)
        A.([L LContext; L Right] @:: question)
    ; Tr.negative' ~loc:collectright
        A.([L LContext; L Right] @:: answer b.d2)
        ~label:(None, None)
    ; Positive.(
        make
          ~addr:A.([L Value] @:: answer b.result)
          ~label:(None, None)
          ~f:
            (let* x = L collectleft and* y = L collectright in
             E.apply b.f (E.pair x y) )) ]
  in
  { locations= Place.[L askleft; L askright; L collectleft; L collectright]
  ; transitions }

let binary_operator_par b =
  let askleft = new_loc ~show:show_q None in
  let askright = new_loc ~show:show_q None in
  let collectleft = new_loc ~show:(show_dt b.d1) None in
  let collectright = new_loc ~show:(show_dt b.d2) None in
  let transitions =
    [ Negative.(
        make
          ~f:(fun x -> askleft @:= x ||| askright @:= x)
          ~addr:A.([L Value] @:: question)
          ~label:(None, None))
    ; Tr.positive' ~loc:askleft
        A.([L LContext; L Left] @:: question)
        ~label:(None, None)
    ; Tr.negative' ~loc:collectleft
        A.([L LContext; L Left] @:: answer b.d1)
        ~label:(None, None)
    ; Tr.positive' ~loc:askright
        A.([L LContext; L Right] @:: question)
        ~label:(None, None)
    ; Tr.negative' ~loc:collectright
        A.([L LContext; L Right] @:: answer b.d2)
        ~label:(None, None)
    ; Positive.(
        make
          ~addr:A.([L Value] @:: answer b.result)
          ~label:(None, None)
          ~f:
            (let* x = L collectleft and* y = L collectright in
             E.(apply b.f (pair x y)) )) ]
  in
  { locations= Place.[L askleft; L askright; L collectleft; L collectright]
  ; transitions }

let unary_operator o =
  flow' ~show:show_q
    ~source:A.([L Value] @:: question)
    ~target:A.([L LContext] @:: question)
    ()
  @@@ flow ~show:(show_dt o.result)
        ~source:A.([L LContext] @:: answer o.d)
        ~target:A.([L Value] @:: answer o.result)
        ~f:o.f ()

(** {3 Promotion} *)
module Prom = struct
  type item = I : ('a Place.t * ('a * Exponential.t) Place.t) -> item

  let items = ref []

  let upcast : type a. a Place.t -> (a * Exponential.t) Place.t =
   fun loc ->
    let find_map (I (l1, l2)) : (a * Exponential.t) Place.t option =
      match Place.equal l1 loc with
      | Some Net.Poly.Eq.E -> Some l2
      | None -> None
    in
    match List.find_map find_map !items with
    | Some l -> l
    | None ->
        let l' = Place.make ~show:(show_exp (Place.show loc)) () in
        items := I (loc, l') :: !items ;
        l'

  let contract :
      type x a b c. (x, a * c) E.t -> (x, b * c) E.t -> (x, (a * b) * c) E.t
      =
   fun e1 e2 ->
    E.(_when (snd e1 == snd e2) (pair (pair (fst e1) (fst e2)) (snd e1)))

  (** Promoting pre condition. We take a pre condition of type [α] and we
      return an new precondition [β] along with an expression β → α × Exp for
      some β.

      Concretely, if α = α_1 … α_n, β = (α_1 × Exp) … (α_n × Exp). *)
  type 'a pre = Pre : 'b Cond.t * ('b, 'a * Exponential.t) E.t -> 'a pre

  let rec promote_pre_aux : type a. a Cond.t -> a pre = function
    | Cond.L loc -> Pre (Cond.L (upcast loc), E.input)
    | CC (x, y) ->
        let (Pre (c1, e1)) = promote_pre_aux x in
        let (Pre (c2, e2)) = promote_pre_aux y in
        Pre
          ( CC (c1, c2)
          , contract
              (E.compose (E.fst E.input) e1)
              (E.compose (E.snd E.input) e2) )

  let promote_pre pre e =
    let (Pre (pre, f)) = promote_pre_aux pre in
    let x = E.compose f (E.lift_left e) in
    Pre (pre, x)

  (** Promotion of post condition, same story *)
  type 'a post = Post : 'b Cond.t * ('a, 'b) E.t -> 'a post

  let rec promote_post :
      type a b. b Cond.t -> (a, b * Exponential.t) E.t -> a post =
   fun post exp ->
    match post with
    | Cond.L loc -> Post (Cond.L (upcast loc), exp)
    | CC (x, y) ->
        let (Post (p1, e1)) =
          promote_post x E.(pair (fst (fst exp)) (snd exp))
        in
        let (Post (p2, e2)) =
          promote_post y E.(pair (snd (fst exp)) (snd exp))
        in
        Post (CC (p1, p2), E.pair e1 e2)

  let promote net =
    let promote_transition (Transition.T t) =
      match t.cond with
      | Positive x -> (
        match x.addr with
        | A.(Addr (L Value :: is, k)) ->
            let (Pre (pre, func)) = promote_pre x.pre t.func in
            let addr = A.(Addr (R PromotedValue :: is, k)) in
            Transition.T {t with func; cond= Positive {addr; pre}}
        | A.(Addr (R Context :: is, k)) ->
            let (Pre (pre, func)) = promote_pre x.pre t.func in
            let func =
              (func : (_, (_ * Exponential.t) * Exponential.t) E.t)
            in
            let func =
              E.(
                pair
                  (fst (fst func))
                  (Exponential.pair (snd (fst func)) E.(snd func)))
            in
            let addr = A.(Addr (R Context :: is, k)) in
            Transition.T {t with func; cond= Positive {addr; pre}}
        | _ -> assert false )
      | Negative x -> (
        match x.addr with
        | A.(Addr (L Value :: is, k)) ->
            let (Post (post, func)) =
              promote_post x.post (E.lift_left t.func)
            in
            let addr = A.(Addr (R PromotedValue :: is, k)) in
            Transition.T {t with func; cond= Negative {addr; post}}
        | A.(Addr (R Context :: is, k)) ->
            let (Post (post, func)) =
              promote_post x.post (E.lift_left t.func)
            in
            let func =
              (func : ((_ * Exponential.t) * Exponential.t, _) E.t)
            in
            let f x =
              let a, b = Exponential.unpair E.(snd x) in
              E.(pair (pair (fst x) a) b)
            in
            let func = E.compose (f E.input) func in
            let addr = A.(Addr (R Context :: is, k)) in
            Transition.T {t with func; cond= Negative {addr; post}}
        | _ -> assert false )
      | Neutral x ->
          let (Pre (pre, f)) = promote_pre_aux x.pre in
          let f' = E.compose f (E.lift_left t.func) in
          let (Post (post, func)) = promote_post x.post f' in
          Transition.T {t with cond= Neutral {pre; post}; func}
    in
    let locations =
      List.map (fun (Place.L x) -> Place.L (upcast x)) net.locations
    in
    let transitions = List.map promote_transition net.transitions in
    {locations; transitions}
end

let promote = Prom.promote

(** {2 Contraction} *)

let contraction g =
  let lift_right f x = E.(pair (fst x) (f (snd x))) in
  let iter (A.A address) =
    let loc = new_loc ~show:(show_exp (A.show_msg address)) None in
    if Address.polarity (A.A address) = Negative then
      ( Place.L loc
      , [ Tr.negative ~label:(None, None) ~loc
            ~f:(lift_right Exponential.left)
            A.([R PromotedValue; L Left] @:: address)
        ; Tr.negative ~label:(None, None) ~loc
            ~f:(lift_right Exponential.right)
            A.([R PromotedValue; L Right] @:: address)
        ; Tr.positive' ~label:(None, None) ~loc A.([R Context] @:: address)
        ] )
    else
      ( Place.L loc
      , [ Tr.negative' ~label:(None, None) ~loc A.([R Context] @:: address)
        ; Tr.positive ~label:(None, None) ~loc
            ~f:(lift_right Exponential.unleft)
            A.([R PromotedValue; L Left] @:: address)
        ; Tr.positive ~label:(None, None) ~loc
            ~f:(lift_right Exponential.unright)
            A.([R PromotedValue; L Right] @:: address) ] )
  in
  let locations, transitions =
    List.split (List.map iter (Game.addresses g))
  in
  {locations; transitions= List.concat transitions}

let contracted_tensor context sigma tau =
  compose_bang (contraction (GameOf.context context)) (tensor sigma tau)

let operator ts op =
  match (op, sem_op ts op) with
  | Par, Bin b -> binary_operator_par b
  | _, Bin b -> binary_operator_seq b
  | _, Un b -> unary_operator b
  | _, Other f -> f ()

(** {1 Interpretation function} *)
type context_entry = Typ.annoty

type state = {context: (string * context_entry) list; typ: Typ.annoty}

let rec interpret (term : Typ.t Term.t) state : TNet.t =
  let open Syntax.Term in
  let annot typ = fst @@ Typ.annotate_typ (-1) typ in
  let add x typ state = {state with context= (x, typ) :: state.context} in
  let aux x state = interpret x {state with typ= annot x.data} in
  match term.constructor with
  | Let (_, x, loc, body, rest) when not @@ List.mem (fst x) (freevars rest)
    ->
      contracted_tensor state.context (aux body state) (interpret rest state)
      >>> seq body.loc rest.loc term.loc loc (dt_of_term body)
            (dt_of_term rest)
  | Let (_, x, _, body, rest) ->
      contracted_tensor state.context
        (lambda (fst x)
           (interpret rest (add (fst x) (annot body.data) state)) )
        (aux body state)
      >>> netlet (dt_of_term body) (dt_of_term rest)
  | Var s ->
      let data =
        try List.assoc s state.context
        with Not_found ->
          failwith
            (Printf.sprintf
               "Interpretation: Variable %s absent from\n        context" s )
      in
      var term.loc s data
  | Lambda (s, _, body) ->
      let domain, cod =
        match state.typ with
        | Typ.ArrowA (domain, cod) -> (domain, cod)
        | _ -> assert false
      in
      lambda s (interpret body {(add s domain state) with typ= cod})
  | Const c -> const term.loc c
  | App (t, u) ->
      let sigma, _ =
        match t.data with
        | Typ.Arrow (sigma, _) -> Typ.annotate_typ (-1) sigma
        | _ -> assert false
      in
      compose_bang
        (contraction (GameOf.context state.context))
        (app
           (interpret t {state with typ= Typ.ArrowA (sigma, state.typ)})
           (promote
              (interpret u
                 {state with typ= fst @@ Typ.annotate_typ (-1) u.data} ) ) )
  | If (_locs, t, u, v) ->
      contracted_tensor state.context (aux t state)
        (contracted_tensor state.context (interpret u state)
           (interpret v state) )
      >>> _if (dt_of_term u)
  | Operator (op, [x; y]) ->
      contracted_tensor state.context (aux x state) (aux y state)
      >>> operator [x; y] (fst op)
  | Operator (op, [x]) -> aux x state >>> operator [x] (fst op)
  | Operator (_, _) -> failwith "Operator with more than two arguments?"
  | Newref (x, m) ->
      lambda (fst x) (interpret m (add (fst x) Typ.(BaseA (Ref, -1)) state))
      >>> newref (dt_of_term m) x
  | Newsem (x, m) ->
      lambda (fst x) (interpret m (add (fst x) Typ.(BaseA (Sem, -1)) state))
      >>> newsem (dt_of_term m) x
  | Y -> (
    match state.typ with ArrowA (_, typ) -> y typ | _ -> assert false )

let interpret ({context; typ} : Interface.t) term =
  let init (x, t) = (x, t) in
  let net = interpret term {typ; context= List.map init context} in
  let module Opti = Net.Optimisation.Make (Sig) in
  let skippable _ = true in
  Opti.optim ~merge_label ~skippable net
