module type Kind = sig
  type 'a t

  val pol : 'a t -> Polarity.t

  val show : 'a t -> string

  val show_msg : 'a t -> 'a -> string * string

  val equal : 'a t -> 'b t -> ('a, 'b) Poly.Eq.t option
end

module type Sig = sig
  type linear

  type replicated

  type exponential

  module Kind : Kind

  val pol_linear : linear -> Polarity.t

  val pol_replicated : replicated -> Polarity.t

  val show_linear : linear -> string

  val show_replicated : replicated -> string

  val show_exponential : exponential -> string
end

module A = struct
  type 'a t = 'a Datatyp.t

  let pol _ = Polarity.Positive

  let show _ = ""

  let show_msg x y = (Datatyp.show_value x y, "")

  let equal = Datatyp.equal
end

module QA = struct
  type 'a t =
    | Question : 'a Datatyp.t -> 'a t
    | Answer : 'a Datatyp.t -> 'a t

  let pol : type a. a t -> Polarity.t = function
    | Question _ -> Polarity.Negative
    | Answer _ -> Polarity.Positive

  let show : type a. a t -> string = function
    | Question _ -> "eval"
    | Answer _ -> "return"

  let show_msg : type a. a t -> a -> string * string =
   fun k x ->
    match k with
    | Question Datatyp.Unit -> ("¿", "")
    | Answer Datatyp.Unit -> ("✓", "")
    | Question d -> ("¿ " ^ Datatyp.show_value d x, "")
    | Answer d -> (Datatyp.show_value d x, "")

  let equal : type a b. a t -> b t -> (a, b) Poly.Eq.t option =
   fun x y ->
    match (x, y) with
    | Question d, Question d' -> Datatyp.equal d d'
    | Answer d, Answer d' -> Datatyp.equal d d'
    | _, _ -> None
end

module Make (S : Sig) = struct
  type ('a, 'b) injection =
    | L : S.linear -> ('a, 'a) injection
    | R : S.replicated -> ('a * S.exponential, 'a) injection

  type ('a, 'b) injections =
    | [] : ('a, 'a) injections
    | ( :: ) :
        ('a, 'b) injection * ('b, 'c) injections
        -> ('a, 'c) injections

  type 'a t = Addr : ('a, 'b) injections * 'b S.Kind.t -> 'a t

  type _t = A : 'a t -> _t

  let show_injection : type a b. (a, b) injection -> string = function
    | L s -> S.show_linear s
    | R r -> S.show_replicated r

  let rec show_injections : type a b. (a, b) injections -> string = function
    | [] -> ""
    | t :: q -> show_injection t ^ "/" ^ show_injections q

  let show (A (Addr (is, k))) = show_injections is ^ S.Kind.show k

  let rec polarity_injections : type a b. (a, b) injections -> Polarity.t =
    function
    | [] -> Positive
    | L l :: rest ->
        Polarity.mult (S.pol_linear l) (polarity_injections rest)
    | R r :: rest ->
        Polarity.mult (S.pol_replicated r) (polarity_injections rest)

  let polarity (A (Addr (is, k))) =
    Polarity.mult (polarity_injections is) (S.Kind.pol k)

  let rec show_msg : type a. a t -> a -> string * string =
   fun (Addr (is, k)) value ->
    match (is, k) with
    | [], k -> S.Kind.show_msg k value
    | L _ :: rest, k -> show_msg (Addr (rest, k)) value
    | R _ :: rest, k ->
        let a, b = show_msg (Addr (rest, k)) (fst value) in
        (a, S.show_exponential (snd value) ^ " · " ^ b)

  let rec equal_injections :
      type a b c.
      (a, c) injections -> (b, c) injections -> (a, b) Poly.Eq.t option =
   fun is is' ->
    match (is, is') with
    | [], [] -> Some Poly.Eq.E
    | L i :: is, L i' :: is' when i = i' -> (
      match equal_injections is is' with
      | Some Poly.Eq.(E) -> Some Poly.Eq.(E)
      | _ -> None )
    | R i :: is, R i' :: is' when i = i' -> (
      match equal_injections is is' with
      | Some Poly.Eq.(E) -> Some Poly.Eq.(E)
      | None -> None )
    | _, _ -> None

  let equal : type a b. a t -> b t -> (a, b) Poly.Eq.t option =
   fun (Addr (is, k)) (Addr (is', k')) ->
    match S.Kind.equal k k' with
    | None -> None
    | Some Poly.Eq.E -> (
      match equal_injections is is' with
      | Some Poly.Eq.E -> Some Poly.Eq.E
      | None -> None )

  let rec destruct : type a b. (a, b) injections -> a t -> b t option =
   fun is (Addr (is', k')) ->
    match (is, is') with
    | [], is' -> Some (Addr (is', k'))
    | L i :: is, L i' :: is' ->
        if i = i' then destruct is (Addr (is', k')) else None
    | R i :: is, R i' :: is' ->
        if i = i' then destruct is (Addr (is', k')) else None
    | _, _ -> None

  let rec construct_ : type a b. (a, b) injections -> _t -> _t =
   fun x y ->
    match x with
    | [] -> y
    | L l :: is ->
        let (A (Addr (is, k))) = construct_ is y in
        A (Addr (L l :: is, k))
    | R l :: is ->
        let (A (Addr (is, k))) = construct_ is y in
        A (Addr (R l :: is, k))

  let rec construct : type a b. (a, b) injections -> b t -> a t =
   fun x y ->
    match x with
    | [] -> y
    | L l :: is ->
        let (Addr (is, k)) = construct is y in
        Addr (L l :: is, k)
    | R l :: is ->
        let (Addr (is, k)) = construct is y in
        Addr (R l :: is, k)

  let ( @:: ) c e = construct c e

  type rule = S.linear list * S.linear list

  let ( --> ) a b = (a, b)

  let rec to_injs : S.linear list -> ('a, 'a) injections = function
    | [] -> []
    | l :: ls -> L l :: to_injs ls

  let apply_one : type a. S.linear list * S.linear list -> a t -> a t option
      =
   fun (d, c) a ->
    match destruct (to_injs d) a with
    | Some a -> Some (construct (to_injs c) a)
    | _ -> None

  let rec apply (rules : rule list) a =
    match rules with
    | [] -> None
    | f :: fs -> (
      match apply_one f a with Some x -> Some x | None -> apply fs a )
end
