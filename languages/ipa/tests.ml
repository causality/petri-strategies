module D = Net.Datatyp
module Test = Common.Test (Language)

let init = (None, [])

let is_int k (Sig.Move.M (addr, value)) =
  match addr with
  | Addr ([L Value], Answer D.Nat) ->
      if k = (value : int) then Ok ()
      else Error (Printf.sprintf "Expected integer %d got %d" k value)
  | _ -> Error (Printf.sprintf "Expected integer %d" k)

let test ~code ~pred =
  Test.test ~code ~pred
    ~input:(Sig.Move.M (Sig.Address.(Addr ([L Value], Question D.Unit)), ()))

let det_returns code p = test ~code ~pred:(Test.check_runs [Test.returns p])

let returns code ps =
  test ~code ~pred:(Test.check_runs (List.map Test.returns ps))

let det_terminates code =
  test ~code ~pred:(Test.check_runs [Test.terminates])

let run () =
  Test.run_tests
    [ det_returns "2+1" (is_int 3)
    ; det_returns "1 + (3 * 4)" (is_int 13)
    ; det_terminates "() | ()"
    ; det_returns "(fun a b -> a + b) 2 3" (is_int 5)
    ; det_returns "(fun f -> f 1 + f 2) (fun x -> succ x)" (is_int 5)
    ; returns "newref r in (r := 1) | !r" [is_int 1; is_int 0]
    ; det_returns "(fun c -> c + c) (newref r in (r := 2); !r)" (is_int 4)
    ; det_returns
        "Y (fun fact -> fun n -> if iszero n then 1 else n * fact (pred n)) \
         2"
        (is_int 2)
    ; det_terminates
        "(fun x y -> newsem s in\n\
        \  grab(s); (x; release(s) |\n\
        \   grab(s); y;  release(s))) () ()" ]
