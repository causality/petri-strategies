(** Managing the interaction with the code. *)

open Net

(** This module implements the main bits of the interface to interact with a
    code and its interpretation as a Petri net. *)

(** Most of the code is parametrised by a language interpretation *)
module Make (Lang : Common.Language) : sig
  module TNet := TNet.Make(Lang.Sig)
  module Run := Run.Make(Lang.Sig)
  module Game := Game.Make(Lang.Sig)
  module D := Net.Datatyp
  open TNet

  type selection =
    | NoSelection : selection
    | Token : Run.Availability.t -> selection
    | Justifier :
        { parent_addr: 'b Lang.Sig.Address.t
        ; addr: 'a Lang.Sig.Address.t
        ; tr: Transition._t
        ; f: 'a -> Run.Event._t
        ; justifiers: 'b list }
        -> selection
    | Data : 'b D.t * ('b -> t) -> selection

  and t =
    { net: TNet.t
    ; selection: selection
    ; interface: Lang.Interface.t
    ; game: Game.t
    ; marking: Run.Marking.t
    ; run: Run.t
    ; mode: bool }

  type elt := Html_types.div_content H.elt

  (** Update function *)
  type upd := t -> unit

  val tooltip :
    upd -> t -> (string * Html_types.div_content H.elt list) option

  val draw_code : upd -> string -> t -> elt

  val draw_net : upd -> t -> (elt -> 'r) -> unit

  val draw_run : upd -> t -> (elt -> 'r) -> unit

  val interpret : string -> t

  val reverse_mode : t -> t
end
