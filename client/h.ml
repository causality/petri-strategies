(* A library of HTML widgets. *)
open Net
open Js_of_ocaml
open Js_of_ocaml_tyxml.Tyxml_js
include Js_of_ocaml_tyxml.Tyxml_js.Html
module SVG = Js_of_ocaml_tyxml.Tyxml_js.Svg
module Cast = To_dom
module CastSVG = Js_of_ocaml_tyxml.Tyxml_js.Svg

module Node = struct
  let reset x =
    let x = (x :> Dom.node Js.t) in
    let rec aux () =
      Js.Opt.case x##.firstChild
        (fun () -> ())
        (fun y ->
          ignore (x##removeChild y) ;
          aux () )
    in
    aux ()

  let add_child node child = ignore @@ node##appendChild (Cast.of_node child)

  let add_children node children = List.iter (add_child node) children

  let set_children node children =
    reset node ;
    add_children node children

  let set_content node children =
    reset (Cast.of_element node) ;
    add_children (Cast.of_element node) children

  let make maker attrs children =
    let node = maker Dom_svg.document in
    List.iter
      (fun (name, value) ->
        node##setAttribute (Js.string name) (Js.string value) )
      attrs ;
    List.iter (fun child -> ignore @@ node##appendChild child) children ;
    node

  let pcdata s =
    (Dom_svg.document##createTextNode (Js.string s) :> Dom.node Js.t)
end

let i = ref 0

let fix f =
  let rec fixpoint = lazy (f fixpoint) in
  Lazy.force fixpoint

let new_id () =
  incr i ;
  "_" ^ string_of_int !i

let empty = Unsafe.data ""

let when_def x f = match x with None -> empty | Some a -> f a

let guard b e = if b then e else empty

let guard' b e = if b then e else []

let concat sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  aux (List.filter (fun x -> x <> empty) l)

let concat' sep l =
  let rec aux = function
    | [] -> []
    | [t] -> [t]
    | t :: q -> t :: sep () :: aux q
  in
  List.concat @@ aux (List.filter (fun x -> x <> []) l)

let set_class x cl = x##.className := Js.string cl

let add_class x cl = x##.classList##add (Js.string cl)

let get id =
  try Dom_html.getElementById id
  with _ -> failwith @@ Printf.sprintf "Element `%s` not found" id

let value elem = Js.to_string elem##.value

let combobox default list =
  let make_option (name, _) =
    if name = default then option ~a:[a_selected ()] (txt name)
    else option (txt name)
  in
  let value self = Js.to_string @@ (Cast.of_select self)##.value in
  let on_change self _ =
    let v = value (Lazy.force self) in
    List.assoc v list () ; true
  in
  let default_option =
    if List.mem_assoc default list then []
    else
      [ option
          ~a:[a_value ""; a_disabled (); a_selected (); a_hidden ()]
          (txt default) ]
  in
  let options = default_option @ List.map make_option list in
  let combobox =
    fix (fun self ->
        select ~a:[a_onchange (on_change self); a_required ()] options )
  in
  ( (combobox : Html_types.select elt :> [> Html_types.select] elt)
  , fun () -> value combobox )

module Dot = struct
  let find tagName node =
    let rec go sibling =
      Js.Opt.case sibling
        (fun _ -> None)
        (fun sibling ->
          if sibling##.nodeName = tagName then Some sibling
          else go sibling##.nextSibling )
    in
    go node##.firstChild

  let onclick f elem value =
    elem##.onclick := Dom.handler (fun _ -> f value ; Js._false)

  let update_text elem f =
    match find (Js.string "text") elem with
    | None -> ()
    | Some elem -> Node.reset elem ; f elem

  let remove elem name =
    match find (Js.string name) (elem :> Dom.node Js.t) with
    | None -> ()
    | Some x -> ignore @@ elem##removeChild x

  let add_child c elem _value = elem##appendChild (c :> Dom.node Js.t)

  let title show elem value =
    let () = remove elem "title" in
    let title = Dom_svg.createTitle Dom_svg.document in
    ignore @@ title##appendChild (Node.pcdata (show value)) ;
    ignore @@ elem##appendChild (title :> Dom.node Js.t)

  let text show elem value =
    let () = remove elem "text" in
    let text = Dom_svg.createTextElement Dom_svg.document in
    show value text ;
    ignore @@ elem##appendChild (text :> Dom.node Js.t)

  let rec find_by_id id (node : Dom.element Js.t) =
    if node##getAttribute (Js.string "id") = id then Some node
    else
      (* We need the Obj.magic since childNodes is just a list of nodes, but
         we need a list of elements. *)
      List.find_map (find_by_id id)
        (Dom.list_of_nodeList
           (Obj.magic node##.childNodes : Dom.element Dom.nodeList Js.t) )

  let draw ?(filter = fun _ _ -> ()) dot k =
    let dot_source = Dot.to_string dot in
    let draw_dot_function = Js.Unsafe.pure_js_expr "drawDot" in
    let after_drawn (svg_element : Dom_html.element Js.t) =
      let rec walk (element : Dom.node Js.t) =
        Js.Opt.case
          (Dom_svg.CoerceTo.element element)
          (fun () -> ())
          (fun element ->
            let id = Js.to_string element##.id in
            match
              List.find_opt (fun node -> Dot.Node.name node = id) dot.nodes
            with
            | None ->
                List.iter
                  (fun n -> walk n)
                  (Dom.list_of_nodeList element##.childNodes)
            | Some node ->
                filter
                  (Obj.magic element : Dom_html.element Js.t)
                  (Dot.Node.label node) )
      in
      walk (svg_element :> Dom.node Js.t) ;
      k (Obj.magic svg_element : [> Html_types.svg] elt)
    in
    ( Js.Unsafe.(
        fun_call draw_dot_function
          [| inject @@ Js.string dot_source
           ; inject @@ Js.wrap_callback after_drawn |])
      : unit )
end

let button f s = a ~a:[a_onclick f; a_class ["button"]] [txt s]

module Form = struct
  type 'a t = Html_types.div_content elt list * (unit -> 'a)

  let map f (h, get) = (h, fun () -> f (get ()))

  let prefix l (h, get) = (l @ h, get)

  let list forms =
    let get () = List.map (fun (_, f) -> f ()) forms in
    let h = [ul (List.map (fun (x, _) -> li x) forms)] in
    (h, get)

  let combine prompt choices =
    let span = div [] in
    let make_option (name, form) =
      (name, fun () -> Node.set_content span (fst (Lazy.force form)))
    in
    match choices with
    | [(s, (form : 'a t Lazy.t))] ->
        let h, get = Lazy.force form in
        (h, fun () -> (s, get ()))
    | _ ->
        let h, get = combobox prompt (List.map make_option choices) in
        ( [(h :> Html_types.div_content elt); span]
        , fun () ->
            let s = get () in
            (s, snd (Lazy.force @@ List.assoc s choices) ()) )
end
