(** Conditions, i.e. non-empty list locations *)

(** This module represents conditions as non-empty list of locations tracking
    the over all type. For instance for a [int Place.t] plus a
    [string Place.t], the resulting condition has type [string * int]. *)
type _ t = L : 'a Place.t -> 'a t | CC : 'a t * 'b t -> ('a * 'b) t

(** Existential quantification over the type of tokens *)
type _t = C : 'a t -> _t

val ( ++ ) : 'a t -> 'b t -> ('a * 'b) t
(** Concatenation of conditions *)

val length : 'a t -> int
(** Length of a condition *)

val locs : 'a t -> Place._t list
(** Return the list of locations of a condition, forgetting their type. *)

val ids : 'a t -> int list
(** Returns the list of location ids of a condition *)

val mem : 'a Place.t -> 'b t -> bool
(** [mem loc cond] returns true when [loc] belongs to [cond] *)

val show_in : 'a t -> 'a -> string
(** Show a value for a particular condition *)

val lookup : 'a t -> 'a -> 'b Place.t -> 'b option
(** [lookup cond v loc] looks up the value of location [loc] in the value [v]
    of condition [cond]. If [loc] is not part of [cond], returns None. For
    instance [lookup (L loc1 ++ L loc2) (3, 2) loc1 = Some 3] *)

val union : _t option -> _t option -> _t option
(** This computes the union of two partial conditions (that one can see as
    potentially empty lists of locations *)
