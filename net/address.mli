module type Kind = sig
  type 'a t

  val pol : 'a t -> Polarity.t

  val show : 'a t -> string

  val show_msg : 'a t -> 'a -> string * string

  val equal : 'a t -> 'b t -> ('a, 'b) Poly.Eq.t option
end

module type Sig = sig
  type linear

  type replicated

  type exponential

  module Kind : Kind

  val pol_linear : linear -> Polarity.t

  val pol_replicated : replicated -> Polarity.t

  val show_linear : linear -> string

  val show_replicated : replicated -> string

  val show_exponential : exponential -> string
end

module QA : sig
  type 'a t =
    | Question : 'a Datatyp.t -> 'a t
    | Answer : 'a Datatyp.t -> 'a t

  include Kind with type 'a t := 'a t
end

module A : Kind with type 'a t = 'a Datatyp.t

module Make (S : Sig) : sig
  type ('a, 'b) injection =
    | L : S.linear -> ('a, 'a) injection
    | R : S.replicated -> ('a * S.exponential, 'a) injection

  type ('a, 'b) injections =
    | [] : ('a, 'a) injections
    | ( :: ) :
        ('a, 'b) injection * ('b, 'c) injections
        -> ('a, 'c) injections

  type 'a t = Addr : ('a, 'b) injections * 'b S.Kind.t -> 'a t

  type _t = A : 'a t -> _t

  val show : _t -> string

  val show_msg : 'a t -> 'a -> string * string

  val polarity : _t -> Polarity.t

  val equal : 'a t -> 'b t -> ('a, 'b) Poly.Eq.t option

  val construct : ('a, 'b) injections -> 'b t -> 'a t

  val construct_ : ('a, 'b) injections -> _t -> _t

  val destruct : ('a, 'b) injections -> 'a t -> 'b t option

  val ( @:: ) : ('a, 'b) injections -> 'b t -> 'a t

  type rule

  val ( --> ) : S.linear list -> S.linear list -> rule

  val to_injs : S.linear list -> ('a, 'a) injections

  val apply : rule list -> 'a t -> 'a t option
end
