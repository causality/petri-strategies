(** Signature for Petri nets *)

(** This define the interface that a user must provide in order to
    instanciate the {!tNet} module *)

module type S = sig
  (** Label for locations. *)
  module LocLabel : sig
    type t

    val show : t -> string
  end

  module TransLabel : sig
    (** Label for transitions *)
    type t

    val show : t -> string
  end

  (** Addresses for visible actions *)
  module Address : sig
    (** The type of addresses for visible actions. These denote addresses for
        interaction with the context. A ['a address] denotes a point of
        communication with the context where values of types ['a] are
        expected to be exchanged. *)
    type 'a t

    type _t = A : 'a t -> _t

    val show : _t -> string

    val polarity : _t -> Polarity.t

    val parent : _t -> _t -> bool
    (** [parent m m'] returns true if [m] is the parent of [m'] *)

    val equal : 'a t -> 'b t -> ('a, 'b) Poly.Eq.t option

    val show_msg : 'a t -> 'a -> string * string
  end

  (** [sdep m m'] returns true if m ≤ m' in the game. *)
  module Move : sig
    type t = M : 'a Address.t * 'a -> t

    val sdep : t -> t -> bool
  end
end
