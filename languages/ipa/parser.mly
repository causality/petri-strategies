%{
    open Syntax
    open Term
    let make loc constructor = { Term.constructor; loc; data = (); }, loc
    let ( --> ) = Common.Loc.fromto'
    let binary_op t1 t2 op (_, l) =
      make (t1 --> t2) (Term.Operator ((op, l), [fst t1; fst t2]))
    let unary_op o t op =
      make (o --> t) (Term.Operator ((op, snd o), [fst t]))
      
    let rec make_lambda patterns prog = match patterns with
      | [] -> prog
      | ((var, typ), loc) :: q ->
         let term = make_lambda q prog in
         make ((var, loc) --> prog) (Term.Lambda (var, typ, fst term))
                  
  let seq l t1 t2 =
    make (t1 --> t2) @@ Let (l, ("", Common.Loc.ghost), l, fst t1, fst t2)
      
%}

%token  <string*Common.Loc.t>  TK_ID			
%token  <bool*Common.Loc.t>	TK_BOOL
%token  <int*Common.Loc.t>  TK_INT
%token  <unit*Common.Loc.t> TK_PAR TK_LPAR TK_RPAR TK_SEMICOLON TK_PLUS TK_FUN TK_SET TK_IF TK_THEN TK_ELSE TK_ARROW TK_IN TK_UNIT TK_MULT TK_DIV TK_MINUS TK_LET TK_EQUAL TK_NEWREF TK_NEWSEM TK_Y TK_DEREF TK_GRAB TK_RELEASE TK_SUCC TK_PRED TK_ISZERO
%token EOF


%start program
%type <unit Syntax.Term.t * Common.Loc.t> program

%left TK_PAR
%left TK_SEMICOLON
%nonassoc TK_EQUAL
%nonassoc TK_SET
%left TK_PLUS TK_MINUS
%left TK_MULT TK_DIV
    
%%
const:
    | TK_INT { Term.Int (fst $1), snd $1}
   | TK_BOOL { Term.Bool (fst $1), snd $1 }
   | TK_UNIT { Term.Skip, snd $1 }
immediate:
   | const { make ($1 --> $1) (Term.Const (fst $1)) }
   | TK_ID { make ($1 --> $1) (Term.Var (fst $1)) }
   | TK_LPAR prog TK_RPAR { $2 }
   | TK_Y { make ($1 --> $1) Term.Y }
var:
   | TK_ID { (fst $1, None), snd $1 }
vars: list (var) { $1 }
       
app:
   | immediate { $1 }
   | app immediate { make ($1 --> $2) @@ Term.App (fst $1, fst $2) }
simple_exp:
   | TK_DEREF immediate { unary_op $1 $2 Term.Deref }
   | TK_GRAB immediate { unary_op $1 $2 Term.Grab }
   | TK_ISZERO immediate { unary_op $1 $2 Term.Iszero }
   | TK_RELEASE immediate { unary_op $1 $2 Term.Release }
   | TK_SUCC immediate { unary_op $1 $2 Term.Succ }
   | TK_PRED immediate { unary_op $1 $2 Term.Pred }
   (* binary ops *)
   | expression TK_PLUS expression { binary_op $1 $3 Term.Add $2 }
   | expression TK_MINUS expression { binary_op $1 $3 Term.Sub $2 }
   | expression TK_DIV expression { binary_op $1 $3 Term.Div $2 }
   | expression TK_MULT expression { binary_op $1 $3 Term.Mult $2 }
   | app { $1 }
expression:
   | simple_exp { $1 }
   | expression TK_SET expression { binary_op $1 $3 Term.Assign $2 }
   | expression TK_SEMICOLON expression { seq (snd $2) $1 $3 }
   | expression TK_EQUAL expression { binary_op $1 $3 Term.Equal $2 }
   | expression TK_PAR expression { binary_op $1 $3 Term.Par $2 }
prog:
   | expression { $1 }
   | TK_NEWSEM TK_ID TK_IN prog { make ($1 --> $4) (Term.Newsem ($2, fst $4)) }
   | TK_NEWREF TK_ID TK_IN prog { make ($1 --> $4) (Term.Newref ($2, fst $4)) }
   | TK_IF expression TK_THEN expression TK_ELSE expression
    { make ($1 --> $6) (Term.If ((snd $1, snd $3, snd $5), fst $2, fst $4, fst $6)) }
   | TK_FUN vars TK_ARROW prog
       { make_lambda $2 $4 }
   | TK_LET TK_ID vars TK_EQUAL prog TK_IN prog
       { make ($1 --> $7)
           (Term.Let (snd $1, $2, snd $6, fst @@ make_lambda $3 $5, fst $7)) }
program:
       prog EOF { $1 }
