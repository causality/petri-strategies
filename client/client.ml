open Js_of_ocaml

(** State of the interface *)

module type V = sig
  module Lang : Common.Language

  val view : View.Make(Lang).t
end

type sidebyside = (module V)

type t =
  { code: string  (** Code being evaluated *)
  ; tooltip: (string * Html_types.div_content H.elt list) option
        (** Content of the tooltip (for errors) *)
  ; view: view }

and view = Edit | SideBySide : sidebyside -> view

let state = ref {code= ""; tooltip= None; view= Edit}

let draw_function =
  ref (fun _ k -> k ([] : Html_types.div_content H.elt list))

let textarea = H.(textarea (txt ""))

let set_draw f = draw_function := f

let draw () =
  !draw_function !state (fun kids ->
      H.Node.set_children Dom_html.document##.body kids )

let import () =
  match !state.view with
  | SideBySide _ -> ()
  | Edit ->
      let value = (H.Cast.of_textarea textarea)##.value in
      state := {!state with code= Js.to_string value}

let export () =
  match !state.view with
  | SideBySide _ -> ()
  | Edit -> (H.Cast.of_textarea textarea)##.value := Js.string !state.code

let update_cont f =
  import () ;
  f !state (fun new_state ->
      state := new_state ;
      export () ;
      draw () )

let update ?(reset_tooltip = true) f =
  update_cont (fun state k ->
      try
        let state' =
          f (if reset_tooltip then {state with tooltip= None} else state)
        in
        k state'
      with
      | Common.Loc.Error (loc, s) ->
          let tooltip =
            [ H.txt s
            ; H.br ()
            ; Color.color ~cl:["indented"]
                ~locs:[(loc, [Color.Class "hl-error"])]
                state.code ]
          in
          k {state with tooltip= Some ("error", tooltip)}
      | Failure s ->
          k
            { state with
              tooltip=
                Some
                  ("error", [H.txt @@ Printf.sprintf "Fatal\n  error: %s" s])
            }
      | e ->
          k
            { state with
              tooltip=
                Some
                  ( "error"
                  , [ H.txt
                      @@ Printf.sprintf "Fatal error: %s"
                           (Printexc.to_string e) ] ) } )

let set_tooltip tooltip = update (fun st -> {st with tooltip= Some tooltip})

let set_error error = set_tooltip error

let fresh =
  let r = ref (-1) in
  fun () -> decr r ; !r

let lang_choice, get_lang =
  let names =
    List.map (fun (module L : Common.Language) -> L.name) Languages.languages
  in
  H.combobox (List.hd names) (List.map (fun n -> (n, fun () -> ())) names)

let interpret () =
  let module Lang =
  ( val List.find
          (fun (module L : Common.Language) -> L.name = get_lang ())
          Languages.languages )
  in
  update (fun state ->
      let module View = View.Make (Lang) in
      let module V = struct
        module Lang = Lang

        let view = View.interpret state.code
      end in
      {state with view= SideBySide (module V)} )

let edit_code code = update (fun state -> {state with code; view= Edit})

let load_example i code =
  Dom_html.window##.location##.hash := Js.string (string_of_int i) ;
  update (fun state -> {state with code}) ;
  interpret ()

let draw_state state k =
  let tooltip =
    let make (cl, content) =
      H.(div ~a:[a_id "tooltip"; a_class [cl]] content)
    in
    match (state.tooltip, state.view) with
    | Some x, _ -> make x
    | _, SideBySide (module V) ->
        let module Vi = View.Make (V.Lang) in
        let upd t =
          Printf.printf "Updating !!\n" ;
          let module V' = struct
            module Lang = V.Lang

            let view = t
          end in
          update (fun _ -> {state with view= SideBySide (module V')})
        in
        Option.(value ~default:H.empty @@ map make (Vi.tooltip upd V.view))
    | _ -> H.empty
  in
  let ret cl content =
    k
      H.
        [ h1
            ~a:[a_class ["title"]]
            [ txt "IPA to Petri nets [ "
            ; a ~a:[a_href "doc.html"] [txt "documentation"]
            ; txt " ]" ]
        ; tooltip
        ; div ~a:[a_id "root"; a_class [cl]] content ]
  in
  match state.view with
  | Edit ->
      ret "edit"
        [ textarea
        ; H.span
            [ lang_choice
            ; fst
              @@ H.combobox "Load an example"
                   (List.mapi
                      (fun i (s, s') -> (s, fun () -> load_example i s'))
                      Exs.list )
            ; H.button
                (fun _ -> interpret () ; false)
                "Compute the interpretation" ] ]
  | SideBySide (module V) ->
      let module View = View.Make (V.Lang) in
      let upd t =
        let module V' = struct
          module Lang = V.Lang

          let view = t
        end in
        update (fun _ -> {state with view= SideBySide (module V')})
      in
      View.draw_run upd V.view (fun augmentation ->
          View.draw_net upd V.view (fun content ->
              ret "view"
                H.
                  [ div ~a:[a_id "left"]
                      [ h2 [txt "Source"]
                      ; div
                          ~a:
                            [ a_id "code"
                              (* ; a_onclick (fun _ -> State.(edit_code
                                 state.code) ; true ) *) ]
                          [View.draw_code upd state.code V.view]
                      ; h2
                          [ txt
                              ( "Configuration on "
                              ^ V.Lang.Interface.show V.view.interface ) ]
                      ; div ~a:[a_class ["trace"]] [augmentation]
                      ; H.button
                          (fun _ -> edit_code state.code ; true)
                          "Edit the program"
                      ; H.button
                          (fun _ ->
                            upd (View.reverse_mode V.view) ;
                            true )
                          "Change mode" ]
                  ; div ~a:[a_id "right"]
                      [H.h2 [txt "Interpretation"]; content] ] ) )

let init _ =
  set_draw draw_state ;
  ( try
      let hash = Js.to_string Dom_html.window##.location##.hash in
      let n = int_of_string (String.sub hash 1 (String.length hash - 1)) in
      let _, ex = List.nth Exs.list n in
      load_example n ex
    with _ -> () ) ;
  draw () ; Js._true

let () = Dom_html.window##.onload := Dom_html.handler init
