type tm =
  | Const of int
  | Left of tm
  | Lam of string * tm
  | App of tm * tm
  | Var of string

type typ = Sum of typ * typ | Arr of typ * typ | Nat

(** 1. Monadic evaluator *)

open Net

type 'a exp = (unit, 'a) Expression.t

module MonadicEval (M : sig
  type 'a t

  val return : 'a -> 'a t

  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t
end) =
struct
  module NativeValue = struct
    type t = Sum of (bool * t) | Int of int exp | Fun of (t -> t M.t)
  end

  open M

  let rec eval : (string * NativeValue.t) list -> tm -> NativeValue.t M.t =
   fun env tm ->
    match tm with
    | Var s -> return (List.assoc s env)
    | Const n -> return (NativeValue.Int (Expression.int n))
    | Left t ->
        let* v = eval env t in
        return (NativeValue.Sum (true, v))
    | Lam (x, s) -> return (NativeValue.Fun (fun v -> eval ((x, v) :: env) s))
    | App (t, u) -> (
        let* f = eval env t in
        let* x = eval env u in
        match f with Fun f -> f x | _ -> assert false )
end

(** Our monad *)
module PiMonad (A : sig
  type 'a t
end) =
struct
  type 'a t =
    | Parallel : 'a t list -> 'a t
    | Guard : unit exp * 'a t -> 'a t
    | Output : 'a A.t * 'a exp -> 'b t
    | Alloc : ('a A.t -> 'b t) -> 'b t
    | Return : 'a -> 'a t
    | Input :
        {replicated: bool; safe: bool; chan: 'a A.t; k: 'a exp -> 'b t}
        -> 'b t

  let alloc : unit -> 'a A.t = assert false

  let return x = Return x

  let send chan v = Output (chan, v)

  let recv ?(safe = true) ?(replicated = true) i =
    Input {safe; replicated; chan= i; k= (fun x -> return x)}

  let bind : type a b. a t -> (a -> b t) -> b t = assert false

  let branches l = Parallel (List.map (fun (a, b) -> Guard (a, b)) l)

  let force e =
    branches
      (List.map (fun (g, e) -> (g, Return e)) (Expression.get_foreign e))

  let map f x = bind x (fun x -> Return (f x))

  let ( let* ) = bind
end

(** Values that are passed onto the network *)
module NetworkValue = struct
  type t = Sum of (bool * t) | Int of int | Fun

  let csum = Expression.Ctor.{name= "Sum"; i= (fun (x, y) -> Sum (x, y))}

  let cint = Expression.Ctor.{name= "Int"; i= (fun x -> Int x)}

  let cfun = Expression.Ctor.{name= "Fun"; i= (fun () -> Fun)}
end

(** Addresses for the CBV λ-calculus *)
module Address = struct
  type 'a t = A : {side: side; branches: node list} -> NetworkValue.t t

  and side = Result | Var of string

  and node = Call | Return

  let call (A c) = A {c with branches= c.branches @ [Call]}

  let return (A c) = A {c with branches= c.branches @ [Return]}
end

module Monad = PiMonad (Address)
module Evaluator = MonadicEval (Monad)
open Evaluator
open Monad

let ( let* ) = bind

(** Conversion between network and native values *)

(** Converting a native value into a network value *)
let rec network_of_native :
    typ -> NetworkValue.t Address.t -> NativeValue.t -> NetworkValue.t exp t
    =
 fun typ address value ->
  match (typ, value) with
  | Sum (sigma, tau), Sum (b, t) ->
      let* t = network_of_native (if b then sigma else tau) address t in
      return (Expression.construct2 NetworkValue.csum (Expression.bool b) t)
  | _, Int n -> return (Expression.construct NetworkValue.cint n)
  | Arr (sigma, tau), Fun f ->
      let server =
        let c = Address.call address in
        let* arg = recv c in
        let* v = native_of_network sigma (Address.call c) arg in
        let* result = f v in
        let* result = network_of_native tau (Address.return c) result in
        send (Address.return c) result
      in
      Parallel [server; return (Expression.construct0 NetworkValue.cfun)]
  | _ -> assert false

and native_of_network :
       typ
    -> NetworkValue.t Address.t
    -> NetworkValue.t exp
    -> Evaluator.NativeValue.t t =
 fun typ addr value ->
  match typ with
  | Nat ->
      return (NativeValue.Int (Expression.destruct NetworkValue.cint value))
  | Sum (sigma, tau) ->
      let b, t = Expression.destruct2 NetworkValue.csum value in
      branches
        [ ( Expression.untrue b
          , map (fun t -> NativeValue.Sum (true, t))
            @@ native_of_network sigma addr t )
        ; ( Expression.unfalse b
          , map (fun t -> NativeValue.Sum (false, t))
            @@ native_of_network tau addr t ) ]
  | Arr (sigma, tau) ->
      let closure argument =
        let* argument =
          network_of_native sigma (Address.call addr) argument
        in
        Parallel
          [ send (Address.call addr) argument
          ; (let* v = recv (Address.return addr) in
             native_of_network tau (Address.return addr) v ) ]
      in
      return (NativeValue.Fun closure)

let rec read_env : (string * typ) list -> (string * NativeValue.t) list t =
  function
  | [] -> return []
  | (variable, typ) :: rest ->
      let address = Address.A {side= Var variable; branches= []} in
      let* v = recv address in
      let* v = native_of_network typ address v in
      let* rest = read_env rest in
      return ((variable, v) :: rest)

(** Compute a π-term from a term *)
let eval_pi : (string * typ) list -> tm -> typ -> unit t =
 fun env tm typ ->
  let* env = read_env env in
  let address = Address.A {side= Result; branches= []} in
  let* v = eval env tm in
  let* v = network_of_native typ address v in
  send address v
