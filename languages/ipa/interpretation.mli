open Syntax

module Interface : sig
  type t = {context: (string * Typ.annoty) list; typ: Typ.annoty}

  val game_of : t -> Net.Game.Make(Sig).t

  val show : t -> string
end

val interpret : Interface.t -> Typ.t Term.t -> Net.TNet.Make(Sig).t
