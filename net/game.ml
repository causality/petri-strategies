module Make (S : Sig.S) = struct
  type mode = Linear | Bang of mode

  (* old game type type t = (Address.t * mode * datatyp) list *)

  type loc =
    { id: int
    ; addr: S.Address._t
    ; mode: mode
    ; mutable children: loc list
    ; parent: loc option }

  type t = loc list

  let rec game_to_list (g : t) =
    List.concat
      (List.map (fun l -> (l.addr, l.mode) :: game_to_list l.children) g)

  let ( ||| ) t u = t @ u

  let map f game =
    let rec map_aux f parent loc =
      let id, (addr, mode) = (loc.id, f (loc.addr, loc.mode)) in
      let node = {id; addr; mode; children= []; parent} in
      let children = List.map (map_aux f (Some node)) loc.children in
      node.children <- children ;
      node
    in
    List.map (map_aux f None) game

  let map_addresses f game = map (fun (x, mode) -> (f x, mode)) game

  let bang = map (fun (l, m) -> (l, Bang m))

  let addresses g = List.map (fun (a, _) -> a) (game_to_list g)

  let rec find_map f =
    List.find_map (fun loc ->
        match f loc with Some x -> Some x | None -> find_map f loc.children )

  let find f game =
    match find_map (fun x -> if f x then Some x else None) game with
    | None -> raise Not_found
    | Some loc -> loc

  let find_addr addr game =
    match
      find_map (fun x -> if x.addr = addr then Some x else None) game
    with
    | None ->
        Printf.printf "Could not find address %s\nAvailable addresses: %s\n"
          (S.Address.show addr)
          (String.concat ", " @@ List.map S.Address.show @@ addresses game) ;
        raise Not_found
    | Some loc -> loc

  let is_linear game addr =
    let loc = find_addr addr game in
    match loc.parent with None -> true | Some loc' -> loc.mode = loc'.mode

  let parent g addr =
    let loc = find_addr addr g in
    match loc.parent with Some x -> Some x.addr | None -> None

  let bang = map (fun (l, m) -> (l, Bang m))

  let empty = []

  let roots g = List.map (fun x -> x.addr) g

  let children g addr =
    let loc = find_addr addr g in
    List.map (fun x -> x.addr) loc.children
end
