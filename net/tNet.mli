(** Typed Petri nets *)

(** This module introduces our type for petri nets, where we use GADTs of
    OCaml to keep track of the type of the transition function. *)

(** Our construction is parametrised by an implementation of {!Sig.S}, which
    specifies labels for locations and transitions, the type for external
    tokens, among other things. *)
module Make (S : Sig.S) : sig
  (** {1 Transition of the net. *)
  module Game := Game.Make(S)

  (** We first define the transitions, using a GADT to track the arity and
      types of pre and post conditions. *)
  module Transition : sig
    (** To define transitions, we define the type of conditions. Indeed,
        transitions of different polarities have different kinds of
        condition: e.g. negative ones have only a set of post conditions,
        while neutral have both a set of pre- and post- conditions *)
    type ('input, 'output) cond =
      | Negative :
          {post: 'output Cond.t; addr: 'input S.Address.t}
          -> ('input, 'output) cond
          (** A negative transition has a list of post-conditions and an
              address *)
      | Positive :
          {pre: 'input Cond.t; addr: 'output S.Address.t}
          -> ('input, 'output) cond
          (** A positive transition has a list of pre-conditions and an
              address *)
      | Neutral :
          {pre: 'input Cond.t; post: 'output Cond.t}
          -> ('input, 'output) cond
          (** A neutral transition has no address but both sets of pre- and
              post- condition *)

    (** The type of conditions for transitions transforming input data
        ['input] into ['output]. The third type argument is a phantom type
        used to track polarity. *)

    type ('a, 'b) t =
      { id: int  (** Identifier of the transition *)
      ; func: ('a, 'b) Expression.t
            (** The expression describing the behaviour of the transition on
                colors *)
      ; cond: ('a, 'b) cond  (** Transition function *)
      ; label: S.TransLabel.t  (** Label *) }

    (** Existential quantification over input/output types of transitions *)
    type _t = T : ('a, 'b) t -> _t

    val polarity : ('a, 'b) t -> Polarity.t option

    val addr : ('a, 'b) t -> S.Address._t option

    (** {2 Simple aliases to create visible transitions with one pre/post} *)

    val negative :
         label:S.TransLabel.t
      -> f:('a, 'b) Expression.func
      -> loc:'b Place.t
      -> 'a S.Address.t
      -> _t
    (** Create a negative transition with a label, a transition function, the
        post location and the address *)

    val negative' :
      label:S.TransLabel.t -> loc:'a Place.t -> 'a S.Address.t -> _t
    (** Same as {negative} but where the transition function is the identity. *)

    val positive :
         label:S.TransLabel.t
      -> f:('a, 'b) Expression.func
      -> loc:'a Place.t
      -> 'b S.Address.t
      -> _t
    (** Create a positive transition with a label, a transition function, a
        pre-condition, and an address. *)

    val positive' :
      label:S.TransLabel.t -> loc:'a Place.t -> 'a S.Address.t -> _t
    (** Same as {positive} with the transition function being the identity *)

    val forward : S.TransLabel.t -> 'a Place.t -> 'a Place.t -> _t
    (** Neutral transition that forwards from the first location to the
        second *)

    (** {2 Manipulating transitions} *)
    val run : ('a, 'b) t -> 'a -> 'b option
    (** Run the transition function of a transition given its input. It
        returns an optional function since the transition function may reject
        the input. *)

    val fun_to_string : _t -> string
    (** Give a string representation of the function of a transition *)

    val pre : _t -> Place._t list
    (** Returns the list of pre condition of a transition *)

    val post : _t -> Place._t list
    (** Returns the list of post condition of a transition *)

    val pre_ids : _t -> int list
    (** Returns the list of IDs of the pre conditions of a transition *)

    val post_ids : _t -> int list
    (** Returns the list of IDs of the post conditions of a transition *)

    val id : _t -> int
    (** Returns the id of a transition *)

    val is_neutral : _t -> bool
    (** Returns true if transition is neutral *)

    val is_positive : _t -> bool
    (** Returns true if transition is positive *)

    val is_negative : _t -> bool
    (** Returns true if transition is negative *)

    val polarity : _t -> Polarity.t option
    (** Returns the polarity of a transition ([None] means neutral) *)

    (** {2 Fancy creation of transition} *)

    (** This modules creates a little DSL to define transitions.

        For instance, to define a neutral transition with several pre/post:

        {[
          let open Fancy in
            Neutral.(make ~label ~f:
            (let* v1 = !pre1 and* v2 = !pre2 in
             post1 := v1+v2 ||| post2 := v2)
        ]}

        For negative transitions:

        {[
          let open Fancy in
          Negative.(
            make ~label ~f:(fun token ->
                post1 := f1 token ||| post2 := f2 token ))
        ]}

        For positive transitions:

        {[
          let open Fancy in
          Positive.(
            make ~label
              ~f:
                (let* v1 = !pre1 and* v2 = !pre2 in
                 v1 + v2 ))
        ]} *)

    module Fancy : sig
      module Pre : sig
        type ('split, 'together, 'total) t =
          | L : 'a Place.t -> (('total, 'a) Expression.t, 'a, 'total) t
          | CC :
              ('sp1, 'tg1, 'tot) t * ('sp2, 'tg2, 'tot) t
              -> ('sp1 * 'sp2, 'tg1 * 'tg2, 'tot) t
      end

      module Post : sig
        type ('context, 'output) t =
          | L : 'b Place.t * ('context, 'b) Expression.t -> ('context, 'b) t
          | CC :
              ('context, 'out1) t * ('context, 'out2) t
              -> ('context, 'out1 * 'out2) t
      end

      module Neutral : sig
        val ( ||| ) :
          ('a, 'b) Post.t -> ('a, 'c) Post.t -> ('a, 'b * 'c) Post.t

        val ( @:= ) : 'a Place.t -> ('b, 'a) Expression.t -> ('b, 'a) Post.t

        val bind :
             ('a, 'b, 'b) Pre.t
          -> ('a -> ('b, 'c) Post.t)
          -> S.TransLabel.t
          -> _t

        val ( let* ) :
             ('a, 'b, 'b) Pre.t
          -> ('a -> ('b, 'c) Post.t)
          -> S.TransLabel.t
          -> _t

        val ( and* ) :
             ('a, 'b, 'c) Pre.t
          -> ('d, 'e, 'c) Pre.t
          -> ('a * 'd, 'b * 'e, 'c) Pre.t

        val make : label:S.TransLabel.t -> f:(S.TransLabel.t -> _t) -> _t
      end

      module Negative : sig
        val ( @:= ) : 'a Place.t -> ('b, 'a) Expression.t -> ('b, 'a) Post.t

        val ( ||| ) :
          ('a, 'b) Post.t -> ('a, 'c) Post.t -> ('a, 'b * 'c) Post.t

        val make :
             addr:'input S.Address.t
          -> label:S.TransLabel.t
          -> f:(('input, 'input) Expression.t -> ('input, 'b) Post.t)
          -> _t
      end

      module Positive : sig
        val ( and* ) :
             ('a, 'b, 'c) Pre.t
          -> ('d, 'e, 'c) Pre.t
          -> ('a * 'd, 'b * 'e, 'c) Pre.t

        val ( let* ) :
             ('a, 'b, 'b) Pre.t
          -> ('a -> ('b, 'output) Expression.t)
          -> 'output S.Address.t
          -> S.TransLabel.t
          -> _t

        val make :
             addr:'output S.Address.t
          -> label:S.TransLabel.t
          -> f:('output S.Address.t -> S.TransLabel.t -> _t)
          -> _t
      end
    end

    (** {2 Low-level functions to create transitions} *)
    val make_neutral :
         pre:'a Cond.t
      -> post:'b Cond.t
      -> f:('a, 'b) Expression.t
      -> label:S.TransLabel.t
      -> _t
    (** Creates a neutral transition *)

    val make_negative :
         post:'output Cond.t
      -> addr:'input S.Address.t
      -> f:('input, 'output) Expression.t
      -> label:S.TransLabel.t
      -> _t
    (** Creates a negative transition *)

    val make_positive :
         pre:'input Cond.t
      -> addr:'output S.Address.t
      -> f:('input, 'output) Expression.t
      -> label:S.TransLabel.t
      -> _t
    (** Create a positive transition *)
  end

  (** {1 Net definition and operations} *)

  (** A petri net: a list of locations and transitions *)
  type t = {locations: Place._t list; transitions: Transition._t list}

  val union : t -> t -> t
  (** Disjoint union of nets *)

  val empty : t
  (** Empty net *)

  (** {2 Rewriting nets *)

  (** This module defines a way of rewriting the addresses occurring in a
      net. *)

  module Rewrite : sig
    (** A rewriting function takes an address and return a rewriting a
        result. The result is optional: [None] means that the address is
        unchanged. *)
    type func = {rw: 'a. 'a S.Address.t -> 'a result option}

    (** A rewriting result is a different address, of a different types and
        two ways expressions between them. *)
    and 'a result =
      | Result :
          'b S.Address.t
          * ('a, 'b) Expression.func
          * ('b, 'a) Expression.func
          -> 'a result

    val return : 'a S.Address.t -> 'a result option

    val maybe : 'a S.Address.t option -> 'a result option

    val branches : func list -> func

    val rewrite : func -> t -> t
    (** Address rewriting of visible transitions *)
  end

  (** {2 Composition} *)

  (** A pattern is a function that takes an address [a] and returns None if
      [a] does not match the pattern, or [Some suffix] if it does *)
  type pat = {ma: 'a. 'a S.Address.t -> 'a S.Address.t option}

  val compose :
       merge_label:(S.TransLabel.t -> S.TransLabel.t -> S.TransLabel.t)
    -> left:pat
    -> right:pat
    -> t
    -> t
    -> t
  (** Net composition. [merge_label] is used to merge labels of synchronised
      transition; while left and right are patterns that are used to know
      when transitions are synchronising. *)

  (** {2 Forwarding (copycat)} *)
  type 'a fw_result =
    | Fw :
        { left: 'b S.Address.t
        ; right: 'c S.Address.t
        ; llbl: S.LocLabel.t
        ; tlbl: S.TransLabel.t
        ; leftToRight: ('b, 'c) Expression.func
        ; rightToLeft: ('c, 'b) Expression.func }
        -> 'a fw_result

  type fw = {fw: 'a. 'a S.Address.t -> 'a fw_result}

  val forwarder : fw -> Game.t -> t

  (** {2 Misc} *)

  val appears_in_pre_of : t -> Place._t -> Transition._t list

  val appears_in_post_of : t -> Place._t -> Transition._t list

  val flow :
       ?show:('b -> string * string)
    -> llabel:S.LocLabel.t
    -> tlabel:S.TransLabel.t
    -> f:(('a, 'a) Expression.t -> ('a, 'b) Expression.t)
    -> source:'a S.Address.t
    -> target:'b S.Address.t
    -> unit
    -> t

  val flow' :
       ?show:('a -> string * string)
    -> llabel:S.LocLabel.t
    -> tlabel:S.TransLabel.t
    -> source:'a S.Address.t
    -> target:'a S.Address.t
    -> unit
    -> t

  val to_dot :
       ?name:(Place._t -> string)
    -> t
    -> [> `Place of Place._t | `Transition of Transition._t] Dot.t
  (** Convert a Petri to dot. Nodes of the dot are locations and transitions. *)
end
