open Net
open Net.Expression

type 'a exp = (unit, 'a) Expression.t

module type Monad = sig
  type 'a t

  val return : 'a exp -> 'a t

  val bind : 'a t -> ('a exp -> 'b t) -> 'b t
end

module Make (M : Monad) = struct
  let ( let* ) = M.bind

  type expression =
    | Lambda of string * expression
    | App of expression * expression
    | Bool of bool
    | If of {cond: expression; if_true: expression; if_false: expression}

  type value = Bool of bool | Fun of (value exp -> value M.t)

  let cbool = Ctor.{name= "Bool"; i= (fun x -> Bool x)}

  let cfun = Ctor.{name= "Fun"; i= (fun f -> Fun f)}

  let value_of_bool b = Expression.construct cbool b

  let bool_of_value b = Expression.destruct cbool b

  let expression : (string * value exp) list -> expression -> value M.t =
   fun env exp ->
    match exp with
    | Bool b -> M.return (value_of_bool (Expression.bool b))
    | _ -> assert false
end
