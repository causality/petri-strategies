type _ t = L : 'a Place.t -> 'a t | CC : 'a t * 'b t -> ('a * 'b) t

type _t = C : 'a t -> _t

let ( ++ ) x y = CC (x, y)

let rec locs : type a. a t -> Place._t list = function
  | L l -> [Place.L l]
  | CC (x, y) -> locs x @ locs y

let rec length : type a. a t -> int = function
  | L _ -> 1
  | CC (a, b) -> length a + length b

let rec show_in : type a. a t -> a -> string =
 fun cond x ->
  match cond with
  | L loc -> fst @@ Place.show loc x
  | CC (a, b) ->
      Printf.sprintf "%s · %s" (show_in a (fst x)) (show_in b (snd x))

let rec lookup : type a b. a t -> a -> b Place.t -> b option =
 fun cond value loc ->
  match cond with
  | L loc' -> (
    match Place.equal loc loc' with
    | Some Poly.Eq.E -> Some value
    | None -> None )
  | CC (x, y) -> (
    match lookup x (fst value) loc with
    | Some z -> Some z
    | None -> lookup y (snd value) loc )

let rec ids : type a. a t -> int list = function
  | L loc -> [Place.id loc]
  | CC (x, y) -> ids x @ ids y

let rec mem : type a b. a Place.t -> b t -> bool =
 fun loc cond ->
  match cond with
  | L loc' -> None <> Place.equal loc loc'
  | CC (x, y) -> mem loc x || mem loc y

let rec union x y =
  match (x, y) with
  | None, _ -> y
  | _, None -> x
  | Some (C (L l)), Some (C y) when mem l y -> Some (C y)
  | Some (C (L l)), Some (C y) -> Some (C (L l ++ y))
  | Some (C (CC (z, w))), y -> union (Some (C z)) (union (Some (C w)) y)
