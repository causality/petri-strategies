(** Our representation of games arising from types in IPA (Alternated forest) *)

module Make (S : Sig.S) : sig
  type mode = Linear | Bang of mode

  type loc =
    { id: int
    ; addr: S.Address._t
    ; mode: mode
    ; mutable children: loc list
    ; parent: loc option }

  type t = loc list

  val ( ||| ) : t -> t -> t

  val addresses : t -> S.Address._t list

  val map_addresses : (S.Address._t -> S.Address._t) -> t -> t

  val find_addr : S.Address._t -> t -> loc

  val is_linear : t -> S.Address._t -> bool

  val parent : t -> S.Address._t -> S.Address._t option

  val bang : t -> t

  val empty : t

  val roots : t -> S.Address._t list

  val children : t -> S.Address._t -> S.Address._t list
end
