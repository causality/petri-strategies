module Typ = struct
  include Syntax.Typ
end

module Term = struct
  include Syntax.Term

  type t = Typ.t Syntax.Term.t

  let infer = Typer.go

  let of_string s =
    let lexbuf = Lexing.from_string s in
    try fst @@ Parser.program Lexer.token lexbuf with
    | Common.Loc.Error (_, _) as exn -> raise exn
    | e ->
        let loc = Common.Loc.of_lexbuf lexbuf in
        Common.Loc.fail loc e

  let parse s =
    let te, context = Typer.go (of_string s) in
    let te = Syntax.Barendregt.barendregt te in
    let context, n =
      List.fold_left
        (fun (context, n) (s, ty) ->
          let ty', n = Typ.annotate_typ n ty in
          ((s, ty') :: context, n) )
        ([], 0) context
    in
    let typ, _ = Typ.annotate_typ n te.data in
    (te, {Interpretation.Interface.context= List.rev context; typ})

  let interpret = Interpretation.interpret

  let examples = Exs.list
end

module Sig = Sig
module Interface = Interpretation.Interface
module Game = Net.Game.Make (Sig)

let loc_of_location l = l

let loc_of_transition (l, _) = l

let descr_of_transition (_, s) = s

let name = "IPA"

let r = ref 0

module A = Sig.Address
module M = Sig.Move

let generate_token (type a c) _game (data_gen : c Common.data_generator)
    (addr : a Sig.Address.t) justifier (k : a -> c) : c =
  let rec aux : type a. a Sig.Address.t -> M.t option -> (a -> c) -> c =
   fun addr move k ->
    match (addr, move) with
    | ( A.(Addr ([R Argument], Question d))
      , Some (M.M (Addr ([L Result], Question _), _)) ) ->
        data_gen.gen d (fun v ->
            incr r ;
            k (v, Sig.Exponential.of_int !r) )
    | ( A.(Addr ([R Context], Question d))
      , Some (M.M (Addr ([L Value], Question _), _)) ) ->
        data_gen.gen d (fun v ->
            incr r ;
            k (v, Sig.Exponential.of_int !r) )
    | A.(Addr (R _ :: is, kind)), Some (M.M (Addr (R _ :: is', kind'), value))
      ->
        aux
          A.(Addr (is, kind))
          (Some (M.M (Addr (is', kind'), fst value)))
          (fun e -> k (e, snd value))
    | A.(Addr (L _ :: is, kind)), Some (M.M (Addr (L _ :: is', kind'), value))
      ->
        aux A.(Addr (is, kind)) (Some (M.M (Addr (is', kind'), value))) k
    | A.(Addr ([], Question d)), _ -> data_gen.gen d (fun v -> k v)
    | A.(Addr ([], Answer d)), _ -> data_gen.gen d (fun v -> k v)
    | A.(Addr (L _ :: is, kind)), _ -> aux (Addr (is, kind)) move k
    | _ -> assert false
  in
  aux addr justifier k
