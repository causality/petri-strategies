(** This is the signature that a language model must implement *)
type 'k data_generator = {gen: 'a. 'a Net.Datatyp.t -> ('a -> 'k) -> 'k}

module type Language = sig
  (** Signature for the Petri net *)
  module Sig : Net.Sig.S

  module Interface : sig
    (** The type of interfaces: usually of the form Γ ⊦ A *)
    type t

    val game_of : t -> Net.Game.Make(Sig).t
    (** Interpret an interface as a game *)

    val show : t -> string
    (** Human-friendly representation of an interface *)
  end

  module Term : sig
    (** Programs of the language *)
    type t

    val parse : string -> t * Interface.t
    (** Parsing code: returns a term and the interface *)

    val interpret : Interface.t -> t -> Net.TNet.Make(Sig).t
    (** Interpret a term as a Petri Net *)

    val examples : (string * string) list
    (** A list of examples *)
  end

  (** Extract information from labels of locations and transitions *)

  val loc_of_location : Sig.LocLabel.t -> Loc.t option

  val loc_of_transition : Sig.TransLabel.t -> Loc.t option

  val descr_of_transition : Sig.TransLabel.t -> string option

  val name : string
  (** Name of the language *)

  val generate_token :
       Net.Game.Make(Sig).t
    -> 'k data_generator
    -> 'a Sig.Address.t
    -> Sig.Move.t option
    -> ('a -> 'k)
    -> 'k
end

module Loc = Loc

module Test (L : Language) = struct
  module Run = Net.Run.Make (L.Sig)

  let run move s =
    let term, intf = L.Term.parse s in
    let net = L.Term.interpret intf term in
    let g = L.Interface.game_of intf in
    Run.Machine.run g net move

  type t =
    { code: string
    ; input: L.Sig.Move.t
    ; predicate: Run.t list -> (unit, string) Result.t }

  let test ~code ~input ~pred = {code; input; predicate= pred}

  let run_test {code; input; predicate} =
    Printf.printf "Running %s ..." code ;
    let runs = run input code in
    match predicate runs with
    | Ok () -> Printf.printf "passed !\n"
    | Error e -> Printf.printf "failed\n %s\n" e

  let run_tests tests =
    Printf.printf "Running tests …\n" ;
    List.iter run_test tests

  let returns pred run =
    match Run.outputs run with
    | [move] -> pred move
    | [] -> Error "No positive moves (expected one)"
    | _ -> Error "Too many\n     positive moves (expected one)"

  let show_run r =
    let es = Run.events r in
    String.concat "\n"
    @@ List.map (fun e -> Printf.sprintf " - %s" (Run.Event.to_string e)) es

  let check_runs preds_orig runs_orig =
    let rec aux preds runs =
      match (preds, runs) with
      | [], [] -> Ok ()
      | [], _ ->
          Error
            (Printf.sprintf "Too many runs (%d, expected %d)"
               (List.length runs_orig) (List.length preds_orig) )
      | _, [] ->
          Error
            (Printf.sprintf "Too few runs (%d,\n     expected %d)"
               (List.length runs_orig) (List.length preds_orig) )
      | p :: preds, r :: rest -> (
        match p r with
        | Ok () -> aux preds rest
        | Error e ->
            Error
              (Printf.sprintf "Following run failed: %s.\n%s" e (show_run r))
        )
    in
    aux preds_orig runs_orig

  let terminates run =
    if Run.outputs run <> [] then Ok () else Error "No\n     positive moves"
end
