module Make (S : Sig.S) = struct
  module TNet = TNet.Make (S)
  module E = Expression
  open! TNet
  open Place

  let exec seek execute net =
    match seek net with
    | Some pattern -> Some (execute pattern net)
    | None -> None

  let rec exec_list ls net =
    match ls with
    | [] -> None
    | f :: fs -> (
      match f net with None -> exec_list fs net | Some net -> Some net )

  let rec optimise fs net =
    match exec_list fs net with Some net -> optimise fs net | None -> net

  module SquashForward = struct
    type _squashable =
      | S :
          { loc: 'a Place.t
          ; f: ('a, 'b) Expression.t
          ; post: 'b Cond.t
          ; tid: int }
          -> _squashable

    let seek net loc =
      (* We look for a location that appears in the pre of a unique
         transition t *)
      match appears_in_pre_of net loc with
      | [t] -> (
          let (Transition.T t') = t in
          let (Place.L loc) = loc in
          (* We check that the post of t do not contain loc (to avoid
             collapsing a loop) *)
          if List.mem (Place.id loc) (Transition.post_ids t) then None
          else
            (* And that t only contains [loc] as a pre condition *)
            match t'.cond with
            | Neutral {pre; post; _} -> (
              match pre with
              | Cond.L loc' -> (
                match equal loc' loc with
                | Some E -> Some (S {loc; f= t'.func; post; tid= t'.id})
                | None -> None )
              | _ -> None )
            | _ -> None )
      | _ -> None

    let seek net = List.find_map (seek net) net.locations

    type 'a post = Post : 'b Cond.t * ('a, 'b) Expression.t -> 'a post

    let exec (S {loc; f; post; tid}) net =
      let locations =
        List.filter
          (fun l -> Place.Untyped.id l <> Place.id loc)
          net.locations
      in
      let rewrite t =
        let open Cond in
        let rec rewrite_post :
            type a b. b Cond.t -> (a, b) Expression.t -> a post =
         fun cond e ->
          match cond with
          | CC (x, y) ->
              let (Post (p, e1)) = rewrite_post x (E.fst e) in
              let (Post (q, e2)) = rewrite_post y (E.snd e) in
              Post (CC (p, q), E.pair e1 e2)
          | L loc' -> (
            match Place.equal loc loc' with
            | None -> Post (L loc', e)
            | Some Poly.Eq.E -> Post (post, E.compose e f) )
        in
        match t with
        | Transition.T {cond= Positive _; _} -> t
        | Transition.T ({cond= Negative n; _} as x) ->
            let (Post (post, func)) = rewrite_post n.post x.func in
            Transition.T {x with func; cond= Negative {n with post}}
        | Transition.T ({cond= Neutral n; _} as x) ->
            let (Post (post, func)) = rewrite_post n.post x.func in
            Transition.T {x with func; cond= Neutral {n with post}}
      in
      let update_transitions t =
        if Transition.id t = tid then None
        else if List.mem (Place.id loc) (Transition.post_ids t) then
          Some (rewrite t)
        else Some t
      in
      { locations
      ; transitions= List.filter_map update_transitions net.transitions }
  end

  (* type _ trans = TR : ('a, 'b) Transition.t -> 'a trans

     let lonely_loc : type a. t -> a Place.t -> a trans option = fun net loc
     -> let is_the_right_one : type b c. (b, c) Transition.t -> a trans
     option = fun transition -> let check_pre : type x. (x, b, b) Pre._t -> a
     trans option = function | Pre.CC (_, _) -> None | Pre.L loc' -> ( match
     Place.equal loc loc' with | None -> None | Some E -> Some (TR
     transition) ) in match transition.func with | Negative _ -> None |
     Positive {pre; _} -> check_pre pre | Neutral {pre; _} -> check_pre pre
     in List.find_map (fun (Transition.T t) -> is_the_right_one t)
     net.transitions

     let ( let* ) = Option.bind

     let compose_post : type a b c d c_exp a_exp. (c_exp, c) Pre.t -> (a_exp,
     b, a) Post.t -> (c_exp, d, c) Post.t -> (c Place.t * (a_exp, d, a)
     Post.t) option = fun pre p1 p2 -> match (pre, p1) with | Pre.L loc,
     Post.L (loc', exp) -> ( match Place.equal loc loc' with | Some E -> Some
     (loc, Post.pre_apply pre exp p2) | None -> None ) | _, _ -> None

     let compose : type a b c d. _ -> (a, b) Transition.t -> (c, d)
     Transition.t -> (Place._t * (a, d) Transition.t) option = fun
     merge_label t t' -> let* (loc, func) : Place._t * (a, d) Transition.func
     = match (t.func, t'.func) with (* We cannot squash a negative and a
     positive transition *) | Negative _, Positive _ -> None (* Source cannot
     be positive (as they have no post) and target cannot be negative (as
     they have no pre) *) | _, Negative _ | Positive _, _ -> None | Neutral
     x, Neutral x' -> let* loc, post = compose_post x'.pre x.post x'.post in
     Some (Place.L loc, Transition.Neutral {pre= x.pre; post}) | Negative x,
     Neutral x' -> let* loc, post = compose_post x'.pre x.post x'.post in
     Some (Place.L loc, Transition.Negative {post; addr= x.addr}) | Neutral
     x, Positive x' -> let* loc, f = match (x.post, x'.pre) with | Post.L
     (loc, exp), Pre.L loc' -> ( match Place.equal loc loc' with | Some E ->
     Some ( Place.L loc , fun x -> Expression.compose (exp x) (x'.f (Pre.env
     x'.pre)) ) | _ -> None ) | _ -> None in Some (loc, Transition.Positive
     {addr= x'.addr; pre= x.pre; f}) in Some (loc, {t with func; label=
     merge_label t.label t'.label})

     let is_linear net loc = List.length (TNet.appears_in_pre_of net loc) = 1
     && List.length (TNet.appears_in_post_of net loc) = 1

     let find_lonely_location skippable merge_label net = let check
     (Transition.T t) (Transition.T u) = let* loc, replacement = compose
     merge_label t u in if skippable loc then Some (Transition.T t,
     Transition.T u, loc, Transition.T replacement) else None in let*
     Transition.(Place.(T t, T t', L loc, replace)) = List.find_map (fun t ->
     List.find_map (check t) net.transitions) net.transitions in let* () = if
     is_linear net (L loc) then Some () else None in let transitions =
     List.filter (fun (Transition.T x) -> not (x.id = t.id || x.id = t'.id))
     net.transitions in let locations = List.filter (fun (Place.L loc') ->
     not (loc.id = loc'.id)) net.locations in Some {transitions= replace ::
     transitions; locations}*)

  module Unreachable = struct
    type 'a post = P : 'b Cond.t * ('a, 'b) Expression.t -> 'a post

    let rec remove_loc_from_post :
        type a b x.
        a Place.t -> b Cond.t -> (x, b) Expression.t -> x post option =
     fun loc cond e ->
      match cond with
      | Cond.L loc' -> (
        match Place.equal loc loc' with
        | Some Poly.Eq.E -> None
        | None -> Some (P (cond, e)) )
      | CC (x, y) -> (
        match
          ( remove_loc_from_post loc x (E.fst e)
          , remove_loc_from_post loc y (E.snd e) )
        with
        | Some (P (a, e1)), Some (P (b, e2)) ->
            Some (P (CC (a, b), E.pair e1 e2))
        | None, None -> None (* Should not happen *)
        | None, Some x | Some x, None -> Some x )

    let ( let* ) = Option.bind

    let remove_loc_from_transition location (Transition.T transition) =
      let open Transition in
      match transition.cond with
      | Negative x ->
          let* (P (post, func)) =
            remove_loc_from_post location x.post transition.func
          in
          Some (T {transition with func; cond= Negative {x with post}})
      | Positive {pre; _} ->
          if List.mem (Place.id location) (Cond.ids pre) then None
          else Some (T transition)
      | Neutral x ->
          if List.mem (Place.id location) (Cond.ids x.pre) then None
          else
            let* (P (post, func)) =
              remove_loc_from_post location x.post transition.func
            in
            Some (T {transition with func; cond= Neutral {x with post}})

    let remove_loc net (Place.L loc) =
      let locations =
        List.filter (fun x -> Place.(Untyped.id x <> id loc)) net.locations
      in
      let transitions =
        List.filter_map (remove_loc_from_transition loc) net.transitions
      in
      {locations; transitions}

    module M = Map.Make (Place.Untyped)

    let add key x =
      M.update key (function None -> Some [x] | Some l -> Some (x :: l))

    let add_list keys x m = List.fold_left (fun m key -> add key x m) m keys

    let unreachable_locations net =
      let view_transition (pres, posts) t =
        Transition.(add_list (pre t) t pres, add_list (post t) t posts)
      in
      let pres, posts =
        List.fold_left view_transition (M.empty, M.empty) net.transitions
      in
      let is_unreachable l =
        try M.find l pres = [] || M.find l posts = [] with _ -> true
      in
      List.filter is_unreachable net.locations

    let trim net =
      let locs = unreachable_locations net in
      if locs = [] then None
      else Some (List.fold_left (fun net loc -> remove_loc net loc) net locs)
  end

  let optim ~merge_label:_ ~skippable:_ net =
    optimise
      [exec SquashForward.seek SquashForward.exec; Unreachable.trim]
      net
end
