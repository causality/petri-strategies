(* Code coloring *)
open Common
open Js_of_ocaml

let ( !! ) = Regexp.matched_string

type action =
  | Class of string
  | Subscript of Html_types.span_content H.elt list
  | Click of string * (unit -> unit)

type token = String of string | Action of action list * token list

type command = Regexp of string * string | Loc of Loc.t * action list

let act_on a list =
  match a with
  | Class s -> [H.span ~a:[H.a_class [s]] list]
  | Subscript s -> list @ [H.sub s]
  | Click (s, f) ->
      Obj.magic @@ H.[a ~a:[a_title s; a_onclick (fun _ -> f () ; true)] list]

let rec to_xml = function
  | String s -> [H.txt s]
  | Action (acts, toks) ->
      List.fold_left (fun xml act -> act_on act xml) (to_xml_list toks) acts

and to_xml_list l = List.concat_map to_xml l

let rec length = function
  | String s -> String.length s
  | Action (_, toks) -> length_toks toks

and length_toks toks = List.fold_left (fun n x -> n + length x) 0 toks

(* let apply_regexp_string regexp cl string =
 *   let re = Regexp.regexp regexp in
 *   let rec aux acc i =
 *     match Regexp.search_forward re string i with
 *     | Some (pos, result) ->
 *         let tok = Action ([Class cl], [String !!result]) in
 *         let acc =
 *           if pos > i then
 *             tok :: String (String.sub string i (pos - i)) :: acc
 *           else tok :: acc
 *         in
 *         aux acc (pos + (String.length @@ Regexp.matched_string result))
 *     | None ->
 *         List.rev
 *           (String (String.sub string i (String.length string - i)) :: acc)
 *   in
 *   aux [] 0
 * 
 * let apply_regexp regexp cl = function
 *   | String s -> apply_regexp_string regexp cl s
 *   | Act (act, toks) -> Act (act, apply_regexp_string_list regexp cl toks)
 * 
 * and apply_regexp_list regexp cl list =
 *   List.concat_map (apply_regexp regexp cl) list *)

let exec_command_string offset string = function
  | Regexp (regexp, cl) ->
      let re = Regexp.regexp regexp in
      let rec aux acc i =
        match Regexp.search_forward re string i with
        | Some (pos, result) ->
            let tok = Action ([Class cl], [String !!result]) in
            let acc =
              if pos > i then
                tok :: String (String.sub string i (pos - i)) :: acc
              else tok :: acc
            in
            aux acc (pos + (String.length @@ Regexp.matched_string result))
        | None ->
            List.rev
              (String (String.sub string i (String.length string - i)) :: acc)
      in
      aux [] 0
  | Loc (loc, act) ->
      let sub = String.sub string in
      let i = (fst loc).Loc.index - offset
      and j = (snd loc).Loc.index - offset in
      Printf.printf "%s: %d-%d\n" string i j ;
      if 0 <= i && 0 <= j && String.length string >= j then
        [ String (sub 0 i)
        ; Action (act, [String (sub i (j - i))])
        ; String (sub j (String.length string - j)) ]
      else [String string]

let rec exec_command_token offset command = function
  | String s -> exec_command_string offset s command
  | Action (act, toks) ->
      [Action (act, List.concat @@ exec_command_tokens offset command toks)]

and exec_command_tokens offset command toks =
  snd
  @@ List.fold_left_map
       (fun n tok -> (n + length tok, exec_command_token n command tok))
       offset toks

let rec exec_commands_token offset token = function
  | [] -> [token]
  | command :: q ->
      let tokens = exec_command_token offset command token in
      exec_commands_tokens offset tokens q

and exec_commands_tokens offset tokens commands =
  List.concat @@ snd
  @@ List.fold_left_map
       (fun n tok -> (n + length tok, exec_commands_token n tok commands))
       offset tokens

let exec commands string =
  to_xml_list @@ exec_commands_token 0 (String string) commands

let compare_locs loc1 loc2 =
  let open Loc in
  if index_start loc1 < index_start loc2 && index_stop loc1 > index_stop loc2
  then -1
  else if loc1 = loc2 then 0
  else 1

let color ?(cl = []) ?(locs = []) string =
  let locs = List.sort (fun (x, _) (y, _) -> compare_locs x y) locs in
  H.div ~a:[H.a_class ("code" :: cl)]
  @@ exec
       ( List.map (fun (loc, cl) -> Loc (loc, cl)) locs
       @ [ Regexp
             ("let|match|with| rec |type|begin|end|if|then|else", "keyword")
         ; Regexp ("[A-Z][a-zA-Z_]*", "constr")
         ; Regexp ("\".*\"", "string")
         ; Regexp ("[0-9]+", "number") ] )
       string
