% IPA to Petri Nets
# IPA to Petri Nets (documentation)

[Link to the webapp](index.html)

IPA to Petri net is a companion implementation for the papier
*The Geometry of Causality: Multi-Token Geometry of Interaction and its
Causal Unfolding*. It implements the translation from IPA to coloured
Petri nets, and allows the user to explore runs of the resulting net. 

## Input language

We have tried to stick to the presentation of the paper. The examples
are a good showcase of the syntax, and we hope that these are
self-explanatory. A main difference with the paper is the use of `()`
instead of `skip`. 

The typing inference only infers types for the simply-typed
lambda-calculus using constraints. This means that possible
polymorphism is instanciated to unit before computing the net (for
instance the term `x` is inferred as `x : unit ⊦ x : unit`). 

Moreover, some typing constraints are not checked by the typer. In
particular conditionals with higher-order branches or higher-order
blocks in `newref` and `newsem` are not rejected by the typechecker
but give rise to invalid nets.

## Displaying nets

The display of the net mostly follows the same convention as in the
paper:

- Rectangles are transitions and ellipses are locations
- Transitions in red are negative; in green positive; and in blue neutral.

To help the reader we have chosen to include *pointer links*. If a
negative transition corresponds to a multiplicative address which is
not minimal in the game, a dotted arrow will be included from any
transition (necessarily positive) on the justifying address to that
address. For instance, this means that there is an arrow from "Program
called function f" to "Context is returning from that call".

Below the source code we display the current *visible causal run*,
summing up the passed observable actions along with the reconstructed
causal dependency.

## Enabled transitions

Transitions that are enabled are dashed (a transition is enabled when
every location in its pre-condition set holds at least a token). In
theory, negative transition (without pre conditions) are always
enabled. The implementation only allows us to explore legal plays of
the game. This means that a negative transition is only enabled when a
transition on its justifying address has been fired before (In the
previous example: for context to return from f, Program must have
called f before). Moreover, some negative transitions are linear, which
means they can only be fired once in a legal play; or in general once
per token output on a justifying transition. In contrast, nonlinear
negative transitions can be fired as many times as one wants, provided
they are enabled.

In particular, at the beginning only one transition is enabled: the
initial negative transition.

## Firing transition

To fire an enabled transition, simply click on it. If there are
several tokens at one or several pre-conditions of the transition, the
interface asks the user to choose a particular token in each
location.

Firing an enabled transition executes the transition function. Since
transition functions are partial, a particular selection of input
tokens may be rejected. If so the tokens are not consumed and an error
message is displayed. 

Firing a negative transition generates a new token from the Context.
Depending on the type, the interface may ask the user to input a value
for the data. If there are several possible justifying moves for the
token to be generated, then the user can select which one to use from
the visible causal run (once again, the allowed choices are dashed).

## Reverse mode

By default, the net is in forward mode -- showing which transitions
can be fired. By clicking on *reverse mode*, the user can undo any
maximal transitions, not necessarily in the order in which they have
been performed. Visible transitions are undone on the visible causal
run, while neutral transitions are undone on the net.
