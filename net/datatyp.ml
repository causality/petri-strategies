type 'a t = Nat : int t | Bool : bool t | Unit : unit t

type _t = D : 'a t -> _t

let show_value : type a. a t -> a -> string = function
  | Nat -> string_of_int
  | Bool -> string_of_bool
  | Unit -> fun _ -> "()"

let equal : type a b. a t -> b t -> (a, b) Poly.Eq.t option =
 fun x y ->
  match (x, y) with
  | Nat, Nat -> Some E
  | Bool, Bool -> Some E
  | Unit, Unit -> Some E
  | _, _ -> None
