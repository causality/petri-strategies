type spot = {fname: string; line: int; column: int; index: int}

type t = spot * spot

let current_line_num = ref 1

let current_line_start_pos = ref 0

let current_file_name = ref ""

let next_line lexbuf =
  incr current_line_num ;
  current_line_start_pos := Lexing.lexeme_end lexbuf

let spot_of_pos n =
  { fname= !current_file_name
  ; line= !current_line_num
  ; column= n - !current_line_start_pos
  ; index= n }

let of_lexbuf lexbuf =
  ( spot_of_pos (Lexing.lexeme_start lexbuf)
  , spot_of_pos (Lexing.lexeme_end lexbuf) )

let fromto' (_, a) (_, b) = (fst a, snd b)

let fromto a b = (fst a, snd b)

let to_string (a, b) =
  let fname = if a.fname = "" then "" else a.fname ^ ":" in
  if a.fname <> b.fname then
    Printf.sprintf "%s%d.%d-%s:%d.%d:" fname a.line a.column b.fname b.line
      b.column
  else if a.line <> b.line then
    Printf.sprintf "%s%d.%d-%d.%d:" fname a.line a.column b.line b.column
  else if a.column <> b.column then
    Printf.sprintf "%s%d.%d-%d:" fname a.line a.column b.column
  else Printf.sprintf "%s%d.%d:" fname a.line a.column

let ghost =
  ( {fname= "<ghost>"; line= 0; column= 0; index= 0}
  , {fname= "<ghost>"; line= 0; column= 0; index= 0} )

let parsing_symbol () =
  let from_pos p =
    { fname= p.Lexing.pos_fname
    ; line= p.Lexing.pos_lnum
    ; index= p.Lexing.pos_bol + p.Lexing.pos_cnum
    ; column= p.Lexing.pos_cnum }
  in
  ( from_pos @@ Parsing.symbol_start_pos ()
  , from_pos @@ Parsing.symbol_end_pos () )

let get ({index= i; _}, {index= i'; _}) s = String.sub s i (i' - i)

let highlight ({index= i; _}, {index= i'; _}) before after s =
  String.sub s 0 i ^ before
  ^ String.sub s i (i' - i)
  ^ after
  ^ String.sub s i' (String.length s - i')

exception Parse_error of string

exception Error of t * string

let fail loc exn = raise (Error (loc, Printexc.to_string exn))

let parse_error loc fmt =
  Printf.kprintf (fun s -> fail loc (Parse_error s)) fmt

let failwith loc fmt = Printf.kprintf (fun s -> fail loc (Failure s)) fmt

let index_start (a, _) = a.index

let index_stop (_, b) = b.index
