(** Implementation of symbolic expressions *)
module Builtin = struct
  type ('a, 'b) t = {name: string; friendly: string; semantics: 'a -> 'b}

  let make ?friendly ~name semantics =
    {name; semantics; friendly= Option.value friendly ~default:name}
end

module Ctor = struct
  (** A constructor. ['a] is the type of the argument of the constructor and
      ['b] the type of the resulting sum type. For instance Some is ('a, 'a
      option) constructor *)
  type ('a, 'b) t = {name: string; i: 'a -> 'b}

  type ocaml_tag = Unboxed of int | Boxed of int

  let tag {i; _} =
    let o = Obj.repr (i (Obj.magic [|1; 1; 1; 1; 1; 1; 1; 1|])) in
    if Obj.is_int o then Unboxed (Obj.magic o : int) else Boxed (Obj.tag o)

  let destruct tag o =
    let o = Obj.repr o in
    let fail () = None in
    match tag with
    | Unboxed k -> if k = Obj.magic o then Some (Obj.magic ()) else fail ()
    | Boxed k ->
        if k = Obj.tag o then
          if Obj.size o = 1 then Some (Obj.magic (Obj.field o 0))
          else Some (Obj.magic (Obj.with_tag 0 o))
        else fail ()
end

module Field = struct
  let magic_array = Array.init 50 (fun i -> i)

  let index f = Obj.magic (f (Obj.magic magic_array))

  type ('a, 'b) t = {name: string; p: 'a -> 'b}

  let project index v = Obj.(obj @@ field (repr v) index)
end

type ('a, 'b) t =
  | Input : ('a, 'a) t
  | Unit : ('x, unit) t
  | Int : int -> ('x, int) t
  | Builtin : ('a, 'b) Builtin.t * ('x, 'a) t -> ('x, 'b) t
  | Construct : ('a, 'b) Ctor.t * Ctor.ocaml_tag * ('x, 'a) t -> ('x, 'b) t
  | Destruct : ('a, 'b) Ctor.t * Ctor.ocaml_tag * ('x, 'b) t -> ('x, 'a) t
  | Project : ('a, 'b) Field.t * int * ('x, 'a) t -> ('x, 'b) t
  | Tuple : ('ctx, 'record) tuple -> ('ctx, 'record) t
  | Guard : ('x, _) t * ('x, 'a) t -> ('x, 'a) t
  | Branches : ('a, 'b) t list -> ('a, 'b) t
  | Var : 'a Place.t -> ('x, 'a) t
  | Foreign : 'a -> ('x, 'a) t

and ('ctx, 'record, 'builder) record =
  | F : ('a, 'b) Field.t -> ('x, 'a, ('x, 'b) t) record
  | CC :
      ('ctx, 'record, 'builder) record * ('ctx, 'record, 'builder2) record
      -> ('ctx, 'record, 'builder * 'builder2) record

and ('ctx, 'record) tuple =
  | T : ('ctx, 'record, 'builder) record * 'builder -> ('ctx, 'record) tuple

let rec record_len : type a b c. (a, b, c) record -> int = function
  | F _ -> 1
  | CC (a, b) -> record_len a + record_len b

(** Evaluation of expressions *)
let ( let* ) = Option.bind

let rec eval : type a b. (a, b) t -> a -> b option =
 fun e i ->
  match e with
  | Input -> Some i
  | Int k -> Some k
  | Var _ -> failwith "eval: Cannot eval expression with variables"
  | Unit -> Some ()
  | Project (f, _, v) ->
      let* v = eval v i in
      Some (f.p v)
  | Tuple (T (record, args)) ->
      let len = record_len record in
      let o = Obj.new_block 0 len in
      let* _ = build_aux record args i 0 o in
      Some (Obj.obj o)
  | Builtin (b, e) -> Option.map b.semantics (eval e i)
  | Construct (c, _, e) -> Option.map c.i (eval e i)
  | Destruct (_, tag, e) ->
      let* x = eval e i in
      Ctor.destruct tag x
  | Guard (a, b) ->
      let* _ = eval a i in
      eval b i
  | Branches l ->
      let rec aux = function
        | [] -> None
        | t :: q -> (
          match eval t i with Some x -> Some x | None -> aux q )
      in
      aux l
  | Foreign x -> Some x

and build_aux :
    type x r b. (x, r, b) record -> b -> x -> int -> Obj.t -> int option =
 fun record args input index o ->
  match record with
  | F _ ->
      let* field_value = eval args input in
      Obj.set_field o index (Obj.repr field_value) ;
      Some (index + 1)
  | CC (a, b) ->
      let* index = build_aux a (fst args) input index o in
      build_aux b (snd args) input index o

(** Smart constructors — normalise on the fly when possible. *)

let input = Input

let record r b = Tuple (T (r, b))

let rec project_tuple :
    type a b c f. (b, f) Field.t -> (a, b, c) record -> c -> int -> (a, f) t
    =
 fun field record args i ->
  match (record, args) with
  | F _, x ->
      assert (i = 0) ;
      Obj.magic x
  | CC (r1, r2), (x, y) ->
      let n = record_len r1 in
      if i < n then project_tuple field r1 x i
      else project_tuple field r2 y (i - n)

let project : type a b x. (a, b) Field.t -> (x, a) t -> (x, b) t =
 fun field x ->
  let index = Field.(index field.p) in
  match x with
  | Tuple (T (r, a)) -> project_tuple field r a index
  | _ -> Project (field, index, x)

let fail = Branches []

let builtin b v = Builtin (b, v)

let construct c v = Construct (c, Ctor.tag c, v)

let branches l =
  Branches (List.concat_map (function Branches l -> l | x -> [x]) l)

let rec guard : type x a b. (x, a) t -> (x, b) t -> (x, b) t =
 fun g a ->
  match g with
  | Input | Int _ -> a
  | Branches l -> branches (List.map (fun x -> guard x a) l)
  | Project (_, _, t) -> guard t a
  | Tuple (T (r, t)) -> guard_record r t a
  | Construct (_, _, t) -> guard t a
  | Builtin (_, t) -> guard t a
  | _ -> (
    match a with
    | Branches l -> branches (List.map (fun x -> guard a x) l)
    | _ -> Guard (g, a) )

and guard_record :
    type a b c d. (a, b, c) record -> c -> (a, d) t -> (a, d) t =
 fun record args a ->
  match (record, args) with
  | F _, x -> guard x a
  | CC (r1, r2), (x, y) -> guard_record r1 x (guard_record r2 y a)

let destruct : type x a b. (a, b) Ctor.t -> (x, b) t -> (x, a) t =
 fun c v ->
  let tag = Ctor.tag c in
  match v with
  | Construct (_, tag', v') when tag = tag' -> Obj.magic v'
  | Construct (_, _, _) -> Branches []
  | _ -> Destruct (c, tag, v)

(** Composition of expressions - pretty straightforward *)
let rec compose : type a b c. (a, b) t -> (b, c) t -> (a, c) t =
 fun t u ->
  match u with
  | Input -> t
  | Var v -> guard t (Var v)
  (* If u is constant, we cannot remove [t] as [t] may contain checks that
     still need to be performed. Indeed fail >> const t = fail *)
  | Unit -> guard t Unit
  | Int k -> guard t (Int k)
  | Foreign x -> guard t (Foreign x)
  | Project (field, _, u') -> project field (compose t u')
  | Tuple us -> Tuple (compose_record t us)
  | Branches l -> Branches (List.map (compose t) l)
  | Construct (c, _, u') -> construct c (compose t u')
  | Destruct (c, _, u') -> destruct c (compose t u')
  | Builtin (b, u') -> builtin b (compose t u')
  | Guard (g, u') -> guard (compose t g) (compose t u')

and compose_record : type a b r. (a, b) t -> (b, r) tuple -> (a, r) tuple =
 fun t (T (record, args)) ->
  match (record, args) with
  | F field, x -> T (F field, compose t x)
  | CC (r1, r2), (x, y) ->
      let (T (x, a)) = compose_record t (T (r1, x)) in
      let (T (y, b)) = compose_record t (T (r2, y)) in
      T (CC (x, y), (a, b))

(** Binding with OCaml types *)

let int k = Int k

let unit = Unit

let ffst = Field.{name= "fst"; p= fst}

let fsnd = Field.{name= "snd"; p= snd}

let record_pair = CC (F ffst, F fsnd)

let pair x y = record record_pair (x, y)

let fst x = project ffst x

let snd x = project fsnd x

let construct0 c = construct c Unit

let construct2 c x y = construct c (pair x y)

let destruct0 = destruct

let destruct2 c x =
  let z = destruct c x in
  (fst z, snd z)

let ctrue = Ctor.{name= "true"; i= (fun _ -> true)}

let cfalse = Ctor.{name= "false"; i= (fun _ -> false)}

let ttrue = Ctor.tag ctrue

let _true = Construct (ctrue, ttrue, Int 0)

let untrue e = destruct ctrue e

let tfalse = Ctor.tag cfalse

let _false = Construct (cfalse, tfalse, Int 0)

let unfalse e = destruct cfalse e

let bool b = if b then _true else _false

let _if ~_then ?_else cond =
  match _else with
  | None -> guard (destruct ctrue cond) _then
  | Some _else ->
      branches
        [ guard (destruct ctrue cond) _then
        ; guard (destruct cfalse cond) _else ]

let not b = _if b ~_then:_false ~_else:_true

let _when b x = guard (destruct ctrue b) x

let _unless b x = guard (destruct cfalse b) x

let csome = Ctor.{name= "Some"; i= (fun x -> Some x)}

let cnone = Ctor.{name= "None"; i= (fun _ -> None)}

let some x = construct csome x

let unsome x = destruct csome x

let none () = construct0 cnone

let unnone x = destruct cnone x

let cnil = Ctor.{name= "nil"; i= (fun _ -> [])}

let ccons = Ctor.{name= "cons"; i= (fun (x, y) -> x :: y)}

let tnil = Ctor.tag cnil

let nil = Construct (cnil, tnil, Unit)

let unnil x = destruct0 cnil x

let cons x y = construct ccons (pair x y)

let uncons x = destruct2 ccons x

let hd x = Stdlib.fst (uncons x)

let tl x = Stdlib.snd (uncons x)

let beq = {Builtin.name= "="; friendly= "="; semantics= (fun (x, y) -> x = y)}

let ( == ) e1 e2 = builtin beq (pair e1 e2)

(** Untyped representaton for displaying and exporting to SMT.

    This representation neatly separates the "pure" part of a term that never
    fails, and the destruct that may fail. In general a term is represented
    as a normal form, which is a list of branches, each branch having the
    shape

    let #0 = destruct constructor value in ...

    let #n = destruct constructor value in value *)

module Untyped = struct
  type builtin = B : ('a, 'b) Builtin.t -> builtin

  type ctor = C : ('a, 'b) Ctor.t * Ctor.ocaml_tag -> ctor

  module Var = struct
    type t = int

    let compare = Int.compare

    let to_string = Printf.sprintf "#%d"
  end

  (** Pure untyped terms that may never fail *)
  module Pure = struct
    type t =
      | Input
      | Unit
      | Int of int
      | Tuple of (string * int * t) list
      | Project of (string * int) * t
      | Builtin of builtin * t
      | Construct of ctor * t
      | Intermediate of Var.t
          (** The case intermediate is used for the result of the matches *)

    let rec equal t u =
      match (t, u) with
      | Input, Input -> true
      | Unit, Unit -> true
      | Int k, Int k' -> k = k'
      | Tuple l, Tuple l' ->
          List.for_all2 (fun (_, _, x) (_, _, y) -> equal x y) l l'
      | Project (_, x), Project (_, y) -> equal x y
      | Builtin (B b, t), Builtin (B b', t') ->
          b.name = b'.name && equal t t'
      | Construct (C (_, t), v1), Construct (C (_, t'), v2) ->
          t = t' && equal v1 v2
      | Intermediate i, Intermediate j -> i = j
      | _, _ -> false

    let rec to_string = function
      | Input -> "input"
      | Unit -> "()"
      | Int k -> string_of_int k
      (* | Project ((name, _), Input) -> name*)
      | Project ((name, _), v) -> Printf.sprintf "%s.%s" (to_string v) name
      | Tuple l -> (
        match l with
        | [("fst", _, v); ("snd", _, v')] ->
            Printf.sprintf "(%s, %s)" (to_string v) (to_string v')
        | _ ->
            Printf.sprintf "{ %s } "
            @@ String.concat "; "
                 (List.map
                    (fun (name, _, x) ->
                      Printf.sprintf "%s=%s" name (to_string x) )
                    l ) )
      | Intermediate v -> Var.to_string v
      | Construct (C ({Ctor.name; _}, _), Unit) -> name
      | Construct (C ({Ctor.name; _}, _), e) -> (
        match (name, e) with
        | "cons", Tuple [("fst", _, v); ("snd", _, v')] ->
            Printf.sprintf "%s :: %s" (to_string v) (to_string v')
        | _, _ -> Printf.sprintf "%s(%s)" name (to_string e) )
      | Builtin (B {Builtin.name; _}, e) ->
          Printf.sprintf "%s(%s)" name (to_string e)
  end

  (** We then define Impure terms that are going to be morally a set of
      "pattern matches" (i.e. a pair (constructor, pure)), followed by a
      value that may refer to the result of the pattern matches. *)
  module M = Map.Make (Var)

  let findi p m =
    let exception Found of Var.t in
    try
      M.iter (fun i v -> if p i v then raise (Found i)) m ;
      None
    with Found i -> Some i

  module Impure = struct
    type bindings = (ctor * Pure.t) M.t

    (** We could have let [type t = (bindings * Pure.t) list] but it turns
        out that this makes it more natural to define the basic operations as
        it is more compositional by construction. *)
    type t = Pure.t * bindings -> (Pure.t * bindings) list

    let input (input, bindings) = [(input, bindings)]

    let int k (_input, bindings) = [(Pure.Int k, bindings)]

    let unit (_input, bindings) = [(Pure.Unit, bindings)]

    let post_compose (f : Pure.t -> Pure.t) (t : t) : t =
     fun (input, bindings) ->
      List.map
        (fun (value, bindings) -> (f value, bindings))
        (t (input, bindings))

    let project f i =
      post_compose (fun v -> Pure.Project ((f.Field.name, i), v))

    let rec tuple ?(acc = []) l (input, bindings) =
      match l with
      | [] -> [(Pure.Tuple (List.rev acc), bindings)]
      | (name, tag, t) :: q ->
          let branches = t (input, bindings) in
          List.concat_map
            (fun (r1, bindings) ->
              tuple ~acc:((name, tag, r1) :: acc) q (input, bindings) )
            branches

    let construct c = post_compose (fun v -> Pure.Construct (c, v))

    let builtin b = post_compose (fun v -> Pure.Builtin (b, v))

    let fresh m = try 1 + (Stdlib.fst @@ M.max_binding m) with _ -> 0

    let destruct c v (input, bindings) =
      let equal (C (_, t1), v1) (C (_, t2), v2) =
        t1 = t2 && Pure.equal v1 v2
      in
      let deal_with_branch (value, bindings') =
        match findi (fun _ -> equal (c, value)) bindings' with
        | Some i -> (Pure.Intermediate i, bindings')
        | None ->
            let j = fresh bindings' in
            let bindings' = M.add j (c, value) bindings' in
            (Intermediate j, bindings')
      in
      List.map deal_with_branch (v (input, bindings))

    let branches l (input, bindings) =
      List.concat_map (fun f -> f (input, bindings)) l

    let guard a b (input, bindings) =
      List.concat_map
        (fun (_, bindings') -> b (input, bindings'))
        (a (input, bindings))
  end

  let bindings_to_string bindings =
    let binding_to_string (i, (C (ctor, _), value)) =
      Printf.sprintf "  let %s = un%s(%s) in" (Var.to_string i)
        ctor.Ctor.name (Pure.to_string value)
    in
    String.concat "\n" @@ List.map binding_to_string (M.bindings bindings)

  let to_string (f : Impure.t) =
    let show_branch (value, bindings) =
      if M.is_empty bindings then Pure.to_string value
      else
        Printf.sprintf "%s\n  %s"
          (bindings_to_string bindings)
          (Pure.to_string value)
    in
    String.concat "\n---\n" @@ List.map show_branch (f (Input, M.empty))

  let rec project : type a b. (a, b) t -> Impure.t = function
    | Var _ -> failwith "project: Cannot project var"
    | Input -> Impure.input
    | Int k -> Impure.int k
    | Foreign _ -> failwith "project: cannot project foreign value"
    | Unit -> Impure.unit
    | Project (field, i, v) -> Impure.project field i (project v)
    | Tuple t -> Impure.tuple (project_record t)
    | Construct (c, t, v) -> Impure.construct (C (c, t)) (project v)
    | Builtin (b, v) -> Impure.builtin (B b) (project v)
    | Destruct (c, t, v) -> Impure.destruct (C (c, t)) (project v)
    | Branches l -> Impure.branches (List.map project l)
    | Guard (a, b) -> Impure.guard (project a) (project b)

  and project_record :
      type a b. (a, b) tuple -> (string * int * Impure.t) list =
   fun (T (record, args)) ->
    match (record, args) with
    | F f, args -> [(f.name, Field.index f.p, project args)]
    | CC (r1, r2), (x, y) ->
        project_record (T (r1, x)) @ project_record (T (r2, y))
end

let to_string x = "input → \n" ^ Untyped.(to_string (project x))

type ('a, 'b) func = ('a, 'a) t -> ('a, 'b) t

let apply f e = compose e (f input)

let post_apply f e = compose (f input) e

let unfunc f = f input

let functionalise e e' = compose e' e

module Var = struct
  let inject v = Var v

  let rec hide : type a x b. a Place.t -> (x, b) t -> (x * a, b) t =
   fun v e ->
    match e with
    | Input -> fst Input
    | Unit -> Unit
    | Int k -> Int k
    | Foreign x -> Foreign x
    | Var v' -> (
      match Place.compare v v' with Poly.Cmp.Eq -> snd Input | _ -> Var v' )
    | Construct (a, b, c) -> Construct (a, b, hide v c)
    | Destruct (a, b, c) -> Destruct (a, b, hide v c)
    | Builtin (b, e) -> Builtin (b, hide v e)
    | Project (field, k, t) -> Project (field, k, hide v t)
    | Tuple tuple -> Tuple (hide_tuple v tuple)
    | Guard (g, t) -> Guard (hide v g, hide v t)
    | Branches l -> Branches (List.map (hide v) l)

  and hide_tuple :
      type a ctx record.
      a Place.t -> (ctx, record) tuple -> (ctx * a, record) tuple =
   fun v tuple ->
    match tuple with
    | T (F f, x) -> T (F f, hide v x)
    | T (CC (c1, c2), (x, y)) ->
        let (T (c1, x)) = hide_tuple v (T (c1, x)) in
        let (T (c2, y)) = hide_tuple v (T (c2, y)) in
        T (CC (c1, c2), (x, y))

  let rec freevars : type x a. (x, a) t -> Cond._t option = function
    | Input -> None
    | Unit -> None
    | Int _ -> None
    | Foreign _ -> None
    | Var v' -> Some (C (L v'))
    | Construct (_, _, c) -> freevars c
    | Destruct (_, _, c) -> freevars c
    | Builtin (_, e) -> freevars e
    | Project (_, _, t) -> freevars t
    | Tuple tuple -> freevars_tuple tuple
    | Guard (g, t) -> Cond.union (freevars g) (freevars t)
    | Branches l -> List.fold_left Cond.union None (List.map freevars l)

  and freevars_tuple : type ctx record. (ctx, record) tuple -> Cond._t option
      = function
    | T (F _, x) -> freevars x
    | T (CC (c1, c2), (x, y)) ->
        Cond.union (freevars_tuple (T (c1, x))) (freevars_tuple (T (c2, y)))

  let mem var e =
    match freevars e with None -> false | Some (C c) -> Cond.mem var c

  let t =
    compose (pair (pair (fst input) (fst (snd input))) (snd (snd input)))

  let rec hide_aux : type x y a. x Cond.t -> (y, a) t -> (y * x, a) t =
   fun cond e ->
    match cond with
    | L loc -> hide loc e
    | CC (x, y) ->
        compose
          (pair (pair (fst input) (snd (snd input))) (fst (snd input)))
          (hide_aux x (hide_aux y e))

  let hide_cond cond e = compose (pair unit input) (hide_aux cond e)
end

let lift_left e = pair (compose (fst input) e) (snd input)

let lift_right e = pair (fst input) (compose (snd input) e)

let foreign x = Foreign x

let rec get_foreign : type x a. (x, a) t -> ((x, unit) t * a) list = function
  | Foreign x -> [(unit, x)]
  | Unit -> [(unit, ())]
  | Int k -> [(unit, k)]
  | Guard (g, u) -> List.map (fun (g', v) -> (guard g g', v)) (get_foreign u)
  | Branches l -> List.concat_map get_foreign l
  | Builtin (_, _)
   |Input | Var _
   |Construct (_, _, _)
   |Destruct (_, _, _)
   |Project (_, _, _)
   |Tuple _ ->
      []
