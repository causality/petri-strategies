open Common
open Syntax

type context = (string * Typ.t) list

let type_error loc fmt =
  Printf.kprintf (fun s -> raise (Loc.Error (loc, "Type error: " ^ s))) fmt

type unification_var = int

type open_typ =
  | OArrow of open_typ * open_typ
  | OBase of Typ.base
  | OVar of unification_var

let counter = ref 0

let fresh () = OVar (incr counter ; !counter)

let show =
  let rec aux paren = function
    | OBase b -> Typ.show_base b
    | OArrow (t, u) when paren ->
        "(" ^ aux true t ^ " -> " ^ aux false u ^ ")"
    | OArrow (t, u) -> aux true t ^ " -> " ^ aux false u
    | OVar k -> Printf.sprintf "'a_%d" k
  in
  aux false

let rec inject = function
  | Typ.Arrow (t, u) -> OArrow (inject t, inject u)
  | Typ.Base b -> OBase b

let rec substitute sub = function
  | OArrow (t, u) -> OArrow (substitute sub t, substitute sub u)
  | OBase b -> OBase b
  | OVar v -> ( try List.assoc v sub with Not_found -> OVar v )

let substitute_substitution sub =
  List.map (fun (name, typ) -> (name, substitute sub typ))

let rec project = function
  | OArrow (t, u) -> Typ.Arrow (project t, project u)
  | OBase b -> Base b
  | OVar _ -> Base Unit

let typ_of_const = function
  | Term.Skip -> Typ.Unit
  | Bool _ -> Typ.Bool
  | Int _ -> Typ.Int

let typ_of_op = function
  | Term.Add | Sub | Mult | Div -> ([OBase Int; OBase Int], OBase Int)
  | And | Or -> ([OBase Bool; OBase Bool], OBase Bool)
  | Equal -> ([OBase Int; OBase Int], OBase Bool)
  | Not -> ([OBase Bool], OBase Bool)
  | Term.Iszero -> ([OBase Int], OBase Bool)
  | Term.Deref -> ([OBase Ref], OBase Int)
  | Term.Assign -> ([OBase Ref; OBase Int], OBase Unit)
  | Term.Grab -> ([OBase Sem], OBase Unit)
  | Term.Release -> ([OBase Sem], OBase Unit)
  | Term.Succ | Term.Pred -> ([OBase Int], OBase Int)
  | Term.Par ->
      let v = fresh () in
      ([OBase Unit; v], v)

let rec gather_aux context {Term.loc; constructor; _} =
  match constructor with
  | Y ->
      let v = fresh () in
      ([], Term.Y, OArrow (OArrow (v, v), v))
  | Term.Var s -> (
    try ([], Term.Var s, List.assoc s context)
    with Not_found -> type_error loc "Unbound variable %s" s )
  | Lambda (name, ann, body) ->
      let typ = Option.value ~default:(fresh ()) (Option.map inject ann) in
      let constraints, body, ret = gather ((name, typ) :: context) body in
      (constraints, Lambda (name, ann, body), OArrow (typ, ret))
  | App (t1, t2) ->
      let c1, t1', func_type = gather context t1 in
      let c2, t2', arg_type = gather context t2 in
      let ret = fresh () in
      ( c1 @ c2
        @ [ ( loc
            , func_type
            , OArrow (arg_type, ret)
            , "Applied term must have an arrow type with argument type \
               matching the argument" ) ]
      , App (t1', t2')
      , ret )
  | If (ls, cond, branch1, branch2) ->
      let c1, cond', t_cond = gather context cond in
      let c2, branch1', t_branch1 = gather context branch1 in
      let c3, branch2', t_branch2 = gather context branch2 in
      ( c1 @ c2 @ c3
        @ [ ( cond.loc
            , t_cond
            , OBase Bool
            , "Condition of an if statement must be a boolean" )
          ; ( branch2.loc
            , t_branch1
            , t_branch2
            , "Branches of a conditional must have the same type" ) ]
      , If (ls, cond', branch1', branch2')
      , t_branch1 )
  | Let (l, (name, nloc), l', body, rest) ->
      let c1, body', t_body = gather context body in
      let c2, rest', t_rest = gather ((name, t_body) :: context) rest in
      (c1 @ c2, Let (l, (name, nloc), l', body', rest'), t_rest)
  | Const c -> ([], Const c, OBase (typ_of_const c))
  | Operator ((op, oploc), l) ->
      let data = List.map (gather context) l in
      let expected, out = typ_of_op op in
      let constraints, l' =
        List.split
        @@ List.map2
             (fun t (constraints, arg', t_arg) ->
               ( (arg'.Term.loc, t, t_arg, "Argument of operator")
                 :: constraints
               , arg' ) )
             expected data
      in
      (List.concat constraints, Operator ((op, oploc), l'), out)
  | Newref ((name, loc), body) ->
      let constraints, body', t =
        gather ((name, OBase Typ.Ref) :: context) body
      in
      (constraints, Newref ((name, loc), body'), t)
  | Newsem ((name, loc), body) ->
      let constraints, body', t =
        gather ((name, OBase Typ.Sem) :: context) body
      in
      (constraints, Newsem ((name, loc), body'), t)

and gather context term =
  let constraints, constructor, data = gather_aux context term in
  (constraints, {term with data; constructor}, data)

let rec unify t u =
  match (t, u) with
  | OBase b, OBase b' when b = b' -> Some []
  | OVar v, x | x, OVar v -> Some [(v, x)]
  | OArrow (sigma, tau), OArrow (sigma', tau') ->
      Option.bind (unify sigma sigma') (fun l ->
          Option.map
            (fun l' -> substitute_substitution l' l @ l')
            (unify (substitute l tau) (substitute l tau')) )
  | _ -> None

let solve =
  let substitute_in l sigma =
    let sub_one (loc, t1, t2, r) =
      (loc, substitute sigma t1, substitute sigma t2, r)
    in
    List.map sub_one l
  in
  let rec aux substitution = function
    | [] -> substitution
    | (loc, t1, t2, reason) :: q -> (
      match unify t1 t2 with
      | None ->
          type_error loc "%s: Cannot unify %s and %s" reason (show t1)
            (show t2)
      | Some sigma ->
          aux
            (sigma @ substitute_substitution sigma substitution)
            (substitute_in q sigma) )
  in
  aux []

let go term =
  let context =
    List.map (fun name -> (name, fresh ())) (Term.freevars term)
  in
  let constraints, term', _ = gather context term in
  let sigma = solve constraints in
  ( Term.map (fun t -> project (substitute sigma t)) term'
  , List.map (fun (name, x) -> (name, project (substitute sigma x))) context
  )
