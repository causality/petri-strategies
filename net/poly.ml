module Eq = struct
  type ('a, 'b) t = E : ('a, 'a) t
end

module Cmp = struct
  type ('a, 'b) t =
    | Less : ('a, 'b) t
    | Great : ('a, 'b) t
    | Eq : ('a, 'a) t

  let to_eq : type a b. (a, b) t -> (a, b) Eq.t option = function
    | Less | Great -> None
    | Eq -> Some Eq.E

  let to_int : type a b. (a, b) t -> int = function
    | Less -> -1
    | Great -> 1
    | Eq -> 0
end

module type Ordered = sig
  type 'a t

  val compare : 'a t -> 'b t -> ('a, 'b) Cmp.t
end

module Hide (O : Ordered) = struct
  type t = H : 'a O.t -> t

  let compare (H x) (H y) = Cmp.to_int (O.compare x y)
end
