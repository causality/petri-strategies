open Common
open Net
module E = Expression

module O = struct
  open Expression
  open Builtin

  let add x = builtin (make ~name:"+" (fun (x, y) -> x + y)) x

  let sub x = builtin (make ~name:"-" (fun (x, y) -> x - y)) x

  let mul x = builtin (make ~name:"*" (fun (x, y) -> x * y)) x

  let div x = builtin (make ~name:"/" (fun (x, y) -> x / y)) x

  let eq x = builtin (make ~name:"=" (fun (x, y) -> x = y)) x

  let succ x = builtin (make ~name:"succ" (fun x -> x + 1)) x

  let pred x = builtin (make ~name:"pred" (fun x -> x - 1)) x

  let iszero x = builtin (make ~name:"iszero" (fun x -> x = 0)) x

  let not x = builtin (make ~name:"not" Stdlib.not) x
end

module Exponential = struct
  type t = Left of t | Right of t | Pair of t * t | Diamond
  [@@deriving yojson]

  let cleft = E.Ctor.{name= "Left"; i= (fun x -> Left x)}

  let left x = E.construct cleft x

  let unleft x = E.destruct cleft x

  let cright = E.Ctor.{name= "Right"; i= (fun x -> Right x)}

  let right x = E.construct cright x

  let unright x = E.destruct cright x

  let cpair = E.Ctor.{name= "Pair"; i= (fun (x, y) -> Pair (x, y))}

  let pair x = Expression.construct2 cpair x

  let unpair x = Expression.destruct2 cpair x

  let cdiamond = E.Ctor.{name= "Diamond"; i= (fun () -> Diamond)}

  let diamond _ = Expression.construct0 cdiamond

  let undiamond x = Expression.destruct0 cdiamond x

  let rec show = function
    | Left m -> Printf.sprintf "ℓ%s" (show m)
    | Right m -> Printf.sprintf "𝓇%s" (show m)
    | Pair (m1, m2) -> Printf.sprintf "⟨%s, %s⟩" (show m1) (show m2)
    | Diamond -> Printf.sprintf "◆"

  let rec of_int n =
    if n = 0 then Diamond
    else
      let e = of_int (n / 2) in
      if n mod 2 = 1 then Left e else Right e
end

module Address = struct
  module AddrSig = struct
    type linear =
      | Result
      | LContext
      | Value
      | Var of string
      | Left
      | Right
      | Read
      | Write
      | Grab
      | Release

    type replicated = Argument | Context | PromotedValue

    type exponential = Exponential.t

    let show_exponential = Exponential.show

    let pol_linear = function
      | Var _ | Result | Left | Right | Read | Write | Grab | Value | Release
        ->
          Polarity.Positive
      | LContext -> Polarity.Negative

    let show_linear = function
      | Result -> "result"
      | Value -> "value"
      | LContext -> "context"
      | Var s -> s
      | Left -> "left"
      | Right -> "right"
      | Read -> "read"
      | Write -> "write"
      | Grab -> "grab"
      | Release -> "release"

    let pol_replicated = function
      | Context | Argument -> Polarity.Negative
      | PromotedValue -> Polarity.Positive

    let show_replicated = function
      | Context -> "context"
      | Argument -> "arg"
      | PromotedValue -> "value!"

    module Kind = Net.Address.QA
  end

  include Net.Address.Make (AddrSig)

  (* let rec is_root = function | [] -> true | Result :: q -> is_root q | _
     -> false*)

  let parent (A a) (A b) =
    let rec aux : type a b. a t -> b t -> bool =
     fun (Addr (is, k)) (Addr (is', k')) ->
      match (is, k, is', k') with
      | [], Question _, [], Answer _ -> true
      | [L Result], Question _, [R Argument], Question _ -> true
      | [L Value], Question _, [R Context], Question _ -> true
      | L i :: is, k, L i' :: is', k' when i = i' ->
          aux (Addr (is, k)) (Addr (is', k'))
      | R i :: is, k, R i' :: is', k' when i = i' ->
          aux (Addr (is, k)) (Addr (is', k'))
      | _ -> false
    in
    aux a b
end

module LocLabel = struct
  type t = Loc.t option

  let show _ = ""
end

module TransLabel = struct
  type t = Loc.t option * string option

  let show _ = ""
end

module Move = struct
  type t = M : 'a Address.t * 'a -> t

  let rec sdep (M (Addr (is, k), v)) (M (Addr (is', k'), v')) =
    match (is, k, is', k') with
    | [], Question _, [], Answer _ -> true
    | [L Result], Question _, [R Argument], Question _ -> true
    | [L Value], Question _, [R Context], Question _ -> true
    | L i :: is, k, L i' :: is', k' when i = i' ->
        sdep (M (Addr (is, k), v)) (M (Addr (is', k'), v'))
    | R i :: is, k, R i' :: is', k' when i = i' && snd v = snd v' ->
        sdep (M (Addr (is, k), fst v)) (M (Addr (is', k'), fst v'))
    | _ -> false
end
