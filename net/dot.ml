module Attribute = struct
  type t = {name: string; value: string}

  let show {name; value} = Printf.sprintf "%s=\"%s\"" name value

  let show_list l = String.concat ", " @@ List.map show l

  let arrowhead kind = {name= "arrowhead"; value= kind}

  let fillcolor lbl = {name= "fillcolor"; value= lbl}

  let color c = {name= "color"; value= c}

  let shape c = {name= "shape"; value= c}

  let label lbl = {name= "label"; value= lbl}

  let style s = {name= "style"; value= s}

  let class_ s = {name= "class"; value= s}

  let classes l = {name= "class"; value= String.concat " " l}

  let id s = {name= "id"; value= s}
end

module Node = struct
  type 'a t = {name: string; attrs: Attribute.t list; label: 'a}

  let show {name; attrs; _} =
    Printf.sprintf "%s [%s]" name (Attribute.show_list attrs)

  let _id = ref 0

  let make ~label ~attrs =
    let name = "node_" ^ string_of_int (incr _id ; !_id) in
    {name; attrs= Attribute.id name :: attrs; label}

  let name {name; _} = name

  let label {label; _} = label
end

module Edge = struct
  type 'a t = {src: 'a; tgt: 'a; attrs: Attribute.t list}

  let show table {src; tgt; attrs; _} =
    Printf.sprintf "%s -> %s [%s]" (List.assoc src table)
      (List.assoc tgt table)
      (Attribute.show_list attrs)

  let make ~src ~tgt ~attrs = {src; tgt; attrs}
end

type 'a t = {nodes: 'a Node.t list; edges: 'a Edge.t list}

let to_string ?(name = "es") t =
  let table = List.map (fun n -> (n.Node.label, n.name)) t.nodes in
  Printf.sprintf "digraph %s {\n %s\n%s\n }" name
    (String.concat "\n" @@ List.map Node.show t.nodes)
    (String.concat "\n" @@ List.map (Edge.show table) t.edges)

let to_file t fname =
  let fd = open_out fname in
  output_string fd (to_string t) ;
  close_out fd

let convert ?(fmt = "pdf") graph output =
  let temp = Filename.temp_file "graph" "dot" in
  to_file graph temp ;
  ignore @@ Sys.command
  @@ Printf.sprintf "dot -T%s %s > %s" fmt (Filename.quote temp)
       (Filename.quote output)

let to_pdf g o = convert g o

let to_png g o = convert ~fmt:"png" g o

let view ?(viewer = "xdg-open") t =
  let pdf_file = Filename.temp_file "graph" "pdf" in
  let () = to_pdf t pdf_file in
  ignore @@ Sys.command
  @@ Printf.sprintf "%s %s" viewer (Filename.quote pdf_file)
