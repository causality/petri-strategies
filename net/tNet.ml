let ( let* ) = Option.bind

module Make (S : Sig.S) = struct
  module Game = Game.Make (S)
  module E = Expression

  module Transition = struct
    type ('input, 'output) cond =
      | Negative :
          {post: 'output Cond.t; addr: 'input S.Address.t}
          -> ('input, 'output) cond
          (** A negative transition has a list of post-conditions and an
              address *)
      | Positive :
          {pre: 'input Cond.t; addr: 'output S.Address.t}
          -> ('input, 'output) cond
          (** A positive transition has a list of pre-conditions and an
              address *)
      | Neutral :
          {pre: 'input Cond.t; post: 'output Cond.t}
          -> ('input, 'output) cond
          (** A neutral transition has no address but both sets of pre- and
              post- condition *)

    (** The type of conditions for transitions transforming input data
        ['input] into ['output]. The third type argument is a phantom type
        used to track polarity. *)

    type ('a, 'b) t =
      { id: int  (** Identifier of the transition *)
      ; func: ('a, 'b) Expression.t
            (** The expression describing the behaviour of the transition on
                colors *)
      ; cond: ('a, 'b) cond  (** Transition function *)
      ; label: S.TransLabel.t  (** Label *) }

    (** Existential quantification over input/output types of transitions *)
    type _t = T : ('a, 'b) t -> _t

    let addr : type a b. (a, b) t -> S.Address._t option =
     fun t ->
      match t.cond with
      | Positive {addr; _} -> Some (S.Address.A addr)
      | Negative {addr; _} -> Some (S.Address.A addr)
      | _ -> None

    let id = ref 0

    let polarity : type a b. (a, b) t -> Polarity.t option =
     fun t ->
      match t.cond with
      | Positive _ -> Some Polarity.Positive
      | Negative _ -> Some Polarity.Negative
      | _ -> None

    let make_neutral ~pre ~post ~f ~label =
      T {func= f; cond= Neutral {pre; post}; label; id= (incr id ; !id)}

    let make_negative ~post ~addr ~f ~label =
      T {cond= Negative {addr; post}; func= f; label; id= (incr id ; !id)}

    let make_positive ~pre ~addr ~f ~label =
      T {func= f; cond= Positive {addr; pre}; label; id= (incr id ; !id)}

    let negative ~label ~f ~loc addr =
      make_negative ~label ~addr ~post:Cond.(L loc) ~f:(f Expression.input)

    let negative' ~label ~loc = negative ~f:(fun x -> x) ~label ~loc

    let positive ~label ~f ~loc addr =
      make_positive ~label ~addr ~pre:Cond.(L loc) ~f:(f Expression.input)

    let positive' ~label ~loc = positive ~f:(fun x -> x) ~label ~loc

    let forward label a b =
      make_neutral
        ~pre:Cond.(L a)
        ~post:Cond.(L b)
        ~f:Expression.input ~label

    let pre (T t) =
      match t.cond with
      | Negative _ -> []
      | Neutral {pre; _} -> Cond.locs pre
      | Positive {pre; _} -> Cond.locs pre

    let id (T t) = t.id

    let pre_ids x = List.map Place.Untyped.id (pre x)

    let post (T t) =
      match t.cond with
      | Positive _ -> []
      | Neutral {post; _} -> Cond.locs post
      | Negative {post; _} -> Cond.locs post

    let post_ids x = List.map Place.Untyped.id (post x)

    let run : type inp outp. (inp, outp) t -> inp -> outp option =
     fun transition input -> Expression.eval transition.func input

    let rec to_string_post_aux :
        type a b. b Cond.t -> (a, b) Expression.t -> string list =
     fun cond exp ->
      match cond with
      | Cond.L loc ->
          [ Printf.sprintf "Expression for loc %d\n ———————————\n%s\n"
              (Place.id loc)
              (Expression.to_string exp) ]
      | CC (a, b) ->
          to_string_post_aux a (Expression.fst exp)
          @ to_string_post_aux b (Expression.snd exp)

    let to_string_post cond exp =
      String.concat " \n " @@ to_string_post_aux cond exp

    let fun_to_string (T t) =
      match t.cond with
      | Neutral {post; _} -> to_string_post post t.func
      | Negative {post; _} -> to_string_post post t.func
      | Positive _ -> Expression.to_string t.func

    let is_neutral (T t) = match t.cond with Neutral _ -> true | _ -> false

    let is_negative (T t) =
      match t.cond with Negative _ -> true | _ -> false

    let is_positive (T t) =
      match t.cond with Positive _ -> true | _ -> false

    let polarity (T t) =
      match t.cond with
      | Positive _ -> Some Polarity.Positive
      | Negative _ -> Some Polarity.Negative
      | Neutral _ -> None

    module Fancy = struct
      module Pre = struct
        type ('split, 'together, 'total) t =
          | L : 'a Place.t -> (('total, 'a) Expression.t, 'a, 'total) t
          | CC :
              ('sp1, 'tg1, 'tot) t * ('sp2, 'tg2, 'tot) t
              -> ('sp1 * 'sp2, 'tg1 * 'tg2, 'tot) t

        let ( +++ ) x y = CC (x, y)

        let rec cond : type a b c. (a, b, c) t -> b Cond.t = function
          | L loc -> L loc
          | CC (x, y) -> CC (cond x, cond y)

        let rec env : type a b c. (a, b, c) t -> (c, b) Expression.t -> a =
         fun pre input ->
          match pre with
          | L _ -> input
          | CC (x, y) ->
              (env x (Expression.fst input), env y (Expression.snd input))
      end

      module Post = struct
        type ('context, 'output) t =
          | L : 'b Place.t * ('context, 'b) Expression.t -> ('context, 'b) t
          | CC :
              ('context, 'out1) t * ('context, 'out2) t
              -> ('context, 'out1 * 'out2) t

        let ( ||| ) x y = CC (x, y)

        let ( @:= ) a b = L (a, b)

        let rec exp : type a b. (a, b) t -> (a, b) Expression.t = function
          | L (_, x) -> x
          | CC (x, y) -> Expression.pair (exp x) (exp y)

        let rec cond : type a b. (a, b) t -> b Cond.t = function
          | L (l, _) -> L l
          | CC (x, y) -> CC (cond x, cond y)
      end

      module Neutral = struct
        let bind :
            type a b c.
            (a, b, b) Pre.t -> (a -> (b, c) Post.t) -> S.TransLabel.t -> _t =
         fun pre k ->
          let post = k (Pre.env pre Expression.input) in
          fun label ->
            make_neutral ~pre:(Pre.cond pre) ~post:(Post.cond post)
              ~f:(Post.exp post) ~label

        let ( ! ) l = Pre.L l

        let ( ||| ) = Post.( ||| )

        let ( @:= ) = Post.( @:= )

        let ( and* ) x y = Pre.(x +++ y)

        let ( let* ) = bind

        let make ~label ~f = f label
      end

      module Negative = struct
        let ( ||| ) = Post.( ||| )

        let ( @:= ) = Post.( @:= )

        let make ~addr ~label ~f =
          let post = f Expression.input in
          make_negative ~addr ~label ~post:(Post.cond post)
            ~f:(Post.exp post)
      end

      module Positive = struct
        let ( ! ) l = Pre.L l

        let ( +++ ) = Pre.( +++ )

        let get l = Pre.L l

        let ( let* ) pre k addr label =
          let f = k (Pre.env pre Expression.input) in
          make_positive ~pre:(Pre.cond pre) ~addr ~label ~f

        let ( and* ) x y = x +++ y

        let make ~addr ~label ~f = f addr label
      end
    end
  end

  type t = {locations: Place._t list; transitions: Transition._t list}

  let empty = {locations= []; transitions= []}

  let union n1 n2 =
    { locations= n1.locations @ n2.locations
    ; transitions= n1.transitions @ n2.transitions }

  type map =
    {map: 'a 'b. ('a, 'b) Transition.t -> ('a, 'b) Transition.t option}

  let map_transitions {map} net =
    { net with
      transitions=
        List.filter_map
          (fun (Transition.T t) ->
            Option.map (fun t' -> Transition.T t') (map t) )
          net.transitions }

  module Rewrite = struct
    type func = {rw: 'a. 'a S.Address.t -> 'a result option}

    and 'a result =
      | Result :
          'b S.Address.t
          * ('a, 'b) Expression.func
          * ('b, 'a) Expression.func
          -> 'a result

    let success x = Result (x, (fun x -> x), fun x -> x)

    let return x = Some (success x)

    let maybe x = Option.map success x

    let branches l =
      let f x =
        let rec aux = function
          | [] -> None
          | {rw} :: fs -> (
            match rw x with None -> aux fs | Some x -> Some x )
        in
        aux l
      in
      {rw= f}

    let rewrite {rw} net =
      let open Transition in
      let map (Transition.T t) : Transition._t =
        match t.cond with
        | Neutral _ -> T t
        | Positive x ->
            let (Result (addr, f, _)) =
              Option.value ~default:(success x.addr) @@ rw x.addr
            in
            T
              { t with
                cond= Positive {x with addr}
              ; func= Expression.apply f t.func }
        | Negative x ->
            let (Result (addr, _, g)) =
              Option.value ~default:(success x.addr) @@ rw x.addr
            in
            T
              { t with
                cond= Negative {x with addr}
              ; func= Expression.post_apply g t.func }
      in
      {net with transitions= List.map map net.transitions}
  end

  module T = Transition

  type pat = {ma: 'a. 'a S.Address.t -> 'a S.Address.t option}

  type ('a, 'b) pat_result =
    | Positive of 'a Cond.t * 'b S.Address.t
    | Negative of 'a S.Address.t * 'b Cond.t

  let is_on :
      type a b. pat -> (a, b) Transition.t -> (a, b) pat_result option =
   fun {ma= pattern} t ->
    match t.cond with
    | Neutral _ -> None
    | Positive {addr; pre} ->
        Option.map (fun x -> Positive (pre, x)) (pattern addr)
    | Negative {addr; post} ->
        Option.map (fun x -> Negative (x, post)) (pattern addr)

  let synchronise merge_label ~left ~right sigma tau =
    let can_synchronise :
        type a b c d.
        (a, b) Transition.t -> (c, d) Transition.t -> Transition._t option =
     fun tsigma ttau ->
      match (is_on left tsigma, is_on right ttau) with
      | Some (Negative (addr, post)), Some (Positive (pre, addr')) -> (
        match S.Address.equal addr addr' with
        | Some E ->
            Some
              (Transition.make_neutral ~pre ~post
                 ~f:Expression.(compose ttau.func tsigma.func)
                 ~label:(merge_label tsigma.label ttau.label) )
        | _ -> None )
      | Some (Positive (pre, addr)), Some (Negative (addr', post)) -> (
        match S.Address.equal addr addr' with
        | Some E ->
            Some
              (Transition.make_neutral ~pre ~post
                 ~f:Expression.(compose tsigma.func ttau.func)
                 ~label:(merge_label tsigma.label ttau.label) )
        | _ -> None )
      | _ -> None
    in
    let map_sigma (T.T tsigma) =
      match is_on left tsigma with None -> Some (T.T tsigma) | _ -> None
    in
    let map (T.T ttau) =
      match is_on right ttau with
      | None -> [T.T ttau]
      | _ ->
          List.filter_map
            (fun (T.T tsigma) -> can_synchronise tsigma ttau)
            sigma.transitions
    in
    List.filter_map map_sigma sigma.transitions
    @ List.concat_map map tau.transitions

  let compose ~merge_label ~left ~right t u =
    let t_and_u = union t u in
    {t_and_u with transitions= synchronise merge_label ~left ~right t u}

  let to_dot ?(name = fun _ -> "") net =
    let open Dot in
    let lookup loc =
      List.find (fun (Place.L l) -> Place.id l = loc) net.locations
    in
    let pointers =
      List.concat_map
        (fun (T.T src) ->
          List.filter_map
            (fun (T.T dst) ->
              match (T.addr src, T.addr dst) with
              | Some p, Some q when S.Address.parent p q ->
                  Some
                    (Edge.make ~src:(`Transition (T.T src))
                       ~tgt:(`Transition (T.T dst))
                       ~attrs:[Attribute.style "dotted"] )
              | _ -> None )
            net.transitions )
        net.transitions
    in
    let node_of_location loc =
      Node.make ~label:(`Place loc)
        ~attrs:
          Attribute.[shape "ellipse"; label (name loc); classes ["location"]]
    in
    let walk_transition (edges, nodes) (T.T t) =
      let from_edge loc =
        Edge.make
          ~src:(`Place (lookup loc))
          ~tgt:(`Transition (T.T t)) ~attrs:[]
      in
      let to_edge loc =
        Edge.make
          ~tgt:(`Place (lookup loc))
          ~src:(`Transition (T.T t)) ~attrs:[]
      in
      let lbl, co =
        match t.cond with
        | Neutral _ -> ("", "neutral")
        | Positive {addr; _} ->
            (S.Address.show (S.Address.A addr), "positive")
        | Negative {addr; _} ->
            (S.Address.show (S.Address.A addr), "negative")
      in
      ( List.map from_edge (T.pre_ids (T t))
        @ List.map to_edge (T.post_ids (T t))
        @ edges
      , Node.make ~label:(`Transition (T.T t))
          ~attrs:Attribute.[shape "rectangle"; classes [co]; label lbl]
        :: nodes )
    in
    Printf.printf "--- Walking the talk... --- \n%!" ;
    let edges, nodes =
      List.fold_left walk_transition
        (pointers, List.map node_of_location net.locations)
        net.transitions
    in
    Printf.printf "--- Final concat... --- \n%!" ;
    {edges; nodes}

  type 'a fw_result =
    | Fw :
        { left: 'b S.Address.t
        ; right: 'c S.Address.t
        ; llbl: S.LocLabel.t
        ; tlbl: S.TransLabel.t
        ; leftToRight: ('b, 'c) Expression.func
        ; rightToLeft: ('c, 'b) Expression.func }
        -> 'a fw_result

  type fw = {fw: 'a. 'a S.Address.t -> 'a fw_result}

  let forwarder fw g =
    let iter (S.Address.A address as a) =
      let (Fw result) = fw.fw address in
      let loc =
        Place.make ~show:(fun x -> S.Address.show_msg result.left x) ()
      in
      let ts =
        (* Right to Left *)
        if S.Address.polarity a = Polarity.Negative then
          [ T.make_negative ~addr:result.right
              ~post:Cond.(L loc)
              ~f:(Expression.unfunc result.rightToLeft)
              ~label:result.tlbl
          ; T.make_positive ~addr:result.left
              ~pre:Cond.(L loc)
              ~f:E.input ~label:result.tlbl ]
        else
          (* Left to right *)
          [ T.make_negative ~addr:result.left
              ~post:Cond.(L loc)
              ~f:E.input ~label:result.tlbl
          ; T.make_positive ~addr:result.right
              ~f:(Expression.unfunc result.leftToRight)
              ~pre:Cond.(L loc)
              ~label:result.tlbl ]
      in
      (Place.L loc, ts)
    in
    let locations, ts = List.split (List.map iter (Game.addresses g)) in
    {locations; transitions= List.concat ts}

  type filter = {filter: 'a. 'a S.Address.t -> ('a, 'a) Expression.t}

  let filter_tokens net ~outgoing ~ingoing =
    let map_transition (Transition.T t) =
      let func' =
        match t.cond with
        | Neutral _ -> t.func
        | Positive x -> Expression.compose t.func (outgoing.filter x.addr)
        | Negative x -> Expression.compose (ingoing.filter x.addr) t.func
      in
      Transition.T {t with func= func'}
    in
    {net with transitions= List.map map_transition net.transitions}

  let appears_in_pre_of net loc =
    List.filter
      (fun t -> List.mem (Place.Untyped.id loc) (T.pre_ids t))
      net.transitions

  let appears_in_post_of net loc =
    List.filter
      (fun t -> List.mem (Place.Untyped.id loc) (T.pre_ids t))
      net.transitions

  let flow :
         ?show:('b -> string * string)
      -> llabel:S.LocLabel.t
      -> tlabel:S.TransLabel.t
      -> f:(('a, 'a) E.t -> ('a, 'b) E.t)
      -> source:'a S.Address.t
      -> target:'b S.Address.t
      -> unit
      -> t =
   fun ?show ~llabel ~tlabel ~f ~source ~target () ->
    ignore llabel ;
    let l = Place.make ?show () in
    { locations= [L l]
    ; transitions=
        [ T.negative ~label:tlabel ~loc:l ~f source
        ; T.positive' ~label:tlabel ~loc:l target ] }

  let flow' ?show ~llabel ~tlabel ~source ~target () =
    flow ?show ~llabel ~tlabel ~f:(fun x -> x) ~source ~target ()
end
