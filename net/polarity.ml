type t = Positive | Negative

let is_negative = function Negative -> true | _ -> false

let is_positive = function Positive -> true | _ -> false

let dual = function Positive -> Negative | Negative -> Positive

let mult x y = if x = y then Positive else Negative

let ( * ) = mult
