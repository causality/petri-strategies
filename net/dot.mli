(** Dot: a thin layer of abstraction over the Dot format. *)

module Attribute : sig
  type t = {name: string; value: string}

  val arrowhead : string -> t

  val color : string -> t

  val style : string -> t

  val fillcolor : string -> t

  val shape : string -> t

  val class_ : string -> t

  val classes : string list -> t

  val id : string -> t

  val label : string -> t
end

module Node : sig
  type +'a t

  val make : label:'a -> attrs:Attribute.t list -> 'a t

  val name : 'a t -> string

  val label : 'a t -> 'a
end

module Edge : sig
  type +'a t

  val make : src:'a -> tgt:'a -> attrs:Attribute.t list -> 'a t
end

(** The type of dot graphs: a list of nodes and edges. ['a] labels nodes and
    ['b] labels edges. *)
type 'a t = {nodes: 'a Node.t list; edges: 'a Edge.t list}

val to_string : ?name:string -> 'a t -> string
(** Converts an abstraction representation to a Dot-formatted string. *)

val to_file : 'a t -> string -> unit
(** [to_file graph fname] outputs the representation of [graph] to [fname]. *)

val to_pdf : 'a t -> string -> unit
(** [to_pdf graph fname] outputs the representation of [graph] in PDF.
    Requires Dot. *)

val to_png : 'a t -> string -> unit
(** [to_png graph fname] outputs the representation of [graph] in PNG.
    Requires Dot. *)

val view : ?viewer:string -> 'a t -> unit
(** View a graph by calling a viewer. This needs the binary dot. *)

(** {1 Attributes} *)
